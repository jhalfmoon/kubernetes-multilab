#!/bin/bash -eux
#
# Build a alpine image for vagrant.

DEST=/tmp
NAME=alpine-vagrant

if ! (which bsdtar &>/dev/null) ; then
    echo "ERROR: bsdtar executable not found. Please install the package libarchive-tools. Exiting."
    exit 1
fi

rm -rf $DEST/$NAME
git clone https://github.com/rgl/$NAME.git $DEST/$NAME
cd $DEST/$NAME

VER=$(jq -r .variables.version alpine.json)
echo "Building alpine version $VER"

sed -ie 's/\(headless.*:\).*/\1 false,/g' alpine.json
export PACKER_KEY_INTERVAL=10ms
time packer build \
    -only=alpine-${VER}-amd64-libvirt \
    -on-error=abort \
    -var  'cpus=4' \
    -var  'memory=4096' \
    -var  'qemu_display=gtk' \
    alpine.json

vagrant box add -f alpine-$VER-amd64 alpine-$VER-amd64-libvirt.box

#export HEADLESS=false
#    -var  'headless=false' \
