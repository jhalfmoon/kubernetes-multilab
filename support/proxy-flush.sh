#!/bin/bash

# Tool to assist debugging of proxy issues

# This was used to debug two repos that were giving issues, primarily the first one listed:
# docker pull cr.l5d.io/linkerd/proxy:stable-2.11.0
# docker pull rancher/shell:v0.1.10
# The linkerd images were being requested from cr.l5d.io and redirected to ghcr.io. By caching cr.l5d.io instead of ghcr.io,
# caching was not happening and worse: The downloads were failing. Removing cr.l5d.io from the list of cacheable sites and adding
# ghcr.io fixed the issue.

echo "--- Deleting (running) container(s)"
docker container rm -f docker_registry_proxy
#docker container rm -f registry.local

echo "--- Removing cache file storage"
sudo rm -rf /vagrant/temp/registry/docker_mirror_*

echo "--- Starting proxy"
#/vagrant/scripts/docker-registry-install.sh
/vagrant/scripts/proxy-install.sh
sudo -E /vagrant/scripts/proxy-activate.sh
