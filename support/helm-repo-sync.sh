#!/bin/bash

# Clone / update a given set of repositories and optionally create symlinks to any
# Helmn chart values.yaml file in a central folder, for easy access for reference
# while working with these Helm charts.

BASEDIR=$HOME/repos/helm
CMD=$(readlink -f $0)
CONFIG_FILE=${CMD%%.sh}.conf

if [[ ! -e $CONFIG_FILE ]] ; then
    echo "ERROR: $CONFIG_FILE not found."
    echo "You can use $CONFIG_FILE.example to create your custom config file."
    exit 1
else
    source $CONFIG_FILE
fi

echo "Using BASEDIR $BASEDIR"

[[ -d $BASEDIR/values ]] || mkdir -p $BASEDIR/values

while read NAME URL VALUES ; do
    cd $BASEDIR
    if [[ -d $NAME ]] ; then
        echo === Updating $NAME  -  $URL =======
        cd $NAME
        git fetch --all
        # https://stackoverflow.com/questions/292357/what-is-the-difference-between-git-pull-and-git-fetch
        git merge FETCH_HEAD
    else
        echo === Cloning $NAME  -  $URL =======
        git clone $URL $NAME
        cd $NAME
    fi
    # create symlink to values file
    LINK=$BASEDIR/values/$NAME
    echo "Linking $BASEDIR/$NAME/$VALUES to $LINK"
    if [[ -n $VALUES ]] ; then
        if [[ -e $VALUES ]] ; then
            [[ -L $LINK ]] || ln -s $BASEDIR/$NAME/$VALUES $LINK
        else
            echo "ERROR: Values file $VALUES should exist, but doesn't. Please adjust path to values file."
        fi
    fi
done < <(echo "$REPOS" | grep -v '^$' | grep -v '^#')

echo
echo "Symlinks to values file are located @ $BASEDIR/values"
echo

