#!/bin/bash

# Get vm name from vagrantfile
HERE=$(dirname $(readlink -f $0))
. $HERE/get-name.sh

# Default snapshot name is 'clean' unless a name is given as a parameter
NAME=clean
[[ -n $1 ]] && NAME=$1

cd $HERE/../$VM_TYPE
VMS=$(vagrant status | grep "${VM_TYPE}-[0-9]\+" | awk '{print $1}')

echo "$VMS" | while read VM; do
    # only destroy first is a snapshot exists
    if [[ $(virsh snapshot-list --domain $VM --name| wc -w) -gt 0 ]] ; then
        echo "Destroying node '$VM'"
        virsh destroy --domain $VM
    fi
done

echo "$VMS" | while read VM; do
    echo "Restoring snapshot '$NAME' for node '$VM'"
    virsh snapshot-revert --snapshotname $NAME --domain $VM
done
