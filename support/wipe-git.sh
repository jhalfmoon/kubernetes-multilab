#!/bin/bash

echo "Delete ALL changes to this repo, including cache/temp files."
echo -n "To confirm enter YES: "
read REPLY
if [[ $REPLY != 'YES' ]] ; then
    echo "Cancelled."
else
    git reset --hard HEAD
    sudo git clean -fxd
    git checkout master
fi

echo
echo "NOTE: If you forgot to stop any libvirt instances before running this tool,"
echo "then you might encounter a vagrant or libvirt error. In that case run the"
echo "following command: ./support/wipe-libvirt"
echo
