#!/bin/bash -eux

IMAGE=$(ls *.img)
DEV=/dev/nbd0
MOUNT=/tmp/temp-mount

cat > my-user-data <<EOF
#cloud-config
password: changeme
chpasswd: { expire: False }
ssh_pwauth: True
EOF

modprobe nbd max_part=8
qemu-nbd --connect=$DEV $IMAGE

umount $MOUNT >/dev/null
[[ -d $MOUNT ]] || mkdir -p $MOUNT
mount ${DEV}p1 $MOUNT

umount $MOUNT
qemu-nbd --disconnect $DEV
rmmod nbd


#qemu-img convert -O qcow2 $IMAGE $IMAGE.tmp
#cloud-localds $IMAGE.tmp my-user-data
#qemu-img convert -pc -O qcow2 -o preallocation=off,cluster_size=64k,compression_type=zstd $IMAGE.tmp $IMAGE.new
#virt-install --import -n xenial --memory 4096 --os-type linux --vcpus 4 --disk path=/tmp/disk.img --disk path=/tmp/my-seed.img --network default --graphics none --console pty,target_type=serial  --debug
