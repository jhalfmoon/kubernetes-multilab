#!/bin/bash

# Get vm name from vagrantfile
HERE=$(dirname $(readlink -f $0))
. $HERE/get-name.sh

# Default snapshot name is 'clean' unless a name is given as a parameter
NAME=clean
[[ -n $1 ]] && NAME=$1

cd $HERE/../$VM_TYPE
vagrant status | grep "${VM_TYPE}-[0-9]\+" | awk '{print $1}' | while read VM; do
    echo "Creating snapshot '$NAME' for node '$VM'"
    virsh snapshot-create-as --name $NAME --domain $VM
done
