
# Tool to import the generated CA cert into the following:
#   Linux (redhat, ubuntu, arch)
#   Chrome
#   Firefox (seems not to work; needs debugging d.d. 20210913)

HERE=$(dirname $(readlink -f $0))
CERT=$HERE/../temp/rootCA.pem
DATE=$(date +%Y%m%d-%H%M%S)

if ! [[ -e $CERT ]] ; then
    echo "ERROR: The root CA $CERT was not found. Possibly none was generated."
    return 1
fi

if ! (which certutil &>/dev/null) ; then
    echo "ERROR: Certutil not found. Please install the package 'nss'"
    return 1
else
    echo "Adding $CERT to chrome based browsers"
    certutil -d "sql:$HOME/.pki/nssdb" -A -i $CERT -n "Localhost dev CA XXX ($DATE)" -t C,,
    echo "Adding $CERT to firefox based browesers"
    certutil -d ~/.mozilla/firefox/*.default -A -i $CERT -n "Localhost dev CA XXX ($DATE)" -t C,,
fi

# Add cert to system-wide ca certs and restart docker to get it to uset the CA
echo "Are you sure you with to install the self-sign CA to your system?"
echo -n "To confirm enter YES: "
read REPLY
if [[ $REPLY == 'YES' ]] ; then
    if [[ -e /etc/arch-release ]] ; then
        # https://wiki.archlinux.org/title/User:Grawity/Adding_a_trusted_CA_certificate
        sudo trust anchor --store $CERT
    elif [[ -e /etc/redhat-release ]] ; then
        # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-shared-system-certificates
        sudo cp $(readlink -f $HERE/../temp/rootCA.pem) /etc/pki/ca-trust/source/anchors/
        sudo update-ca-trust
    elif [[ -e /etc/debian_version ]] ; then
        # https://askubuntu.com/questions/73287/how-do-i-install-a-root-certificate
        sudo openssl x509 -in $(readlink -f $HERE/../temp/rootCA.pem) -inform PEM -out /usr/share/ca-certificates/kubernetes-lab.crt
        echo "kubernetes-lab.crt" | sudo tee -a /etc/ca-certificates.conf
        sudo update-ca-certificates
    else
        echo "Unrecognized Linux distro; CA not installed."
    fi
else
    echo "Cancelled."
fi
