#!/bin/bash

docker logs -f $(docker container ls | grep registry-proxy | awk '{print $1}') | grep --color 'MISS\|HIT'
