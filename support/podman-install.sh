# Source me

# This is a quick and dirty howto for installing podman on ubuntu
# Source: https://github.com/containers/podman/issues/14302#issuecomment-1310886397
#
# Upstream docs (NOTE: Those do not mention homebrew): https://podman.io/docs/installation

sudo apt-get install build-essential

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo >> /home/vagrant/.bashrc
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/vagrant/.bashrc
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

brew install podman
brew services start podman

