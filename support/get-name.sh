#!/bin/bash

# This is a support tool to detect what if a Vagrant config with one of the following names is running:
NAMES='k8s k3s k3d kind minikube openstack alpine ubuntu'

REGEX="\($(echo $NAMES|sed 's/ /\\\|/g')\)-1"

VM_TYPE=$(virsh list --all | grep -- "$REGEX" | awk '{print $2}' | cut -d- -f1)

echo "Detecting active know vm instances ($NAMES)..."
if [[ -z $VM_TYPE ]] ; then
    echo "No known instances found of type:"
    echo "$NAMES"
    exit 1
elif [[ $(echo $VM_TYPE | wc -l) -gt 1 ]] ; then
    echo "Multiple instances found: $VM_TYPE"
else
    echo "Instance found: $VM_TYPE"
fi
