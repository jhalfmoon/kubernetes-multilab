#!/bin/bash

# Tool to show vagrant vm and libvirt snapshot status.
# Call this from the root of a folder containing a Vagrant file.

HERE=$(dirname $(readlink -f $0))
. $HERE/get-name.sh

echo -e "\nCurrent libvirt instances:"
virsh list --all

cd $HERE/../$VM_TYPE
[[ ! -e Vagrantfile ]] && exit 1
vagrant status

echo "Available snapshots:"
vagrant status | grep "${VM_TYPE}-[0-9]\+" | awk '{print $1}' | while read VM; do
    virsh snapshot-list --domain $VM
done
