#!/bin/bash -eu

# Install vagrant from source.
# Created on 2024.11 because vagrant on Arch was broken due to ruby gem issues.
# References:
#   https://github.com/hashicorp/vagrant
#   https://github.com/vagrant-libvirt/vagrant-libvirt
#   https://bundler.io/v2.5/man/bundle-install.1.html
#   https://github.com/rubygems/bundler
#   https://guides.rubygems.org/rubygems-basics/
#   https://guides.rubygems.org/command-reference/

BIN_DIR=$HOME/.bin-ruby
BUNDLE_DIR=$HOME/.bundles
REPO_DIR=$BUNDLE_DIR/vagrant.git
REPO_URL='https://github.com/hashicorp/vagrant.git'
COMMIT='v2.4.3'
DATE=$(date +%Y%m%d-%H%M%S)

CheckInstalled() {
    STATUS=0
    for ITEM in $@ ; do
        if (which $ITEM &> /dev/null) ; then
            echo "$ITEM binary is available."
        else
            echo "$ITEM is not installed. Please install it."
            STATUS=1
        fi
    done
    return $STATUS
}

echo
echo "If you encounter errors, try installing the build requirements first:"
echo
echo "Arch:"
echo "sudo pacman -Sy ; sudo pacman -S --noconfirm git ruby bundler libvirt base-devel"
echo "Debian/Ubuntu:"
echo "sudo apt update ; sudo apt install -y git ruby-dev ruby-bundler libvirt-dev build-essential"
echo
echo "You might also require more dependencies to build / use vagrant-libvirt."
echo "https://vagrant-libvirt.github.io/vagrant-libvirt/installation.html"
echo
echo "Press enter to continue"
read

CheckInstalled git ruby bundler || exit 1

if [[ ! $PATH =~ $BIN_DIR ]] ; then
    echo
    echo WARNING: $BIN_DIR is not in \$PATH. You must fix this in .bash_profile. Press enter to continue.
    read
fi

CURRENT=$(which vagrant 2> /dev/null)
if [[ -n "$CURRENT" ]] && [[ ! $CURRENT =~ $BIN_DIR ]]; then
    echo
    echo "Vagrant binary is currently in \$PATH at the following location: $CURRENT"
    echo "If you run into issues, then uninstall that version to see if it fixes it."
    echo "Press enter to continue."
    read
fi

echo ''

[[ -e $BUNDLE_DIR ]] || mkdir -p $BUNDLE_DIR
[[ -e $BIN_DIR ]]    || mkdir -p $BIN_DIR

# This can be useful when debugging / testing - Start from scratch
#rm -rf $BUNDLE_DIR $BIN_DIR
#rm -rf $HOME/{.gem,.cache/gem,.local/share/gem}

[[ -e $REPO_DIR ]] || git clone $REPO_URL $REPO_DIR
cd $REPO_DIR

# This can be useful when debugging / testing - It wipes the repo clean, including package cache
# git clean -fxd

git checkout main
git pull

# Get list of more recent tags newer than current
NEW_TAGS="$(git tag | sed -e 1,/$COMMIT/\ d)"
if [[ -n $NEW_TAGS ]] ; then
    echo
    echo "WARNING: There are more recent releases available. You might want to consider using upgrading. Press enter to continue."
    echo
    echo "$NEW_TAGS"
    echo
    read
fi

git checkout $COMMIT || return 1

# Set the location for bundle installs to the same location as used by gem
# Via: https://stackoverflow.com/questions/19072070/how-to-find-where-gem-files-are-installed
#      https://bundler.io/v1.12/man/bundle-config.1.html
bundle config set path "$(gem environment user_gemhome)"
# Keep ruby binaries in seperate folder for easy cleanup
bundle config set bin $BIN_DIR
# Install build dependencies
bundle config set deployment false

# If the following error appears: "Could not find <package-version> in any of the sources"
# then try adding --full-index to the bundle install command.
# https://stackoverflow.com/questions/11885398/is-there-a-fix-for-the-could-not-find-gem-in-any-of-the-sources-error-that-d
echo "Installing dependencies..."
bundle install

# Create a cache directory with gems to minimize re-downloading on subsequent builds
echo "Creating package cache..."
bundle package

# Build the vagrant gem using the bundled rake version
echo "Building the application..."
bundle exec rake build

# Deploy packages for real. Note that if deploypment mode is not used, running vagrant will break
# when you do 'vagrant pluging install', because vagrant-spec is a git based gem, which is different
# from all the other dependencies in such a way that it breaks vagrant. This took much time to debug.
bundle config set deployment true
echo "Deploying the application..."
bundle install

echo
echo "Installing vagrant-libvirt..."
# Prevent ugly vagrant notifications
export VAGRANT_I_KNOW_WHAT_IM_DOING_PLEASE_BE_QUIET=true
vagrant plugin expunge --force
vagrant plugin install --verbose vagrant-libvirt

# The following might come in handy when you encounter ruby gem dependency issues and / or want force installation of a certain version:
#
# export VAGRANT_DISABLE_STRICT_DEPENDENCY_ENFORCEMENT=1
# bundle exec vagrant plugin install vagrant-libvirt --plugin-version 0.12.0

echo
echo "You might want to add the following to your .bashrc:"
echo "VAGRANT_CHECKPOINT_DISABLE=1"
echo "VAGRANT_BOX_UPDATE_CHECK_DISABLE=1"
echo "VAGRANT_I_KNOW_WHAT_IM_DOING_PLEASE_BE_QUIET=true"
echo
echo "Ensure that $BIN_DIR is in your \$PATH:"
echo "export \"PATH=$BIN_DIR:\$PATH\""
