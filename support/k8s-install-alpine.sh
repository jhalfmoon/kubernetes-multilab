# Run this script as root
# Source: https://wiki.alpinelinux.org/wiki/K8s

cat <<EOF >/etc/apk/repositories
echo "net.bridge.bridge-nf-call-iptables=1" >> $FILE
sysctl -wp $FILE

http://dl-cdn.alpinelinux.org/alpine/v3.20/main
http://dl-cdn.alpinelinux.org/alpine/v3.20/community
#http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
http://dl-cdn.alpinelinux.org/alpine/edge/testing
EOF

echo "br_netfilter" > /etc/modules-load.d/k8s.conf
modprobe br_netfilter
FILE=/etc/sysctl.d/k8s.conf
echo "net.ipv4.ip_forward=1" > $FILE
echo "net.bridge.bridge-nf-call-iptables=1" >> $FILE
sysctl -wp $FILE

cp -av /etc/fstab /etc/fstab.bak
sed -i '/swap/s/^/#/' /etc/fstab
swapoff -a

# Note: find current versions and pin them.
# apk search kubelet kubeadm kubectl

apk add \
kubelet=~1.32 \
kubeadm=~1.32 \
kubectl=~1.32 \
kubeadm \
kubelet \
kubectl \
cni-plugin-flannel \
cni-plugins \
flannel \
flannel-contrib-cni \
containerd \
uuidgen \
nfs-utils \
chrony \
bash

rc-update add chronyd
rc-service chronyd start

rc-update add containerd
rc-update add kubelet
rc-service containerd start

# Enable time sync (Not required in 3.20 if using default chrony)
#rc-update add ntpd
#rc-service ntpd start

#Option 1 - Using flannel as your CNI.
# NOTE: This may no longer be necessary on newer versions of the flannel package
#ln -s /usr/libexec/cni/flannel-amd64 /usr/libexec/cni/flannel

# Option 2 - Using calico as your CNI NOTE: This is required in 3.20 if you use calico
#ln -s /opt/cni/bin/calico /usr/libexec/cni/calico
#ln -s /opt/cni/bin/calico-ipam /usr/libexec/cni/calico-ipam

kubeadm init --pod-network-cidr=10.244.0.0/16 --node-name=$(hostname)

mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml

kubeadm token create --print-join-command


