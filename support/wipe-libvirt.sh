#!/bin/bash

HERE=$(dirname $(readlink -f $0))
. $HERE/get-name.sh

# Get list of vms
echo "--- Deleting domains"
DOMS=$(virsh list --all --name | grep $VM_TYPE)
if [[ -z $DOMS ]] ; then
    echo "No vms found with prefix $VM_TYPE"
else
    echo -n "Found VMs: "
    echo $DOMS
    # Delete snapshots and then stop and delete domains
    echo "$DOMS" | while read DOM ; do
        echo "--- Cleaning domain $DOM"
        SNAPS=$(virsh snapshot-list $DOM --name)
        if [[ -z $SNAPS ]] ; then
            echo "No snapshots found for $DOM"
        else
            echo "Found $(echo $SNAPS | wc -w) snapshots for $DOM"
            echo "$SNAPS" | while read SNAP ; do
                virsh snapshot-delete --domain $DOM $SNAP
            done
        fi
        if [[ $(virsh domstate $DOM) == "running" ]] ; then
            virsh destroy $DOM
        fi
        virsh undefine $DOM
    done
fi

# Delete any remaining volumes
POOLS=$(virsh pool-list --name)
if [[ -n $POOLS ]] ; then
    echo "--- Deleting dangling volumes"
    echo "$POOLS" | while read POOL ; do
        VOLS=$(virsh vol-list $POOL | grep "$VM_TYPE-[0-9]\+\.img" | awk '{print $1}')
        if [[ -z $VOLS ]] ; then
            echo "No volumes found in pool $POOL"
        else
            echo "Deleting volumes found in pool $POOL"
            echo "$VOLS" | while read VOL; do
                virsh vol-delete $VOL $POOL
            done
        fi
    done
fi
