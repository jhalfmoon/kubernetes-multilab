#!/bin/bash -eu

# This is support script to assist in bringing up the kubernetes setups in this repo.
# Its main goal is to run 'vagrant up' and afterwards set up the kubeconfig file.
# As a secondary function, it checks the folowing:
#   * if /etc/hosts is set up to resolve registry.local
#   * if the root ca in temp/rootCA.pem is installed on your system
#   * if a few useful support tools are installed on your system

HERE=$(dirname $(readlink -f $0))
EXIT_FLAG=0

if ! (which vagrant 2>/dev/null) ; then
    echo "ERROR: Vagrant binary not found. Please install it."
    exit 1
elif ! (vagrant plugin list | grep -q vagrant-libvirt 2>/dev/null) ; then
    echo "INFO: vagrant-libvirt not found. Will install it now..."
    VAGRANT_DISABLE_STRICT_DEPENDENCY_ENFORCEMENT=1 vagrant plugin install vagrant-libvirt
fi

FILE=vagrant-insecure
if [[ ! -e $HOME/.ssh/$FILE ]] ; then
    echo "NOTE: Copying vagrant insecure key to $HOME/.ssh"
    DIR=$HOME/.ssh
    if [[ ! -d $DIR ]] ; then
        mkdir -p $DIR
        chmod 700 $DIR
    fi
    cp $HERE/$FILE $HOME/.ssh/$FILE
    chmod 600 $HOME/.ssh/$FILE
fi

if ! (cat /etc/group | grep -q vagrant 2> /dev/null) ; then
    echo "ERROR: The group 'vagrant' is not present. Please run the following:"
    echo "       sudo groupadd vagrant"
    EXIT_FLAG=1
fi

if ! (groups | grep -q vagrant 2> /dev/null) ; then
    echo "ERROR: Current user is not a member of the group 'vagrant'. Please run the following:"
    echo "sudo usermod $(whois) -aG vagrant"
    EXIT_FLAG=1
fi

if ! (which exportfs 2>/dev/null) ; then
    echo "ERROR: exportfs binary not found. Please install nfs-server."
    echo "Ubuntu:   sudo apt install nfs-kernel-server"
    echo "Arch:     sudo pacman -S nfs-utils"
    EXIT_FLAG=1
fi

if ! (sudo exportfs -ar 2>/dev/null)  ; then
    echo "Sudoers rules for the group vagrant not detected. Run the following to fix it."
    echo ''
    echo 'cat <<EOF | sudo tee /etc/sudoers.d/vagrant-nfs &>/dev/null'
    echo 'Cmnd_Alias VAGRANT_EXPORTS_CHOWN = /bin/chown 0\:0 /tmp/*'
    echo 'Cmnd_Alias VAGRANT_EXPORTS_MV = /bin/mv -f /tmp/* /etc/exports'
    echo 'Cmnd_Alias VAGRANT_NFSD_CHECK = /usr/bin/systemctl status --no-pager nfs-server.service'
    echo 'Cmnd_Alias VAGRANT_NFSD_START = /usr/bin/systemctl start nfs-server.service'
    echo 'Cmnd_Alias VAGRANT_NFSD_APPLY = /usr/sbin/exportfs -ar'
    echo '%vagrant ALL=(root) NOPASSWD: VAGRANT_EXPORTS_CHOWN, VAGRANT_EXPORTS_MV, VAGRANT_NFSD_CHECK, VAGRANT_NFSD_START, VAGRANT_NFSD_APPLY'
    echo 'EOF'
    EXIT_FLAG=1
fi

[[ $EXIT_FLAG -ne 0 ]] && exit 1

# ET no phone home - https://www.vagrantup.com/docs/other/environmental-variables#vagrant_checkpoint_disable
export CHECKPOINT_DISABLE=true
time vagrant up

# Install kubeconfig file if a new one is found
CUR_CONFIG=$HOME/.kube/config
NEW_CONFIG="$(readlink -f $(dirname $0)/../temp/config)"
if [[ -e $NEW_CONFIG ]] ; then
    # if current config is a symlinnk, just replace it
    if [[ -L $CUR_CONFIG ]] ; then
        rm $CUR_CONFIG
    else
        mv $CUR_CONFIG ${CUR_CONFIG}.$(date +%Y%mi%d-%H%M%S)
    fi
    ln -s $NEW_CONFIG $CUR_CONFIG
fi

# Evaluate variables as used by Vagrant provisioning scripts
source $HERE/../scripts/env
source $HERE/../temp/proxy_ip

# Linefeed
echo

# Check if registry is resolvable
if (grep -q $LOCAL_REGISTRY /etc/hosts) ; then
    echo "INFO: registry.local was checked and found in /etc/hosts"
else
    echo "WARNING: The url of local registry was not found in /etc/hosts."
    echo "         In order to push/pull on that registry, do the following:"
    echo "         echo '$PROXY_IP    $LOCAL_REGISTRY' | sudo tee -a /etc/hosts"
fi

# Check if CA is installed in host system
if ! (which openssl >/dev/null) ; then
    echo "WARNING: Openssl is not installed. Unable to verify if CA certificate is installed correctly."
else
    if (openssl verify ${HERE}/../temp/cache/registry/${LOCAL_REGISTRY}/${LOCAL_REGISTRY}.pem > /dev/null) ; then
        echo "INFO: CA cert seems to be installed correctly."
    else
        echo "WARNING: CA cert is not installed (correctly)."
        echo "Please run the script support/cert.sh."
    fi
fi

check_installed() {
    for FILE in $@ ; do
        ! (which $FILE &>/dev/null ) && echo -n "$FILE "
    done
}

LIST=$(check_installed fzf kubectx kubectl kubens)
if [[ -n $LIST ]] ; then
    echo
    echo "WARNING: It is recommended to install the following packages localy / on your laptop: $LIST"
    echo "  https://github.com/junegunn/fzf"
    echo "  https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/"
    echo "  https://github.com/ahmetb/kubectx"
    echo ""
    echo "kubectl:"
    echo '  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" '
    echo ''
    echo 'kubectx / kubens:'
    echo '  DIR=/tmp/kubectx'
    echo '  git clone https://github.com/ahmetb/kubectx $DIR'
    echo '  sudo cp $DIR/kubectx /usr/local/bin'
    echo '  sudo cp $DIR/kubens /usr/local/bin'
    echo '  sudo cp $DIR/kubectx/completion/*.bash /etc/bash_completion.d'
fi

echo
echo "Finished on $(date)"
echo
