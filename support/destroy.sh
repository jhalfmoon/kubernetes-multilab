#!/bin/bash

HERE=$(dirname $(readlink -f $0))
. $HERE/get-name.sh

# Note: $VM_TYPE was returned by get-name
if [[ -n $VM_TYPE ]] ; then
    for TYPE in $VM_TYPE ; do
        echo "Stopping instance of $TYPE..."
        cd $HERE/../$TYPE
        vagrant destroy --force
        #vagrant destroy --force --parallel
    done
fi
