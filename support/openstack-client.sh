# source me
#
# Create a temporary openstack client installation

DIR=/tmp/os

[[ -d $DIR ]] || mkdir -p $DIR
cd $DIR
if [[ -d venv ]] ; then
    source venv/bin/activate
else
    python -m venv venv
    source venv/bin/activate
    pip install python-openstackclient
fi

source <(openstack complete)
source $HOME/k/temp/os-tokenrc

