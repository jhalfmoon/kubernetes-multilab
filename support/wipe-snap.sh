#!/bin/bash

# Get vm name from vagrantfile
HERE=$(dirname $(readlink -f $0))
. $HERE/get-name.sh

# Default snapshot name is 'clean' unless a name is given as a parameter
NAME=clean
[[ -n $1 ]] && NAME=$1

cd $HERE/../$VM_TYPE
vagrant status | grep "$VM_TYPE[0-9]\+" | awk '{print $1}' | while read VM; do
    echo "Deleting snapshot '$NAME' for node '$VM'"
    virsh snapshot-delete --snapshotname $NAME --domain $VM
done

# Get list of vms
DOMS=$(virsh list --all --name | grep $VM_TYPE)
if [[ -z $DOMS ]] ; then
    echo "No vms found with prefix $VM_TYPE"
else
    echo -n "Found VMs: "
    echo $DOMS
    # Delete snapshots and then stop and delete domains
    echo "$DOMS" | while read DOM ; do
        echo "--- Domain $DOM"
        SNAPS=$(virsh snapshot-list $DOM --name)
        if [[ -z $SNAPS ]] ; then
            echo "No snapshots found for $DOM"
        else
            echo "Found $(echo $SNAPS | wc -w) snapshots for $DOM"
            echo "$SNAPS" | while read SNAP ; do
                virsh snapshot-delete --domain $DOM $SNAP
            done
        fi
    done
fi
