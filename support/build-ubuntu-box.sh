#!/bin/bash -u

# Tool to build ubuntu 22.04 LTF from the bento repo. Specifically to build the vagrant
# box for use with libvirt. It patches the bento build file for ubuntu:
#   * nfs client tools are installed, so that it does not happen on each boot
#   * the on-boot auto-type sequence is fixed, as it is horribly broken by default
#   * the installer is given more memory, to improve installation speed
#   * the disply is un-hidden to ease any debugging

NAME=ubuntu-22.04
PLATFORM=x86_64

HERE=$(dirname $(readlink -f $0))
DESTDIR=${1:-/tmp/packer-build}

FREE=$(df -BG --output=avail ${DESTDIR} | tr -d '[:alpha:]' | tr -d ' ' | tail -n1)
if [[ $FREE -lt 32 ]] ; then
    echo
    echo "WARNING: Working folder $DESTDIR has $FREE GB free space."
    echo "This might not be enough to store the intermediate images."
    echo
fi

echo "Note that this build script can break due to upstream changes in the bento templates."
echo "This script patches those templates in an attempt to not break. If it does break, then"
echo "you will need to debug the situation and fix the patch files."
echo

#echo "Press enter to continue..."
#read

CheckInstalled() {
    STATUS=0
    for ITEM in $@ ; do
        if (which $ITEM &> /dev/null) ; then
            echo "$ITEM binary is available."
        else
            echo "$ITEM is not installed. Please install it."
            STATUS=1
        fi
    done
    return $STATUS
}

CheckInstalled git packer vagrant qemu-system-x86_64 || exit 1

[[ -d $DESTDIR ]] || mkdir -p $DESTDIR
cd $DESTDIR

[[ ! -d bento ]] && git clone https://github.com/chef/bento.git
cd bento

git reset --hard HEAD
git checkout main
git pull
# Use most recent known-good commit; The commits after this break ubuntu 22.04 installer d.d. 2024-06-27
#git checkout db7858d
BEHIND=$(git rev-list --left-right HEAD...main | grep -c '>')
if [[ $BEHIND -ne 0 ]] ; then
    echo
    echo "NOTE: This repo is using a commit that is $BEHIND commmits behind master."
    echo "      You might want to update this script to use a more current commit."
    echo
    sleep 5
fi

# Configure the build scripts to add nfs client packages
patch -p1 --forward < $HERE/build-ubuntu-box-generic.patch

echo
echo "=== Starting packer build."
echo "=== NOTE: Your console output may remain quiet for a long time."
echo

# Build libvirt image
unset http_proxy https_proxy HTTP_PROXY HTTPS_PROXY

# If running this from a remote shell, be sure to set headless=true, or building will fail.
if [[ -z $DISPLAY ]] ; then
    echo "WARNING: The variable DISPLAY is not set, so enabling HEADLESS mode for packer."
    HEADLESS=true
else
    HEADLESS=false
fi

time packer init -upgrade packer_templates

# Use this for debugging
#export PACKER_LOG=1

time packer build \
    -only 'qemu.vm' \
    -var  'cpus=4' \
    -var  'memory=4096' \
    -var  'qemu_display=gtk' \
    -var  headless="$HEADLESS" \
    -var-file "os_pkrvars/ubuntu/${NAME}-${PLATFORM}.pkrvars.hcl" \
    ./packer_templates
    # -var 'iso_checksum=sha256:10f19c5b2b8d6db711582e0e27f5116296c34fe4b313ba45f9b201a5007056cb' \
    # -var 'iso_name=ubuntu-22.04.1-live-server-amd64.iso'' \
    # -var 'name=ubuntu-22.04' \

# recompress image from zlib to zstd, for higher decompression speed and smaller image size
OLDBOX=../${NAME}-${PLATFORM}.libvirt.box
NEWBOX=../${NAME}-${PLATFORM}.zst.libvirt.box
TEMPDIR=${NAME}-${PLATFORM}.libvirt.box.dir
cd builds
pwd
[[ -d $TEMPDIR ]] && rm -rf $TEMPDIR
mkdir -p $TEMPDIR
cd $TEMPDIR

echo "= Extracting box contents (qemu image and metadata files)..."
tar xf $OLDBOX

echo "= Re-compressing qemu image from zlib to zstd..."
# NOTE: Bigger cluster size gives significant compression gains, but als significant higher
#       libvirt vm startup times, for unknown reasons. 64K seems to be the sweet spot.

qemu-img convert -pc -O qcow2 -o preallocation=off,cluster_size=64k,compression_type=zstd *.img box.img.zst
mv -f box.img.zst *.img

echo "= Rebuilding vagrant box..."
tar cf $NEWBOX .
ls -alh ..
echo = Removing working files...
rm -rf $TEMPDIR
rm -rf $OLDBOX
export VAGRANT_CHECKPOINT_DISABLE=1

echo "= Attempting to add new box to vagrant (this may fail if the box already exists)"
if ! (vagrant box add --name bento-$NAME $NEWBOX) ; then
    echo "The command 'vagrant box add' has failed. If you need to re-add an existing box, you could do the following:"
    echo
    echo "vagrant box remove bento-$NAME --provider libvirt"

    # This bit here generates the commands that can be used to delete the bento libvirt image(s)
    POOLS=$(virsh pool-list --name)
    for POOL in $POOLS; do
        VOLS=$(virsh vol-list $POOL | tail -n+3)
        while read VOL ; do
            VOL=$(echo $VOL | awk '{print $1}')
            if [[ "$VOL" =~ "bento" ]] ; then
                echo  virsh vol-delete $VOL $POOL
            fi
        done < <(echo "$VOLS")
    done

    echo vagrant box add --name bento-${NAME} $(readlink -f ${NEWBOX})
fi

# or: Build Virtualbox image
#packer build -only=virtualbox-iso $NAME-amd64.json
#export VAGRANT_CHECKPOINT_DISABLE=1
#VAGRANT_CHECKPOINT_DISABLE=1 vagrant box add --name bento-$NAME ../../builds/$NAME.virtualbox.box

