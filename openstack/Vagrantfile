# vi: set ft=ruby :

$nodes          = 2
$vm_cpus        = 4
$vm_memory      = 8192
$ip_range       = "10.0.20"
$ip_range2      = "10.0.40"
$ip_start       = 20
$vm_name_prefix = "openstack"

# https://stackoverflow.com/questions/40270391/shell-environment-variables-in-vagrant-files-are-only-passed-on-first-up
$set_vars = <<SCRIPT
tee -a "/etc/profile.d/100-vagrant.sh" > "/dev/null" <<"EOF"
export CACHE_NAME=$VM_NAME_PREFIX
source /vagrant/scripts/env
source /vagrant/scripts/bashrc
# NOTE: The token file gets generated during provisioning of the controller node
[[ -e $STACK_TOKEN_FILE ]] && source $STACK_TOKEN_FILE
EOF
SCRIPT

Vagrant.configure("2") do |config|
  # Bento Ubuntu images are not available pre-built for libvirt. See readme on how to build your own.
  config.vm.box = "bento-ubuntu-22.04"

  config.vm.box_check_update = false
  config.ssh.insert_key = false
  config.vm.provider :libvirt do |lv|
    lv.cpus = $vm_cpus
    # NOTE: passthrough makes nested qemu VMs orders of magnitude faster
    lv.cpu_mode = 'host-passthrough'
    lv.memory = $vm_memory
    lv.graphics_type = "none"
    lv.default_prefix = ''
  end

  # NFS share options use no_root_squash to prevent chown errors when mounted in docker
  #   https://stackoverflow.com/questions/56126490/docker-run-on-nfs-mount-causes-fail-to-copy-and-fail-to-chown
  #   https://www.vagrantup.com/docs/synced-folders/nfs
  config.vm.synced_folder "..", "/vagrant", disabled: false, type: "nfs", nfs_version: 4, nfs_udp: false,
    linux__nfs_options: [ 'rw','no_root_squash','no_subtree_check','anonuid=1000','anongid=1000' ]
  #
  # If you want to have extra folders from your laptop mapped to the VM, do it here:
  #config.vm.synced_folder "/1/temp/vagrant_project", "/work", disabled: true, type: "nfs", nfs_version: 4, nfs_udp: false,
  #  linux__nfs_options: [ 'rw','no_root_squash','no_subtree_check','anonuid=1000','anongid=1000' ]

  # inject environment variables into the vm, including Vagrant variables
  config.vm.provision :shell, inline: "echo 'export VM_NAME_PREFIX=" + $vm_name_prefix + "' > /etc/profile.d/100-vagrant.sh"
  #config.vm.provision :shell, inline: "echo 'export IP_ADDR=" + $vm_ip_range + "." + $ip_start +"' >> /etc/profile.d/vagrant-env.sh"
  config.vm.provision :shell, privileged: true, inline: $set_vars, run: "always"
  config.vm.provision :shell, privileged: true, path: "../scripts/flag-clear-all.sh"

  (1..$nodes).each do |i|
    config.vm.define $vm_name_prefix + "-#{i}", autostart: true, primary: true do |node|
      node.vm.hostname = $vm_name_prefix + "-#{i}.mine.local"
      node.vm.network :private_network, ip: $ip_range  + ".#{$ip_start+i}", nic_type: "virtio"
      node.vm.network :private_network, ip: $ip_range2 + ".#{$ip_start+i}", nic_type: "virtio"
      if i == 1
        # Uncomment the following line if you want the workers to always wait for the controller to finish provisioning.
        # Leaving it commented will make the workers provision in parallel, but the cache may be out of sync with the controller.
        #
        node.vm.provision :shell, privileged: true,  path: "../scripts/package-restore.sh"
        node.vm.provision :shell, privileged: false, path: "scripts/stack-restore.sh"
        node.vm.provision :shell, privileged: false, path: "scripts/install.sh", args: [ "#{i}", $nodes ]
        node.vm.provision :shell, privileged: false, path: "scripts/cinder-ramdisk.sh"
        node.vm.provision :shell, privileged: false, path: "scripts/token-generate.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/package-save.sh"
        node.vm.provision :shell, privileged: false, path: "scripts/stack-save.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/flag-set.sh", args: "controller"
        node.vm.provision :shell, privileged: true,  path: "../scripts/clean.sh"
      else
        node.vm.provision :shell, privileged: true,  path: "../scripts/flag-wait.sh", args: "controller"
        node.vm.provision :shell, privileged: true,  path: "../scripts/package-restore.sh"
        node.vm.provision :shell, privileged: false, path: "scripts/stack-restore.sh"
        node.vm.provision :shell, privileged: false, path: "scripts/install.sh", args: [ "#{i}", $nodes ]
        node.vm.provision :shell, privileged: false, path: "scripts/cinder-ramdisk.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/clean.sh"
      end
    end
  end
end
