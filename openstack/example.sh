#!/bin/bash

# Example script showing how you could set up an ephemeral client and control open stack with it.

HERE=$(dirname $(readlink -f $0))

USER=beep
KEY=/1/store/keys/ssh/beep-20240306.pub
IMAGE_PATH=/1/temp/vm/cloud-images
IMAGE=jammy-server-cloudimg-amd64.img

# Add any Nova nodes that are not yet part of the cluster after provisioning
ssh k1 -- 'nova-manage cell_v2 discover_hosts --verbose'

# NOTE: bash is explicitely started to ensure running profile and thus having environment variables loaded
ssh k1 bash --login -i '/vagrant/openstack/scripts/token-generate.sh'

# Install the openstack client in temporary python virtual env
source $HERE/../support/openstack-client.sh

cd /tmp
cp $IMAGE_PATH/$IMAGE .

# customize image
#
#virt-customize -a $IMAGE --run-command 'useradd -m foo'
#virt-customize -a $IMAGE --run-command 'echo foo:123 | chpasswd'
virt-customize -a $IMAGE --run-command 'echo root:123 | chpasswd'
virt-customize -a $IMAGE --run-command 'systemctl disable systemd-networkd-wait-online.service'
#virt-customize -a $IMAGE --run-command 'snap refresh ; snap list --all | awk '/disabled/{print $1" --revision "$3}' | xargs -rn3 snap remove'
#virt-customize -a $IMAGE --run-command 'snap refresh ; LANG=C snap list --all | while read snapname ver rev trk pub notes; do if [[ $notes = *disabled* ]]; then sudo snap remove "$snapname" --revision="$rev"; fi; done'
virt-customize -a $IMAGE --install qemu-guest-agent --run-command 'systemctl enable qemu-guest-agent.service'
virt-customize -a $IMAGE --mkdir /var/lib/cloud/seed/nocloud/ --upload $HERE/user-data:/var/lib/cloud/seed/nocloud/user-data
virt-customize -a $IMAGE --mkdir /var/lib/cloud/seed/nocloud/ --upload $HERE/meta-data:/var/lib/cloud/seed/nocloud/meta-data

# remove existing instances
openstack server delete ubuntu-1
openstack image delete ubuntu
openstack flavor delete xx.mine

# create new instances
openstack flavor create xx.mine --id xx.mine --ram 1024 --disk 10 --vcpus 4
openstack image create --disk-format qcow2 --container-format bare --public --file $IMAGE ubuntu
openstack server create --image ubuntu --flavor xx.mine --network public ubuntu-1

# ssh into the control node. This forwards a few ports, allowing you to access Horizon on the guest machine
ssh k1
