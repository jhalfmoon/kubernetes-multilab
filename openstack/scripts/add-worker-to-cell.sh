#!/bin/bash -eux
#
# Add worker to openstack cell

if [[ -e $STACK_TOKEN_FILE ]] ; then
    source $STACK_TOKEN_FILE
    for COUNT in $(seq 1 10) ; do
        openstack compute service list
        sleep 2
    done
#    ./tools/discover_hosts.sh
echo
    echo "ERROR: $TOKEN_FILE does not exist. Unable to add node to cells."
    exit 1
fi
