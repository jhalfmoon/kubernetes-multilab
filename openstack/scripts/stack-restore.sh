#!/bin/bash -eux
#
# Restore cached files to minimize bandwidth usage and improve installation speed

# ensure base dir exists
[[ -d $STACK_BASE ]] || sudo mkdir -p $STACK_BASE
sudo chown vagrant $STACK_BASE

clone https://git.openstack.org/openstack-dev/devstack os-devstack $DEVSTACK_BASE

# restore cached devstack files
STACK_FILE_DIR=$DEVSTACK_BASE/files
[[ -d $STACK_FILE_DIR ]] || sudo mkdir -p $STACK_FILE_DIR
sudo chown -R vagrant $STACK_FILE_DIR
if [[ -e $STACK_FILE_CACHE ]] ; then
    tar xaf $STACK_FILE_CACHE -C $STACK_FILE_DIR
fi

# unpack cached openstack repos
if [[ -e $STACK_REPO_CACHE ]] ; then
    tar xaf $STACK_REPO_CACHE -C $STACK_BASE
fi

# unpack cached pip cache
if [[ -e $STACK_PIP_CACHE ]] ; then
    [[ -d $HOME/.cache ]] || mkdir -p $HOME/.cache
    tar xaf $STACK_PIP_CACHE -C $HOME/.cache
fi

