#!/bin/bash -eux
#
# Generate a token to be used on the command line

cat <<EOF > $STACK_OPENRC_FILE
export OS_AUTH_URL=http://$IP/identity/v3
export OS_PROJECT_NAME=admin
export OS_PROJECT_DOMAIN_NAME=Default
#
export OS_USER_DOMAIN_NAME=Default
export OS_USERNAME=admin
export OS_PASSWORD=labstack
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
EOF

# Unload the following variables. If present, they will maken the "openstack token" command fail.
export -n OS_AUTH_TYPE OS_TOKEN

# Generate token
# https://docs.openstack.org/security-guide/identity/tokens.html
# https://help.ovhcloud.com/csm/en-gb-public-cloud-compute-api-rate-limits?id=kb_article_view&sysparm_article=KB0058581
source $STACK_OPENRC_FILE
TOKEN=$(openstack token issue -f value -c id)

# NOTE: The variable $IP is unique for each node running this script. It is set
#       in /vagrant/scripts/bashrc by parsing the IP address from eth1.
cat <<EOF > $STACK_TOKEN_FILE
export OS_AUTH_URL="http://$IP/identity/v3"
export OS_PROJECT_NAME=demo
export OS_PROJECT_DOMAIN_NAME=default
#
export OS_AUTH_TYPE=v3token
export OS_TOKEN=${TOKEN}
EOF

#export | grep OS_ | awk '{print $3}' | cut -d= -f1 | while read ding ; do export -n $ding ; done
#source $STACK_TOKEN_FILE

