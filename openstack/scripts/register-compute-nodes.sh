#!/bin/bash
#
# Discover compute nodes and register them with a parent cell in Nova

if [[ -e $STACK_TOKEN_FILE ]] ; then
    source $STACK_TOKEN_FILE
    #for COUNT in $(seq 1 10) ; do
    #    openstack compute service list
    #    sleep 2
    #done
    nova-manage cell_v2 discover_hosts --verbose
echo
    echo "ERROR: $TOKEN_FILE does not exist. Unable to add node to cells."
    exit 1
fi
