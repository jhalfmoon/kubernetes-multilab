#!/bin/bash -eux
#
# Store files to cache for repeated installations

# cache pip files
echo "INFO: Caching pip packages located in $HOME/.cache/pip"
tar caf $STACK_PIP_CACHE -C $HOME/.cache pip

# cache openstack repos
echo "INFO: Caching openstack repos located in $STACK_BASE"
tar caf $STACK_REPO_CACHE -C $STACK_BASE $STACK_REPO_LIST

# cache loose files
LIST=$(cd $STACK_BASE/devstack/files ; ls $STACK_FILE_LIST)
if [[ -n "$LIST" ]] ; then
    echo "INFO: Caching openstack file located in $STACK_BASE/stack/files"
    tar caf $STACK_FILE_CACHE -C $DEVSTACK_BASE/files $LIST
else
    echo "INFO: No files were matched, so no files will be cached."
fi

