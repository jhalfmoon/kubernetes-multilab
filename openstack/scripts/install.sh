#!/bin/bash -eux

if [[ $# -ne 2 ]] ; then
    echo "ERROR: This script requires 2 parameters but got $# ."
    exit 1
fi

NODE_INDEX=$1
NODE_COUNT=$2

STACK_CONFIG_DIR=/vagrant/openstack/config

# configure devstack and run it
cd $DEVSTACK_BASE
git clean -fxd
git pull
case $NODE_INDEX in
    1)
        if [[ $NODE_COUNT -eq 1 ]] ; then
            cp $STACK_CONFIG_DIR/local.conf.single local.conf
            # FIXME: local.sh in devstack docs is outdated
            # cp $STACK_CONFIG_DIR/local.sh .
        else
            cp $STACK_CONFIG_DIR/local.conf.controller local.conf
        fi
        ;;
    *)
        cp $STACK_CONFIG_DIR/local.conf.worker.$(($NODE_INDEX-1)) local.conf
        ./stack.sh
        ;;
esac

./stack.sh

# discover compute nodes and register them with a parent cell in Nova
#nova-manage cell_v2 discover_hosts --verbose

# Configure autocompletion and extra package(s)
sudo apt install -y bash-completion mlocate
openstack complete | sudo tee /etc/bash_completion.d/osc.bash_completion > /dev/null
sudo updatedb

cat >>.bash_profile <<EOF
echo "To generate and use a token:"
echo
echo "/vagrant/openstack/scripts/token-generate.sh"
echo "source /vagrant/temp/os-tokenrc"
echo ""
EOF

