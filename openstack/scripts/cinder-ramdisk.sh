#!/bin/bash -eux
#
# Add an zstd compressed RAM disk to the volumegroup used by cinder

SIZE=10G
# Node number 99 is used because the lower numbers have shown to be in use if openstack is deployed
DEV=/dev/nbd99
VG=stack-volumes-lvmdriver-1

sudo apt install -y nbdkit nbd-client

# create block device in RAM
nbdkit -U /tmp/sock memory $SIZE allocator=zstd
sudo modprobe nbd
sudo nbd-client -unix /tmp/sock $DEV

# allow it to be added to lvm
sudo sed -i '/global_filter/ s@ \[ @ \[ "a|nbd|", @g' /etc/lvm/lvm.conf

# add it
sudo wipefs $DEV
sudo pvcreate $DEV
sudo vgextend $VG $DEV

