Kubernetes / Vagrant / Libvirt lab
##################################

A fast and flexible sandbox for experimenting with the several environments:

* k8s
* k3s
* k3d
* Kind
* Minikube
* Openstack

Features
========

* Fast re-provisioning: Container images, packages and other downloads are cached on the VM host and used on subsequent provision runs.
* Automatically configures the host system kubeconfig for direct access to the kube API running on the VM.
* All setups provide both a local container registry and a caching http proxy.
* Support scripts are available to snapshot (snap) and restore (pop) the VM images, for quick cluster create and destroy cycles.
* VMs are provided with kubectx, kubens and fzf and command auto completion for often used command line tools such as docker, k3d and k.
* Support for the self signed certificates is included; no insecure access needed.
* Base VM image is based on Chef's Bento templates, with pre-installed nfs-client packages and updated snapd packages, for fast and low bandwidth provisioning.

Requirements
============

* libvirt / qemu
* Vagrant with libvirt plugin
* nfs-server, used by Vagrant file sharing
* Hashicorp Packer, to build the bento ubuntu image for libvirt, as there is no libvirt version of that vagrant box available online
* kubectl - Alternatively ssh into the one othe VMs and use the supplied kubectl

Getting started
===============

* Install the requirements listed above.
* Install Vagrant libvirt plugin - "vagrant plugin install vagrant-libvirt". Optionally add VAGRANT_DISABLE_STRICT_DEPENDENCY_ENFORCEMENT=1 as the plugin installation sometimes fails due to drifting dependencies.

* Optional: Build the ubuntu libvirt vagrant box image using support/build-vagrant-box.sh .
* Follow the steps given in the example code below.

HyperV / Windows 11
===================

Work in progress. Bento image build is working, but Vagrant currently breaks on linefeeds. See hyperv.rst file for details.

Caching http proxy
==================

The supplied setups are configured to use a caching http proxy to speed up re-provisioning and to prevent hitting container registry rate-limiting. The proxy runs on the kubernetes master node or, in the case of k3d, on the docker host VM. To see which registies get cached, check scripts/proxy-install.sh and search for the string 'REGISTRIES'.

Notes
=====
* When using nftables, ensure that you have rules that allow NFS, SSH, DHCP and DNS traffic::

  chain input {
    ...
    iifname vmap { virbr* : jump my_libvirt }
    ...
  }

  chain my_libvirt {
        ip protocol . th dport vmap { tcp . 22 : accept, tcp . 2049 : accept, udp . 53 : accept, tcp . 53 : accept, udp . 67 : accept}
  }

* Installation help for ubuntu systems::

    sudo apt install vagrant qemu-kvm libvirt-daemon-system libvirt-dev packer build-essentials nfs-kernel-server libnss3-tools
    sudo usermod -aG libvirt,vagrant $(whoami)
    newgrp libvirt                                                                                                                                     newgrp vagrant
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x kubectl
    sudo mv kubectl /usr/local/bin

* Prebuilt Bento images for libvirt are not available. A script called support/build-vagrant-box.sh can do that for you. Bento sometimes lags behind a bit in updating the links to the image downloads. As a consequence, you might sometimes need to manual update / adjust the packer build files to match current download URLs. The supplied script contains a patch to fix a current bug in the ubuntu config file in bento.

* At the time of writing (2021-09-22), CKA uses Ubuntu 18 LTS on the exam. But using Ubuntu 22.04 LTS makes the VMs boot and run significantly faster compared to Ubuntu 18 and there is no noticable difference in how k8s operates.

* The combination Vagrant / libvirt, although fast, can be a bit fussy at times. Be prepared to bump into some issues when updating vagrant / libvirt. Vagrant sometimes throws the following error: "Volume for domain is already created. Run the following to fix this : "source aliases ; wipe-libvirt"

* To wipe all caches and start with a complete clean repo, do: "source aliases ; wipe-git" . This makes use of the supplied support scripts.

* Be sure to examine and source the file named "aliases". It provides very useful aliases, such as info, up, halt, destroy, snap and pop. The latter two provide libvirt snapshots. Also, check out the support/support-functins.sh for very userful support tooling.

* Vagrant libvirt does not officially support snapshots, but: Two scripts are supplied that can make / restore snapshots of running VMs. Making snapshots of stopped VMs will fail. This is a know issue, but it won't be an issue in most situations.

* When using snapshots, you can safely ignore the following error: "Error when updating domain settings: Call to virDomainUndefineFlags failed". It's a Vagrant / libvirt thing when using snapshots.

* To see what traffic the caching proxy is receiving, ssh into the first node ('ssh k1') and run /vagrant/support/tail.sh .

* To make your local Linux host automatically resolve all .localhost addresses to 127.0.0.1 you might want to check out https://www.freedesktop.org/software/systemd/man/nss-myhostname.html . It is part of the package systemd-libs and might need some configuaratino in /etc/nsswithch.conf.

* Vagrant is built with Ruby and when Ruby is updated, Vagrant plugins have a habit of breaking. The fix is usually a plugin reinstall. Ale note theat due to a bug in vagrant-libvirt 0.8, vagrant will fail to bring additional NICs up. Version 0.12.2 is out since 2023-6-24, so be sure to use at least that version::

    vagrant plugin expunge --reinstall
    # or
    vagrant plugin install vagrant-libvirt --plugin-version 0.11.2

SSH
===

For easy access to the guest VM(s) the following ssh config is recommended ::

    Host k1
      Hostname 10.0.20.21
      # generic webserver ports
      LocalForward 8080 0:80
      LocalForward 8443 0:443
      # Generic forwards, when using k3d for example
      LocalForward 8081 0:8081
      LocalForward 8082 0:8082
      # ports used for mitmproxy gui in debug mode by rpardini/docker-registry-proxy, see https://mitmproxy.org/
      # note: mitm proxy uses 8081/8082 by default, but to keep those free for other use, they're forwarded to 809x
      LocalForward 8091 0:8091
      LocalForward 8092 0:8092
    Host k2
      Hostname 10.0.20.22
    Host k3
      Hostname 10.0.20.23
    Host k1 k2 k3 k4 k5
      User vagrant
      Compression no
      StrictHostKeyChecking no
      Identityfile /some/path/keys/ssh/vagrant-insecure
      # disable 'key changed' warnings
      UserKnownHostsFile /dev/null
      # disable message: "Warning: Permanently added 'foo' (ECDSA) to the list of known hosts."
      LogLevel ERROR
      ControlPersist 5m
      ControlMaster yes
      ConnectTimeout 2

k8s
===

Example use::

    source support/aliases
    source support/support-functions.sh
    cd k8s
    destroy ; up ; snap
    kubectl get nodes

    ssh k1
    #show logs of caching proxy
    docker logs -f $(docker container ls | grep registry-proxy | awk '{print $1}')
    exit

    # show status of VMs and snapshots
    info

    # restore snapshot called 'clean'
    pop

    # make a snapshot called 'v2'
    snap v2

    # restore snapshot called 'clean'
    pop ; ssh k1
    exit

    # stop VMs
    halt

    # delete all VMs
    destroy

k3d
===

Example use::

    source support/aliases
    source support/support-functions.sh
    cd k3d
    destroy ; up ; snap
    ssh k1

    # show proxy logs
    docker logs -f $(docker container ls | grep registry-proxy | awk '{print $1}')

    # enter k3s container
    docker exec -ti $(docker container ls | grep k3s | grep server-0 | awk '{print $1}' | head -n1) sh
    hostname

    # enter traefik container from within the docker container
    crictl exec -ti $(ctr c ls | sort -k2 | grep traefik | awk '{print $1}' | head -n1 | tail -n1 ) sh
    hostname
    exit
    exit

    # spin up a new cluster called 'foo1' , see /usr/local/bin/kk1 for more info
    kk1 foo1
    kubectl get nodes

    # spin up a cluster with a custom version of traefik and Calico instead of Flannnel
    kkx

    # delete all cluster
    kdel

    # spin up another cluster with more workers
    kk1 foo2 --agents=3
    kubectx
    kubens
    exit

    # kubectl also works on the host system
    kubectl get nodes

    # Start new cluster with custom traefik, calicao instead of flannel and metallbi instead of servicelb
    kdel ; kkx
    # monitor deployments
    kgaw -A

k3s
===

NOTE: Although the k3s master node does not need to have docker installed, as k3s supplies its own container runtime binary, docker is still installed to run the caching proxy container, as that needs to be running before bringing a k3s cluster up.

Example use::

    source support/aliases
    source support/support-functions.sh
    cd k3s
    destroy ; up ; snap
    kubectl get nodes

openstack
=========
Devstack is installed at /opt/stack/devstack.

For a significant speed-up of nested VMs:
* Set cpu type to host_passthrough in Vagrant / virtmanager
* Set the following options in /etc/modprobe.d/kvm
    options kvm_amd nested=1 avic=1 npt=1
  Source: https://forum.level1techs.com/t/svm-avic-iommu-avic-improves-interrupt-performance-on-zen-2-etc-based-processors/155226

To check your settings, run::

    ls /sys/module/kvm_amd/parameters/* | while read ding; do echo -n "$ding " ; cat $ding ; done

Example usage::

    ssh k1
    nova-manage cell_v2 discover_hosts --verbose

    IMAGE_DIR=/1/temp/vm/cloud-images

    # https://docs.openstack.org/newton/user-guide/common/cli-manage-images.html
    #
    cd /1/temp/vm/cloud-images
    # https://cloud-images.ubuntu.com/jammy/current/
    wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
    # https://alpinelinux.org/cloud/
    ALPINE_SHORT_VER=3.20
    ALPINE_VER=${ALPINE_SHORT_VER}.3
    wget https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_SHORT_VER}/releases/cloud/nocloud_alpine-${ALPINE_VER}-x86_64-bios-tiny-r0.qcow2

    pacman -S guestfs-tools

    USER=beep
    KEY=/1/store/keys/ssh/beep-20240306.pub
    IMAGE_PATH=/1/temp/vm/cloud-images
    IMAGE=jammy-server-cloudimg-amd64.img

    cd /tmp
    cp $IMAGE_PATH/$IMAGE .

    # https://totaldebug.uk/posts/proxmox-template-with-cloud-image-and-cloud-init/
    virt-customize -a $IMAGE --install qemu-guest-agent --run-command 'systemctl enable qemu-guest-agent.service'
    virt-customize -a $IMAGE --run-command "useradd $USER"
    virt-customize -a $IMAGE --run-command "echo -e '123\n123'| passwd $USER"
    virt-customize -a $IMAGE --run-command "mkdir -p /home/$USER/.ssh"
    virt-customize -a $IMAGE --ssh-inject $USER:file:/1/store/keys/ssh/beep-20240306.pub
    virt-customize -a $IMAGE --run-command "chown -R $USER:$USER /home/$USER"
    virt-customize -a $IMAGE --mkdir /var/lib/cloud/seed/nocloud/ --upload user-data:/var/lib/cloud/seed/nocloud/user-data
    virt-customize -a $IMAGE --mkdir /var/lib/cloud/seed/nocloud/ --upload meta-data:/var/lib/cloud/seed/nocloud/meta-data

    openstack image create --disk-format qcow2 --container-format bare --public --file $IMAGE                                                            ubuntu
    openstack image create --disk-format qcow2 --container-format bare --public --file $IMAGE_DIR/nocloud_alpine-${ALPINE_VER}-x86_64-bios-tiny-r0.qcow2 alpine-${ALPINE_VER}-image

    # https://docs.openstack.org/nova/rocky/admin/flavors.html
    openstack flavor create xx.mine --id xx.mine --ram 1024 --disk 10 --vcpus 4

    openstack server create --image ubuntu                     --flavor xx.mine --network public ubuntu-1
    openstack server create --image alpine-${ALPINE_VER}-image --flavor xx.mine --network public alpine-1
    openstack server create --image cirros-0.6.2-x86_64-disk   --flavor m1.tiny --network shared cirros-1

    source support/openstack-client.sh
    openstack server list
    openstack compute list
    openstack agent list
    openstack compute agent list
    openstack compute service list
    openstack server list

Manually mount qcow image howto
===============================

::

    DEV=/dev/nbd0
    MOUNT=/tmp/temp-mount
    IMAGE=jammy-server-cloudimg-amd64.img

    sudo modprobe nbd max_part=8
    sudo qemu-nbd --connect=$DEV $IMAGE

    sudo umount $MOUNT >/dev/null
    [[ -d $MOUNT ]] || mkdir -p $MOUNT
    sudo mount ${DEV}p1 $MOUNT

    sudo umount $MOUNT
    sudo qemu-nbd --disconnect $DEV
    sudo rmmod nbd

Helm
====

::
    NAME=bitnami
    URL=https://charts.bitnami.com/bitnami
    CHART=bitnami/nginx
    NS=test-ns

    helm repo add $NAME $URL
    helm repo list

    helm search repo $NAME
    helm search repo $NAME --versions
    helm show chart $CHART | less
    helm show values $CHART | less

    kubectl create ns $NS
    kubens $NS
    helm install my-release $CHART
    helm list

Links
=====

Documentation
-------------

* https://kubernetes.io
* https://k3s.io
* https://k3d.io
* https://minikube.sigs.k8s.io/docs
* https://kind.sigs.k8s.io/docs/user/quick-start
* https://docs.tigera.io/calico
* https://docs.openstack.org/devstack/latest/guides/single-machine.html
* https://docs.openstack.org/devstack/latest/guides/multinode-lab.html
* https://docs.openstack.org/devstack/latest/configuration.html
* https://docs.openstack.org/python-openstackclient/latest/

Source
------

* https://github.com/kubernetes/kubernetes
* https://github.com/k3s-io/k3s
* https://github.com/rancher/k3d
* https://github.com/kubernetes/minikube
* https://github.com/kubernetes-sigs/kind
* https://github.com/vagrant-libvirt/vagrant-libvirt
* https://github.com/chef/bento
* https://github.com/helm/helm
* https://github.com/ahmetb/kubectx
* https://github.com/docker/compose
* https://github.com/rpardini/docker-registry-proxy
* https://github.com/junegunn/fzf
* https://github.com/FiloSottile/mkcert
* https://github.com/projectcalico/calico
