#!/bin/bash -eu

# In case of issues with minikube:
#   * Use --alsologtostderr -v=8
#   * https://minikube.sigs.k8s.io/docs/handbook/troubleshooting/

# https://github.com/kubernetes/minikube/releases
#
# FILE=minikube-linux-amd64.tar.gz
# LINK=https://github.com/kubernetes/minikube/releases/download/v1.29.0/$FILE
# download $FILE $LINK
# DIR=/usr/local/bin
# tar xf $DL_DIR/$FILE minikube-linux-amd64 -C $DIR
# mv $DIR/minikube-linux-amd64 $DIR/minikube
# chmod +x $DIR/minikube
#
FILE=minikube
LINK=https://storage.googleapis.com/minikube/releases/$MINIKUBE_VERSION/minikube-linux-amd64
DEST=/usr/local/bin/minikube
download $FILE $LINK $DEST
sudo chmod +x $DEST

$DEST completion bash | sudo tee /etc/profile.d/minikube_completion.sh > /dev/null

# restore any previously stored minikube cache
# See https://minikube.sigs.k8s.io/docs/handbook/offline/
# "the none driver caches images directly into Docker rather than a separate disk cache."
# So a seperate step is taken in the Vagrantfile to restore / save docker images aswel.
FIXED_CACHE=$DL_DIR/minikube_cache
LOCAL_CACHE=$HOME/.minikube/cache
if [[ -d $FIXED_CACHE ]] ; then
    mkdir -p $LOCAL_CACHE
    rsync -r --progress --delete-before $FIXED_CACHE/ $LOCAL_CACHE
fi

# Note that mikikube uses the RepoDigests value to determine if it needs to download kicbase.
# If the kicbase image is loaded from file, the RepoDigests value will not be populated
# and minikube will down the ENTIRE image again, even if it is already pre-loaded by docker.
# The workaround for this is to just do a pull first. Nothing will be downloaded, but the
# RepoTags property will be set. This will not work in offline mode. For that situation
# you will need to use a local caching proxy - which is used in this project any way.
docker image list | grep kic | awk '{print $1,$2}' | tr ' ' : | while read IMAGE ; do docker pull $IMAGE; done

#export no_proxy=$no_proxy,$(minikube ip)
export -n HTTP_PROXY HTTPS_PROXY http_proxy https_proxy
unset HTTP_PROXY HTTPS_PROXY http_proxy https_proxy

minikube start

# store minikube cache to persistent storage
rsync -r --progress --delete-before $LOCAL_CACHE/ $FIXED_CACHE

# Minikube supports image dumps and restores. Here is how that could work:
#
#mkdir -p $IMAGE_DIR
#minikube image list | while read IMAGE ; do
#    echo $IMAGE
#    minikube image save $IMAGE $IMAGE_DIR/"${IMAGE//\//_}.docker-image"
#done
