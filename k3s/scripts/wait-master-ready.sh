#!/bin/bash -eu

while ! (kubectl get nodes &> /dev/null) ; do
    echo "=== Waiting for k3s master to come up"
    sleep 5
done

# alias cluster to 'local'
(which kubectx &> /dev/null) && kubectx local=.
