#!/bin/bash -e

# https://k3s.io/
#
# NOTE: If called with the parameter 'agent', then source the variables
# saved by the master installation. This will trigger an agent installation.

echo "=== Installing k3s"

# $TYPE defines the installation type to do, either 'agent' or otherwise defaults to master installation
TYPE="$1"

BIN_DIR=/usr/local/bin
SCRIPT='k3s-installer.sh'

# Get the IP address of the primary node, that the kubernetes API is listening on.
# The proxy_ip file always points to the master node, which is what is needed.
IP=$(cat $TEMP_DIR/proxy_ip | cut -d= -f2)

# Restore previously stored script and binary, if available
if [[ -e $DL_DIR/k3s ]] && [[ -e $DL_DIR/$SCRIPT ]] ; then
    sudo cp $DL_DIR/{k3s,$SCRIPT} $BIN_DIR
#    export INSTALL_K3S_SKIP_DOWNLOAD=true
else
    # never run curl with sudo
    curl -sSfL https://get.k3s.io -o $DL_DIR/$SCRIPT
    sudo cp $DL_DIR/$SCRIPT $BIN_DIR/$SCRIPT
fi

if [[ $TYPE = 'agent' ]] ; then
    source $K3S_JOIN_FILE
    INSTALLER_PARAM=""
else
# Add TLS SAN (subject alternate name) to server installation to allow the controlplane to be reachable from the VM host, ie. your laptop.
# If the SAN is not added, kubectl will throw an error stating that the cert in the kubeconfig does not match the IP address of the control node.
    INSTALLER_PARAM="--tls-san $IP"
fi

# Allow mortals to access the kubeconfig file in order to be able to use kubectl
export K3S_KUBECONFIG_MODE="644"

# If defined, then install the specific k3s version or the specified channel
if [[ -n $K3S_VERSION ]] ; then
    export INSTALL_K3S_VERSION="v${K3S_VERSION}+k3s1"
elif [[ -n $K3S_CHANNEL ]] ; then
    export INSTALL_K3S_CHANNEL=$K3S_CHANNEL
fi

# Run the k3s installer
if ! (cat $BIN_DIR/$SCRIPT | sudo -E sh -s - $INSTALLER_PARAM) ; then
    echo "ERROR: Running $BIN_DIR/$SCRIPT for k3s version $INSTALL_K3S_VERSION failed."
    echo "Check log output for possible reason of failure."
    echo "A common error is that the version or channel number is not valid / available."
    echo "For reference, see https://github.com/k3s-io/k3s/blob/master/channel.yaml"
    exit 1
fi

# Allow users in the group 'docker' to run crictl
DIR=/var/lib/rancher/k3s/agent
while ! [[ -d $DIR ]] ; do
    echo "--- Waiting for k3s to come up"
    sleep 5
done

# Allow user vagrant to run kubectl and ctr without sudo
[[ -e /var/lib/rancher/k3s/agent ]] && sudo chmod -R g+rwX /var/lib/rancher/k3s/agent && sudo chown :vagrant -R /var/lib/rancher/k3s/agent
[[ -e /run/k3s/containerd/containerd.sock ]] && sudo chown :vagrant /run/k3s/containerd/containerd.sock

# If this is a master installation, then save the kubeconfig and join variables for later use by agent install
if [[ $TYPE != 'agent' ]] ; then
    # make directory accesible to normal users to allow kubectx usage
    sudo chmod -R o+rws /etc/rancher
    [[ ! -d $HOME/.kube ]] && mkdir -p $HOME/.kube
    k3s kubectl config view --raw | sed "s/127\.0\.0\.1/$IP/g" > $HOME/.kube/config
    chmod 600 $HOME/.kube/config
    # copy kubeconfig to shared folder
    sudo cp $HOME/.kube/config $TEMP_DIR/config
    # create join file for agents
    cat <<EOF > $K3S_JOIN_FILE
export K3S_TOKEN=$(sudo cat /var/lib/rancher/k3s/server/node-token)
export K3S_URL=https://$IP:6443
EOF
fi

# Experimental: configure pull through cache
if false; then
sudo mkdir -p /etc/rancher/k3s
cat <<EOF | sudo tee /etc/rancher/k3s/registries.yaml
mirrors:
  registry.local:
    endpoint:
      - "https://registry.local"
configs:
  "registry.local":
#    auth:
#      username: xxxxxx
#      password: xxxxxx
    tls:
#      cert_file:
#      key_file:
      ca_file: /certs/registry.local-ca.pem
EOF
fi

# Store k3s binary to cache
cp $BIN_DIR/k3s $DL_DIR/
