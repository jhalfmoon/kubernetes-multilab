#!/bin/bash -eu

echo "=== Installing and configuring base system components"

# Verify that $IP is set and eth1 is up. This check needs to be done, becasue
# if there is an issue with eth1, things will fail silently and horribly.
# This can happen when for example a broken vagrant-libvirt is released, again.
# $IP is set via /etc/profile.d/vagrant.sh
if [[ -z $IP ]] ; then
    echo "ERROR: NIC eth1 does not have an IP address. Check link status (UP/DOWN):"
    ip link show
    exit 1
fi

# keep downloaded packages
echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/01keep-debs.conf

# disable ipv6, if it is causes issues
#echo 'Acquire::ForceIPv4 "true";' > /etc/apt/apt.conf.d/99force-ipv4

# TEST: Proxy was breaking install, so disable it
# source /vagrant/support/proxy_disable.sh
apt-get update

[[ $FLAG_UPDATE_SYSTEM -eq 1 ]] && apt-get upgrade -y

# Add some tools
#   kubectl / k3d a.o. require the bash-completion package for completion to function
apt-get install -y bash-completion mlocate jq

# install alias completion helper script
DIR=$DL_DIR/completion.git
[[ ! -d $DIR ]] && git clone https://github.com/cykerway/complete-alias.git $DIR
cp -r $DIR/complete_alias /etc/profile.d/complete_alias.sh

#=== k8s pre-install preparation

# Reference:
#   https://kubernetes.io/docs/setup/production-environment/container-runtimes/
#   https://docs.docker.com/config/daemon/
# no-swap is / was actually a k8s requirement

cp /vagrant/support/support-functions.sh /etc/profile.d/500-container-support-functions.sh

swapoff -a
[[ -e /swap.img ]] && rm -f /swap.img
sed -i '/\sswap\s/ s/^/#/' /etc/fstab

# Enable ip_vs to make kube_proxy use ipvs instead of iptables
# https://kubernetes.io/docs/concepts/services-networking/service/
# https://kubernetes.io/blog/2018/07/09/ipvs-based-in-cluster-load-balancing-deep-dive/
# https://www.digihunch.com/2020/11/ipvs-iptables-and-kube-proxy/
FILE=/etc/modules-load.d/k8s.conf
cat <<"EOF" > $FILE
overlay
br_netfilter
#ip_vs
EOF
cat $FILE | grep -v '^#' | xargs sudo modprobe

FILE=/etc/sysctl.d/k8s.conf
cat <<"EOF" > $FILE
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
# fix issue with crossplane pods logging "failed to create fsnotify watcher: too many open files"
fs.inotify.max_user_watches     = 1024
fs.inotify.max_user_instances   = 200000
EOF

sysctl --system

# Add path for experimental scripts; Useful for temporary test scripts that you want to persist across vm re-deployments
echo 'export PATH=$PATH:/vagrant/temp/' > /etc/profile.d/010-vagrant-path.sh

## Experimental:
## ipv6 is disabled because registry-proxy will sometimes fail with the following error,
## when trying to connect to packages.cloud.google.com and docs.projectcalico.org:
## [crit] 88#88: *5 connect() to [2a03:b0c0:3:d0::d25:d001]:443 failed (99: Address not available) while connecting to upstream,
#net.ipv6.conf.all.disable_ipv6 = 1
#net.ipv6.conf.default.disable_ipv6 = 1

echo "--- Installing fzf"
#NOTE: fzf is used by kubens/kubectx as an input tool

[[ ! -d $DL_DIR/fzf.git ]] && git clone --depth 1 https://github.com/junegunn/fzf.git $DL_DIR/fzf.git
# install fzf for root and non-root user
sudo -u $SUDO_USER $DL_DIR/fzf.git/install --all

# Customize fzf settings
cat <<"EOF" > /etc/profile.d/fzf.sh
# From: https://betterprogramming.pub/boost-your-command-line-productivity-with-fuzzy-finder-985aa162ba5d
export FZF_DEFAULT_OPTS="
--layout=reverse
--info=inline
--height=50%
--border
--multi
--preview-window=:hidden
--preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--prompt='∼ ' --pointer='▶' --marker='✓'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-y:execute-silent(echo {+} | pbcopy)'
--bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
--bind 'ctrl-v:execute(code {+})'
"

DIR=/usr/share/fzf
if [[ -d $DIR ]] ; then
    source $DIR/completion.bash
    source $DIR/key-bindings.bash
fi

DIR=$HOME/.fzf/shell/completion.bash
if [[ -d $DIR ]] ; then
    source $DIR/completion.bash
    source $DIR/key-bindings.bash
fi
EOF

