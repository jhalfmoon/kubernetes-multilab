#!/bin/bash -eu

echo "=== Starting local docker registry"

# create registry storage folder and config file
#   https://docs.docker.com/registry/deploying/
#   https://docs.docker.com/registry/configuration/
DIR=$REGISTRY_DIR/$LOCAL_REGISTRY
[[ -d $DIR ]] || mkdir -p $DIR
cat <<EOF > $DIR/config.yml
version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
#  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
  secret: foobar
#  tls:
#    certificate: /cert/cert.pem
#    key: /cert/key.pem
health:
   storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
compatibility:
  schema1:
    enabled: false
EOF

# Create certificate and key (requires mkcert to be set up previously)
cd $DIR
mkcert $LOCAL_REGISTRY

# Start registry; to test it do 'curl localhost:5000/v2/_catalog'
docker run -d \
    --restart=always \
    --name $LOCAL_REGISTRY \
    -p 5000:5000 \
    -v $DIR:/var/lib/registry \
    -e REGISTRY_HTTP_ADDR=:5000 \
    -e REGISTRY_HTTP_TLS_CERTIFICATE=/var/lib/registry/$LOCAL_REGISTRY.pem \
    -e REGISTRY_HTTP_TLS_KEY=/var/lib/registry/$LOCAL_REGISTRY-key.pem \
    registry:2 /var/lib/registry/config.yml

# NOTE: The address of the registry should be registered in /etc/hosts.
#       That bit of scripting is done in the activation script for the proxy,
#       because that is run on every node.
