#!/bin/bash -eu

# This sets up a man-in-the-middle caching proxy and a local registry.
# Instead of defining seperate registry sources, the http(s)_proxy variable is set
# to intercept the registry queries and route / cache them. This proxy can also
# cache manifest requests. By default is supPROXY_PORT the docker, quay and gcr registries.
#
# See: https://github.com/rpardini/docker-registry-proxy

# To troubleshoot / monitor the logs of this container:
#   ssh k1
#   docker logs -f $(docker container ls | grep registry-proxy | awk '{print $1}') | grep 'MISS\|HIT'
# Used during testing
#   FOO=docker_registry_proxy; docker container stop $FOO; docker container rm $FOO

echo "=== Starting container image caching proxy (man-in-the-middle http proxy)"

mkdir -p $REGISTRY_DIR/{caching-proxy/cache,caching-proxy/certs} &> /dev/null

# Optional; use in case of debugging.
#
# This is a patch that prevents docker pulls from breaking when encountering a redirect.
# By default, the proxy does 'hidden redirects' which cause redirects to done by the proxy
# instead of sending a the redirect reply to the client. But the proxy still includes the
# location header in the reply, along with the redirected reply, causing docker to break,
# bescause it sees a location header, but no 30x status.
cat << "EOF" > $TEMP_DIR/nginx.manifest.common.conf.patched
    # nginx config fragment included in every manifest-related location{} block.
    add_header X-Docker-Registry-Proxy-Cache-Upstream-Status "$upstream_cache_status";
    add_header X-Docker-Registry-Proxy-Cache-Type "$docker_proxy_request_type";
    proxy_pass https://$targetHost;
    proxy_cache cache;
    proxy_cache_key   $uri;
    proxy_intercept_errors on;
    #error_page 301 302 307 = @handle_redirects;
EOF

# Optionally build container image of proxy cache
#
# Old / unmaintained image sources:
#   https://github.com/rpardini/docker-registry-proxy
#   https://github.com/rpardini/nginx-proxy-connect-stable-alpine
# Current upstream image sources:
#   https://github.com/coreweave/docker-registry-proxy
#   https://github.com/jhalfmoon/nginx-proxy-connect-stable-alpine/blob/master/Dockerfile
# Forks of upstream image sources, that contain updated versions of components; These are used to build the images
#   https://github.com/jhalfmoon/docker-registry-proxy
#   https://github.com/jhalfmoon/nginx-proxy-connect-stable-alpine

if [[ $DOCKER_PROXY_DEBUG -eq 1 ]] ; then
    IMAGE_SUFFIX=-debug
else
    IMAGE_SUFFIX=''
fi

# Either use the old, non-updated public image, or build a current version and use that
if [[ $DOCKER_PROXY_VERSION != 'newbuild' ]] ; then
    IMAGE_NAME=rpardini/docker-registry-proxy:${DOCKER_PROXY_VERSION}${IMAGE_SUFFIX}
else
    # Create build folder
    BUILD_DIR=/tmp/build
    [[ -d $BUILD_DIR ]] && rm -rf $BUILD_DIR
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR

    PROXY_REPO_NAME=docker-registry-proxy
    PROXY_REPO_URL=https://github.com/jhalfmoon/$PROXY_REPO_NAME.git
    PROXY_IMAGE_NAME=${PROXY_REPO_NAME}${IMAGE_SUFFIX}:${DOCKER_PROXY_VERSION}
    if (docker image list |  awk '{print $1":"$2}' | grep -q $PROXY_IMAGE_NAME) ; then
        echo "Note: Image $PROXY_IMAGE_NAME was found in the list of loaded docker images. Skipping build."
    else
        #== First, build the base image
        BASE_REPO_NAME=nginx-proxy-connect-stable-alpine
        BASE_REPO_URL=https://github.com/jhalfmoon/$BASE_REPO_NAME.git
        echo "Building base image, which is required by the build of the actual proxy image."
        echo "Note: Any existing images will be removed and a new image will be built, to prevent using a stale base image."
        # NOTE: The construction with '|| true' and 'wc -w' is used to not make this script fail
        #       when using 'set -e'. If '||true' is not used, the script would abort because grep returns
        #       an error if no string found. 'wc -w' is used because $LIST is never empty - if nothing
        #       is found, it will still contain a newline.
        LIST="$(docker image list | awk '{print $1":"$2}' | grep $BASE_REPO_NAME || true)"
        [[ $(echo "$LIST" | wc -w) -gt 0 ]] && echo $LIST | xargs docker rmi
        cd $BUILD_DIR
        git clone $BASE_REPO_URL $BASE_REPO_NAME
        cd $BASE_REPO_NAME
        # Note: Tagged commit is used to prevent unexpected version drift
        git checkout jhalfmoon_v2
        ALPINE_VER=$(cat Dockerfile | grep 'FROM alpine:' | cut -d: -f2)
        NGINX_VER=$(cat Dockerfile |grep 'ENV NGINX_VERSION' | tr '=' ' ' | awk '{print $3}')
        BASE_IMAGE_NAME="$BASE_REPO_NAME:nginx-${NGINX_VER}-alpine-${ALPINE_VER}${IMAGE_SUFFIX}"
        time docker build \
            --progress=plain \
            --build-arg DO_DEBUG_BUILD=$DOCKER_PROXY_DEBUG \
            -t $BASE_IMAGE_NAME .

        #== Next, build the actual proxy image
        cd $BUILD_DIR
        git clone $PROXY_REPO_URL $PROXY_REPO_NAME
        cd $PROXY_REPO_NAME
        #
        # hack: patch entrypoint to be verbose in order to debug a container restarts
        # sed -ie 's@bin/bash@bin/bash -eux@g' entrypoint.sh
        # hack: update mitmproxy version - https://pypi.org/project/mitmproxy/
        # sed -ie 's@\(mitmproxy\)==[.0-9]\+@\1==10.1.1@g' Dockerfile
        # hack: update markupsafe version - https://pypi.org/project/MarkupSafe/
        # sed -ie 's@\(MarkupSafe\)==[.0-9]\+@\1==2.1.3@g' Dockerfile
        # hack: add missing build dependencies
        # sed -ie 's@\(py3-setuptools\)@\1 rust cargo bsd-compat-headers@g' Dockerfile
        # hack: make pip install use multiple cores when running make
        # sed -ie 's@\(LDFLAGS\)@MAKEFLAGS="-j$(nproc)" \1@g' Dockerfile
        #
        # Notes:
        # * Tagged commit is used to prevent unexpected version drift
        # * As per 2024-12-26 the debug build will fail on the pip installation of mitmproxy. This is the case for the most recent version 11.0.2 and also for earlier versions such as 10.4.2.
        #   Until a few days ago, this build was still working. The exact cause for the failure is unknown.
        # * Tagged commit is used to prevent unexpected version drift
        git checkout jhalfmoon_v2
        time docker build \
            --progress=plain \
            --build-arg BASE_IMAGE=$BASE_IMAGE_NAME \
            --build-arg DO_DEBUG_BUILD=$DOCKER_PROXY_DEBUG \
            -t $PROXY_IMAGE_NAME .
    fi
fi

if [[ $DOCKER_PROXY_DEBUG -eq 1 ]] ; then
    DEBUG_FLAGS='-e DEBUG_NGINX=true -e DEBUG=true -e DEBUG_HUB=true'
else
    DEBUG_FLAGS=''
fi

# Note: Port 8091 and 8092 are optional and for debugging using mitmproxy
docker run \
        --detach \
        --restart=always \
        --name docker_registry_proxy${IMAGE_SUFFIX} \
        -p 0.0.0.0:8091:8081 \
        -p 0.0.0.0:8092:8082 \
        -p 0.0.0.0:$PROXY_PORT:$PROXY_PORT \
        -v $REGISTRY_DIR/caching-proxy/cache:/docker_mirror_cache \
        -v $REGISTRY_DIR/caching-proxy/certs:/ca \
        -e ENABLE_MANIFEST_CACHE=true \
        -e REGISTRIES="$LOCAL_REGISTRY production.cloudflare.docker.com registry.docker.io registry.k8s.io ghcr.io quay.io cr.l5d.io docker.l5d.io" \
        $DEBUG_FLAGS \
        $PROXY_IMAGE_NAME

        # To override proxy caching settings, for testing/setting alternative nginx settings for example:
        # -v $TEMP_DIR/nginx.manifest.common.conf.patched:/etc/nginx/nginx.manifest.common.conf \
        #
        # NOTE: There is a bug in k(3 or 8?)s where it does not follow redirects correctly when downloading images.
        #       It expects the proxy to alway return a 200 status with images. When the status is not 200, the download fails.
        #       This behaviour is seen when caching for the linkderd, downloaded from cr.l5d.io
        #       The workaround is to not include that site in the list of cached sites.
        #       Either one of the following two lines can be used, depending on whether that site needs to be cached or not.
        # -e REGISTRIES="$LOCAL_REGISTRY production.cloudflare.docker.com registry.docker.io k8s.gcr.io ghcr.io quay.io docker.l5d.io" \
        # -e REGISTRIES="$LOCAL_REGISTRY production.cloudflare.docker.com registry.docker.io k8s.gcr.io ghcr.io quay.io cr.l5d.io docker.l5d.io" \

echo "export PROXY_IP=$IP" > $TEMP_DIR/proxy_ip

# No longer needed, but here in case it becomes necessary again:
#
# Disable ipv6 dns resolving in nginx, as ipv6 adresses will randomly break downloads
# for sites as packages.cloud.google.com and docs.projectcalico.org
# NOTE: Nasty hack alert: The 'sleep' statement is needed to prevent an error message
#       during restart. It seems that "/etc/nginx/error.log.debug.warn" is unavailable
#       during exec if sleep is not called first, causing the reload to fail. That file
#       is created in entrypoint.sh and apparently gets called on each exec.
#
# docker exec $(sudo -u $VAGRANT_USER docker ps | grep docker_registry_proxy | awk '{print $1}') bash -c 'echo $(cat /etc/nginx/resolvers.conf | tr -d \;) ipv6=off\; > /etc/nginx/resolvers.conf ; sleep 3 ; nginx -s reload'

