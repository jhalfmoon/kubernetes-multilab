#!/bin/bash -eu

echo "=== Installing docker-compose $COMPOSE_VERSION"

FILE=docker-compose-linux-x86_64
LINK=https://github.com/docker/compose/releases/download/v$COMPOSE_VERSION/$FILE
DEST=/usr/local/bin/docker-compose

download $FILE $LINK $DEST
chmod +x $DEST
