#!/bin/bash -eux

echo "=== Saving apt packages store for re-use"

if [[ -z "$PKG_DIR" ]] ; then
    echo "ERROR: \$PKG_DIR is not set."
    exit 1
fi

[[ ! -d $PKG_DIR ]] && mkdir -p $PKG_DIR/{cache,lib}
rsync -r --delete-before /var/cache/apt/ $PKG_DIR/cache
rsync -r --delete-before /var/lib/apt/ $PKG_DIR/lib

