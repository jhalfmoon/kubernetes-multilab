#!/bin/bash -eu

echo "=== Restoring pip packages from cache"

FILE=$PIP_DIR/pip.tar.zst

# unpack cached pip cache
if [[ -e $FILE ]] ; then
    [[ -d $HOME/.cache ]] || mkdir -p $HOME/.cache
    tar xaf $FILE -C $HOME/.cache
else
    echo "No cache file was found at $FILE"
fi

