#!/bin/sh
#
# Note: sh is used instead of bash because this script needs to be
#       able to run in k3d, whose nested conatiners only provide sh

echo "=== Starting containerd image maintenance (save/restore/flush)"

fail() {
    echo "ERROR: $@"
    exit 1
}

save_images() {
    if ! (echo $NS_LIST | grep -q $NAMESPACE) ; then
#        fail "Namespace does not exist: $NAMESPACE"
        echo "WARNING: Namespace does not exist: $NAMESPACE . No images will be saved."
        return 0
    fi

    if [ ! -d $BASE_DIR ] ; then
        echo "Destination directory $BASE_DIR does not exist, creating it..."
        mkdir -p $BASE_DIR || fail "Creating $BASE_DIR failed."
    fi

    IMAGE_LIST="$( ctr -a $ENDPOINT -n $NAMESPACE image list | tail -n+2 | awk '{print $1}' | grep -v sha256 | grep -v '^$' )"
    [ $(echo "$IMAGE_LIST" | wc -l) -le 1 ] && fail 'No images found in namespace'
    echo "$IMAGE_LIST" | while read IMAGE ; do
       echo Saving "$IMAGE"
       #sudo ctr -a $ENDPOINT -n $NAMESPACE images export - "$IMAGE" | dd of="$BASE_DIR/${IMAGE//\//_}.oci-image"
       NAME=$(echo $IMAGE | tr '/' '_').oci-image
       sudo ctr -a $ENDPOINT -n $NAMESPACE images export - "$IMAGE" | dd of="$BASE_DIR/$NAME"
    done

    echo Export complete
}

restore_images() {
    if [ ! -d $BASE_DIR ] ; then
        echo "Info: Source dir '$BASE_DIR' does not exist. No images will be imported."
        return 0
    fi

    if [ -z "$NAMESPACE" ] ; then
        fail "No namespace given"
    else
        if (echo $NS_LIST | grep -q $NAMESPACE) ; then
            echo "Restoring to existing namespace $NAMESPACE"
        else
            echo "Creating new namespace $NAMESPACE"
            sudo ctr ns create $NAMESPACE
        fi
    fi

    echo "Restoring docker images from directory $(readlink -f $BASE_DIR)"

    cd $BASE_DIR
    LIST="$(ls *.oci-image)"
    if [ -z "$LIST" ] ; then
        echo "No images found."
    else
        echo "$LIST" |  while read FILE ; do
            echo "# Loading $FILE"
            sudo ctr -n $NAMESPACE images import "$FILE"
        done
        echo Import complete.
    fi
}

list_images() {
    echo "Namespaces:\n$NS_LIST\n"
    echo Images:
    for NAMESPACE in $NS_LIST; do
        echo "=== Namespace: $NAMESPACE"
        ctr -a $ENDPOINT -n $NAMESPACE image list | grep -v '^sha256.*' | awk '{print $1,$4}' | column -t
    done
}

# NOTE: This function will also remove images that are in use.
flush_images() {
    local IMAGES
    for NAMESPACE in $NS_LIST; do
        echo "=== Flushing images in namespace: $NAMESPACE"
        IMAGES=$(ctr -a $ENDPOINT -n $NAMESPACE image list | tail -n+2 | grep -v '^sha256.*' | awk '{print $1}')
        if [ $(echo "$IMAGES" | wc -l ) -gt 1 ] ; then
            echo "$IMAGES" | while read IMAGE ; do
                ctr -a $ENDPOINT -n $NAMESPACE image delete --sync $IMAGE
            done
        fi
    done
}

validate_namespace() {
    [ -z $NAMESPACE ] && fail "No namspace was given."
}

validate_basedir() {
    if [ -z $BASE_DIR ] ; then
        BASE_DIR=$IMAGE_DIR/$NAMESPACE
        echo "No base directory given, defaulting to $BASE_DIR"
    else
        echo "Using base directory $BASE_DIR"
    fi
}

NAMESPACE=''
BASE_DIR=''
ACTION=''

while getopts "flprsd:n:" OPTIONS; do
    case $OPTIONS in
        f)  ACTION=flush
            ;;
        l)  ACTION=list
            ;;
        p)  ACTION=prune
            ;;
        r)  ACTION=restore
            ;;
        s)  ACTION=save
            ;;
        d)  BASE_DIR=$OPTARG
            ;;
        n)  NAMESPACE=$OPTARG
            ;;
        *)  fail "Invalid parameter given."
            ;;
    esac
done

if [ -z $ACTION ] ; then
    fail "No action parameter given. Use: -s(ave), -r(estore), -l(ist), -f(lush)"
else
    echo "Selected action: $ACTION\n"
fi

if ! (which crictl &>/dev/null) ; then
    echo "WARNING: crictl is not installed in $PATH. Cancelled action and exiting."
    exit 0
fi

# Determine containerd endpoint to use
ENDPOINT=$(crictl info|jq -r .config.containerdEndpoint)
if [ -z "$ENDPOINT" ] ; then
    echo "WARNING: No valid ENDPOINT file detected. Cancelled action and exiting."
    exit 0
fi
if [ ! -S "$ENDPOINT" ] ; then
    fail "File "$ENDPOINT" is not a valid ENDPOINT file."
fi

NS_LIST="$(ctr -a $ENDPOINT namespace list -q)"

case $ACTION in
    prune)
        crictl rmi --prune
        ;;
    flush)
        flush_images
        ;;
    list)
        list_images
        ;;
    restore)
        validate_basedir
        validate_namespace
        restore_images
        ;;
    save)
        validate_basedir
        validate_namespace
        save_images
        ;;
esac
