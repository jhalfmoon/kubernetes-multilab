#!/bin/bash -eu

FLAG_FILE=$FLAG_DIR/$1
while [[ ! -e $FLAG_FILE ]] ; do
    echo "=== Waiting for $FLAG_FILE to appear..."
    sleep 10
done
