#!/bin/bash -eu

# Get commandline parameters and set defaults
DEST_DIR=${1:-$IMAGE_DIR}
FILTER=${2:-''}

echo "=== Exporting docker container images to $DEST_DIR"

[[ ! -d $DEST_DIR ]] && mkdir -p $DEST_DIR

echo "=== Saving docker images to $DEST_DIR using filter '$FILTER'"

cd $DEST_DIR
docker images | tail -n+2 | grep "$FILTER" | while read NAME TAG ID DUMMY; do
    if [[ $TAG =~ "none" ]] ; then
        FILE="${NAME}:none"
        TAG=''
    else
        FILE="${NAME}:${TAG}"
        TAG=:${TAG}
    fi
    FILE="${FILE//\//_}.docker-image.zst"
    if [[ -e $FILE ]] ; then
        echo -e "\nSkipping saving of image - File exists: $FILE"
    else
        echo -e "\nSaving ${NAME}${TAG} to $FILE"
        docker save "${NAME}${TAG}" | zstd -10 --long | dd of="${FILE}"
    fi
done
echo --- Dump done
