#!/bin/bash -eu

echo "=== Saving pip packages store for re-use"

if [[ -z "$PIP_DIR" ]] ; then
    echo "ERROR: \$PIP_DIR is not set."
    exit 1
fi

[[ ! -d $PIP_DIR ]] && mkdir -p $PIP_DIR

echo "INFO: Caching pip packages located in $HOME/.cache/pip"
tar caf $PIP_DIR/pip.tar.zst -C $HOME/.cache pip

