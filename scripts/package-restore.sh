#!/bin/bash -eu

echo "=== Restoring apt packages from cache"

if [[ -d $PKG_DIR/cache ]] && [[ -d $PKG_DIR/lib ]]; then
    rsync -r $PKG_DIR/cache/ /var/cache/apt
    rsync -r --delete-before $PKG_DIR/lib/ /var/lib/apt
fi
