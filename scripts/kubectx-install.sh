#!/bin/bash -eu
#
# NOTE: These tools expect fzf to be available.

echo "=== Installing kubectx and kubens"

[[ ! -d $DL_DIR/kubectx.git ]] && git clone https://github.com/ahmetb/kubectx $DL_DIR/kubectx.git
sudo cp -r $DL_DIR/kubectx.git /opt/kubectx
sudo ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens
# install autocompletion
sudo ln -sf /opt/kubectx/completion/*.bash /etc/bash_completion.d
