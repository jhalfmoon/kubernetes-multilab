#!/bin/bash -eu

echo "=== Cleaning up package cache and unused container images"

# Remove package cache
rm -f /var/cache/apt/*.deb

# Remove unused images that may have been pulled in from the cache and remain unused
# Note: The true statement is to prevent 'which' from causing this script to exit with a failure
(which docker &> /dev/null) && docker image prune -af || true
(which crictl &> /dev/null) && /vagrant/scripts/ctr-image.sh -p || true
