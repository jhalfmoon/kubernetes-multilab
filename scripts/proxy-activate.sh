#!/bin/bash -e

echo "=== Activating caching proxy (man-in-the-middle http proxy)"

WaitForProxy() {
    echo "--- Waiting for proxy to become available"
    while ! (curl -s -x $PROXY_IP:$PROXY_PORT google.com &> /dev/null ) ; do
        echo Waiting...
        sleep 2
    done
    # add extra linefeed, because the previous action will leave it at EOL of a string
    echo ''
}

# Wait for the proxy to become available
WaitForProxy

# Install the proxy CA in the host vm
curl -sL http://$PROXY_IP:$PROXY_PORT/ca.crt -o /usr/share/ca-certificates/docker_registry_proxy.crt
echo "docker_registry_proxy.crt" >> /etc/ca-certificates.conf
update-ca-certificates --fresh

# Prevent apt from using this proxy
cat <<EOF > /etc/apt/apt.conf.d/95proxy.conf
Acquire::http::Proxy "false";
Acquire::https::Proxy "false";
EOF

cat <<EOF > /etc/profile.d/proxy.sh
export HTTP_PROXY=http://$PROXY_IP:$PROXY_PORT
export HTTPS_PROXY=http://$PROXY_IP:$PROXY_PORT
export NO_PROXY=$NOPROXY_LIST
export http_proxy=http://$PROXY_IP:$PROXY_PORT
export https_proxy=http://$PROXY_IP:$PROXY_PORT
export no_proxy=$NOPROXY_LIST
EOF

# Containerd uses this (not confirmed d.d. 20211019)
cat <<EOF > /etc/environment
export HTTP_PROXY=http://$PROXY_IP:$PROXY_PORT
export HTTPS_PROXY=http://$PROXY_IP:$PROXY_PORT
export NO_PROXY=$NOPROXY_LIST
export http_proxy=http://$PROXY_IP:$PROXY_PORT
export https_proxy=http://$PROXY_IP:$PROXY_PORT
export no_proxy=$NOPROXY_LIST
EOF

# If docker is installed, make it use the proxy
# Oh, and... https://about.gitlab.com/blog/2021/01/27/we-need-to-talk-no-proxy/
[[ -z $DOCKER_PROXY_IP   ]] && DOCKER_PROXY_IP=$PROXY_IP
[[ -z $DOCKER_PROXY_PORT ]] && DOCKER_PROXY_PORT=$PROXY_PORT
if (which docker &> /dev/null) ; then
    mkdir -p /etc/systemd/system/docker.service.d
    cat << EOF > /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://$DOCKER_PROXY_IP:$DOCKER_PROXY_PORT"
Environment="HTTPS_PROXY=http://$DOCKER_PROXY_IP:$DOCKER_PROXY_PORT"
Environment="NO_PROXY=$NOPROXY_LIST"
EOF
    systemctl daemon-reload
    systemctl restart docker.service
fi

# Wait for the proxy to become available again
WaitForProxy

# Add the registry name to hosts
# NOTE: This is not a proxy related config, but strictly a registry related
#       It is done in this script because it is run on each node, whereas
#       the registry installation is only done on a single node.
echo -e "\n$PROXY_IP $LOCAL_REGISTRY" | sudo tee -a /etc/hosts >/dev/null

