#!/bin/bash -e

echo "=== Installing Helm $HELM_VERSION"

HELM_PLATFORM=linux-amd64
FILE=helm-v${HELM_VERSION}-${HELM_PLATFORM}.tar.gz
LINK=https://get.helm.sh/${FILE}

download $FILE $LINK

# Extract binary from tarball
WORK_DIR=$(mktemp -d)
tar -x -C $WORK_DIR -f $DL_DIR/$FILE
mv $WORK_DIR/${HELM_PLATFORM}/helm /usr/local/bin/
rm -rf $WORK_DIR
