#!/bin/bash -eu

# Reference: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux

echo "=== Installing kubectl $K8S_VERSION"

sudo apt-get install -y apt-transport-https ca-certificates curl
# Note: URL for gpg has changed recently, as documted here:
#   https://github.com/kubernetes/release/issues/2862
FILE=/etc/apt/keyrings/kubernetes-apt-keyring.gpg
if ! [[ -e $FILE ]] ; then
    curl -fsSL ${K8S_DEB_REPO_KEY} | sudo gpg --batch --dearmor -o $FILE
    echo "${K8S_DEB_REPO_CONFIG}" | sudo tee /etc/apt/sources.list.d/kubernetes.list > /dev/null
fi

sudo apt-get update
sudo apt-get install --allow-change-held-packages -y kubectl=${K8S_VERSION}-*
sudo apt-mark hold kubectl

KUBE_COMPLETION=/etc/bash_completion.d/kubectl
kubectl completion bash | sudo tee $KUBE_COMPLETION > /dev/null
cat <<EOF | sudo tee /etc/profile.d/kubectl.sh > /dev/null
    source $KUBE_COMPLETION
    complete -F __start_kubectl k
    alias k="kubectl"
EOF
