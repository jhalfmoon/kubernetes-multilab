filetype plugin on
filetype indent on

" The following settings must be set before setting colorschemes. If not done, then colorschemes
" will get loaded incorrectly. Try doing 'colorscheme kruby' before this is set and see the effect.
syntax enable
" enable 256 color mode
"set t_Co=256

" Only source files if installed/available
if filereadable(expand("$HOME/.vim/autoload/plug.vim"))
    if filereadable(expand("$HOME/.vimrc-plug"))
      source $HOME/.vimrc-plug
    endif
endif

" Settings in this block are only applied if the plugins are available
"if filereadable(expand("$HOME/.vim/autoload/plug.vim"))
  " This scheme available if the plugin vim-colorschemes is available
"  colorscheme kruby
"fi

" Color schemes should be loaded after plug#end().
" We prepend it with 'silent!' to ignore errors when it's not yet installed.
silent! colorscheme kruby

" This enables you to put your cursor on one parenthesis, or bracket, or curly brace,
" type %, and then your cursor will automatically jump to the other element of the pair.
runtime macros/matchit.vim

" remap keys to better match colemak layout
noremap j k
noremap k j

" Press enter to clear search highlights - from https://stackoverflow.com/questions/657447/vim-clear-last-search-highlighting
nnoremap <CR> :noh<CR><CR>

set paste

"set mouse=a
set nocompatible                " this actually gets set by default if ~/.vimrc is present
set encoding=utf-8              " force to utf8 mode

set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set expandtab
set modeline
set modelines=5

set nonumber                    " disable all linenumbering by default
set norelativenumber
set ruler                       " show cursor line and column in the status line
"set cursorline                  " highlight the screen line of the cursor
set ignorecase                  " most of the time, we don't care for sensitive searches
set showmatch                   " briefly jump to matching bracket if insert one
set hlsearch                    " highlight matches with last search pattern
set incsearch                   " highlight match while typing search pattern
set scrolloff=3                 " Keep 3 lines below and above the cursor
set backspace=indent,eol,start  " makes backspace key more powerful.
set formatoptions-=cro          " do not autowrap or autoinsert comment leader (see 'help fo-table')
set nobackup                    " keep backup file after overwriting a file
set autoread                    " Set to auto read when a file is changed from the outside

" force search highlighting to something I like
hi Search cterm=NONE ctermfg=black ctermbg=yellow

" Open all folds initially
set foldmethod=indent
set foldlevelstart=99

" Text display settings
set wrap
set whichwrap+=h,l,<,>,[,]

" Always splits to the right and below
set splitright
set splitbelow

let mapleader = ","
nmap       <leader>pp   :set paste! <cr>
nnoremap   <leader>tn   :tabnext<CR>
nnoremap   <leader>tp   :tabNext<CR>
nnoremap   <leader>n    :tabnew<CR>
nnoremap   <leader>ww   :%s/\s\+$//g<CR>
map        <leader>q            ysiw'
map        <leader><leader>q    ysiw"
map <space> 10j
map #<space> 10k


