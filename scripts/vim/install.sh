#!/bin/bash
#
# Install vim-plug and initialize all plugins
# NOTE:
#   * It is assumed that $DL_DIR is already set

HERE=$(dirname $(readlink -f $0))

# debug
#rm -rf ~/.vim* ~/vim*

DEST_DIR=$HOME/.vim/autoload

FILE=plug.vim
if [[ ! -e $DEST_DIR/$FILE ]] ; then
    if [[ ! -e $DL_DIR/$FILE ]] ; then
        curl -fLo $DL_DIR/$FILE --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    fi
    [[ -d $DEST_DIR ]] || mkdir -p $DEST_DIR
    cp -r $DL_DIR/$FILE $DEST_DIR/$FILE
fi

FILE=.vimrc
[[ -e $HOME/$FILE ]] || cp $HERE/$FILE $HOME/$FILE

FILE=.vimrc-plug
[[ -e $HOME/$FILE ]] || cp $HERE/$FILE $HOME/$FILE

# Install plugins before using vim with its actual .vimrc
# Note that --sync is required, otherwise vim will not autoload the vim-plug plugin when called from the commandline
# See: https://github.com/junegunn/vim-plug/issues/675
vim -u $HOME/$FILE +'PlugInstall --sync' +qa

