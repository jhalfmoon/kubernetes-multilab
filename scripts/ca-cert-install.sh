#!/bin/bash -eu

# This script installs mkcert and generates a ca cert.
# Other scripts can then use the CA to generate certificates, for a docker registry for example.
#
# For more info, see:
# https://github.com/FiloSottile/mkcert/
# https://kifarunix.com/create-locally-trusted-ssl-certificates-with-mkcert-on-ubuntu-20-04/

echo "=== Installing mkcert and creating new or copying existing root CA"

MKCERT_NAME=mkcert-v${MKCERT_VERSION}-linux-amd64
MKCERT_LINK=https://github.com/FiloSottile/mkcert/releases/download/v${MKCERT_VERSION}/${MKCERT_NAME}
MKCERT_DEST=/usr/local/bin/mkcert
CA_HOME_DIR=$HOME/.local/share/mkcert/

# Install mkcert
download $MKCERT_NAME $MKCERT_LINK $MKCERT_DEST
sudo chmod +x $MKCERT_DEST

# Re-use the existing CA if one is available, or Create a new CA and store it
if [[ ! -e $TEMP_DIR/rootCA.pem ]] ; then
    mkcert -install
    cp $CA_HOME_DIR/{rootCA-key.pem,rootCA.pem} $TEMP_DIR
    chmod +rw $TEMP_DIR/{rootCA-key.pem,rootCA.pem}
else
    mkdir -p $CA_HOME_DIR
    cp $TEMP_DIR/{rootCA-key.pem,rootCA.pem} $CA_HOME_DIR
    mkcert -install
fi
