#!/bin/bash -eu

echo "=== Installing docker"

# Remove any existing / old packages
apt-get remove -y docker docker.io containerd runc

apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

# Install docker bash completion
download docker.sh https://raw.githubusercontent.com/docker/docker-ce/master/components/cli/contrib/completion/bash/docker /etc/bash_completion.d/

containerd config default > /etc/containerd/config.toml
# Set systemd as cgroup driver; kubeadm will detect this and use it too
# On modifying config.toml: https://www.howtoforge.com/how-to-install-containerd-container-runtime-on-ubuntu-22-04/
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
systemctl restart containerd
systemctl status containerd

# Make docker use systemd cgroups driver. If you do not do this, the following warning will be shown
# [WARNING IsDockerSystemdCheck]: detected "cgroupfs" as the Docker cgroup driver. The recommended driver is "systemd".
#
cat <<EOF > /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

systemctl enable docker
systemctl daemon-reload
systemctl restart docker

# Give user vagrant super powers
usermod -aG docker $VAGRANT_USER
# Make ssh close all sessions to force vagrant to reconnect.
# If this is not done, then Vagrant will re-use its current ssh session
# and doing so, will not be memeber of the docker group, giving us problems.
killall -HUP sshd
