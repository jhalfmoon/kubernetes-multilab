#!/bin/bash -e

echo "=== Restoring docker container images"

SRC_DIR=$IMAGE_DIR
[[ -n $1 ]] && SRC_DIR=$1

if [[ ! -d $SRC_DIR ]] ; then
    echo "No images were imported: $1 does not exitst."
elif [[ $(ls $SRC_DIR | wc -l) -eq 0 ]] ; then
    echo "No images were imported: $SRC_DIR is empty."
else
    echo "= Using source folder $SRC_DIR"
    cd $SRC_DIR
    # handle uncompressed images
    ls *.docker-image 2> /dev/null | while read IMAGE ; do
        docker load < $IMAGE
    done
    # handle compressed images
    ls *.docker-image.zst 2> /dev/null | while read IMAGE ; do
        zstd -cd $IMAGE | docker load
    done
    echo "--- Restore completed"
fi
