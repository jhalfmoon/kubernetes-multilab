#!/bin/bash -eu

echo "=== Waiting for first k3d cluster to be up..."
while [[ $(kubectl get no 2> /dev/null | grep Ready | wc -l) -lt 1 ]] ; do
    echo -n .
    sleep 2
done
echo
