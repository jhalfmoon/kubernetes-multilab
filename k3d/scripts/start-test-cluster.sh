#!/bin/bash -eu

echo "=== Starting test cluster to pre-download the k3d container images"
echo "=== This cluster has port 80 and 443 exposed, for demonstration purposes."

if ! kk1 test-cluster --port 80:80@loadbalancer --port 443:443@loadbalancer ; then
    echo "ERROR: Failed to create cluster. Check logs for possible cause."
    exit 1
fi
