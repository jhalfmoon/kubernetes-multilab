#!/bin/bash -eu

echo "=== Installing k3d"

BINDIR=/usr/local/bin
SCRIPT=k3d-installer.sh

# https://github.com/k3s-io/k3s/blob/master/install.sh
#SCRIPT='INSTALL_K3S_SKIP_START=true install_k3s.sh'

# Restore previously stored script and binary, if available
if [[ -e $DL_DIR/k3d ]] && [[ -e $DL_DIR/$SCRIPT ]] ; then
    cp $DL_DIR/{k3d,$SCRIPT} $BINDIR
else
    curl --retry 5 --retry-delay 5 -sSL https://raw.githubusercontent.com/rancher/k3d/$K3D_VERSION/install.sh -o $BINDIR/$SCRIPT
fi

# Run installer and retry on failure, because I've seen it fail a couple of times
COUNT=0
while [[ COUNT -lt 5 ]] ; do
    COUNT=$((COUNT+1))
    (bash $BINDIR/$SCRIPT) && break
    echo "--- Installation attempt $COUNT failed."
    date
    sleep 5
done

# Store installation script and binary
cp $BINDIR/{k3d,$SCRIPT} $DL_DIR/

cat <<"EOF" >/etc/profile.d/500-k3d.sh
# Wrap k3d to always update the config file. This is particularly useful
# when deleting clusters, so that references to old instances get removed.
k3d() {
    local CONFIG=$TEMP_DIR/config STATUS
    /usr/local/bin/k3d $@
    STATUS=$?
    # sync config to vm host machine
    sudo cp -f $HOME/.kube/config $CONFIG
    # make config +rw for group on host system
    sudo chown $NFS_OWNER_UID $CONFIG
    sudo chmod 600 $CONFIG
    return $STATUS
}
# by exporting the function, any script that use k3d will also use the above wrapper function
export -f k3d

source <(/usr/local/bin/k3d completion bash)
EOF

# see https://k3d.io/usage/configfile/
cat <<EOF > /etc/k3d-default-config.yml
---
# https://k3d.io/v5.4.9/usage/configfile/
#
apiVersion: k3d.io/v1alpha5
kind: Simple
metadata:
  name: default-cluster
servers: 1
agents: 0
options:
  k3d:
    wait: true
    timeout: "60s"
    # disableLoadbalancer: false    # same as --no-lb
    # disableImageVolume: false     # same as --no-image-volume
    # disableRollback: false        # same as --no-Rollback
  # k3s:
  #   extraArgs:                      # same as --k3s-arg
  #     - arg: "--disable=traefik"
  #       nodeFilters:
  #         - server:*
  kubeconfig:
    updateDefaultKubeconfig: true
    switchCurrentContext: true
env:
  - envVar: HTTP_PROXY=http://$PROXY_IP:$PROXY_PORT
    nodeFilters:
      - all
  - envVar: HTTPS_PROXY=http://$PROXY_IP:$PROXY_PORT
    nodeFilters:
      - all
  - envVar: NO_PROXY='$NOPROXY_LIST'
    nodeFilters:
      - all
volumes:
  - volume: $REGISTRY_DIR/caching-proxy/certs/ca.crt:/etc/ssl/certs/registry-proxy-ca.pem
    nodeFilters:
      - all
  - volume: $TEMP_DIR/rootCA.pem:/etc/ssl/certs/registry.local-ca.pem
    nodeFilters:
      - all
kubeAPI:
    # Set hostIP so that k3d uses that routable IP instead of default 0.0.0.0
    hostIP: "$IP"
#    hostPort: "6445"
#ports:
# - port: 80:80
#   nodeFilters:
#     - loadbalancer
# - port: 443:443
#   nodeFilters:
#     - loadbalancer
#registries:
#  create: true
#  config: |
#    mirrors:
#      "registry.local":
#        endpoint:
#          - http://registry.local
#    configs:
#      "registry.local":
#        tls:
#          ## https://k3d.io/usage/guides/registries/
#          # we will mount "my-company-root.pem" in the /etc/ssl/certs/ directory.
#          # --volume "${HOME}/.k3d/my-registries.yml:/etc/rancher/k3s/registries.yml" \
#          # --volume "${HOME}/.k3d/my-company-root.pem:/etc/ssl/certs/my-company-root.pem"
#          ca_file: "/etc/ssl/certs/my-company-root.pem"
#        auth:
#          username: xxxxxx
#          password: xxxxxx
EOF

# Example script; Quick and dirty - destroy all clusters and create one new cluster
FILE=/usr/local/bin/k3del
cat <<"EOF" > $FILE
#!/bin/bash -x
k3d cluster delete --all
EOF
chmod +x $FILE

# Example script:
#   Start cluster with
#       * custom version of traefik, installed using helm-controller
#       * calico instead of flannel, installed using plain k8s manifest
#       * metallb instead of servicelb, installed using plaing k8s manifest
#   Via:
#       https://docs.tigera.io/calico/latest/getting-started/kubernetes/minikube
#   See:
#       https://docs.k3s.io/networking
#       https://docs.k3s.io/installation/network-options
#       https://rancher.com/docs/k3s/latest/en/helm/
#
#       https://github.com/projectcalico/calico/releases
#       https://raw.githubusercontent.com/projectcalico/calico/v3.29.1/manifests/calico.yaml
#
#       https://metallb.universe.tf/installation/
#       https://github.com/metallb/metallb/releases
#       https://raw.githubusercontent.com/metallb/metallb/v0.14.8./config/manifests/metallb-native.yaml
#
FILE=/usr/local/bin/kkx
cat <<"EOF" > $FILE
#!/bin/bash -x
# NOTE: It is recommended / necessary to add a suffix such as '-custom' to the files mounted in $DIR_REM.
#       Failing to do so may give unexpected results as the files may clash with existing files.
#       For example, mounting traefik.yaml without the suffix will result in a failure to install that manifest.
#
[[ -n $K3D_IMAGE ]] && IMAGE="--image ${K3D_IMAGE}" || IMAGE=''
cp -r /vagrant/k3d/extra-manifests /tmp/
DIR_LOC=/tmp/extra-manifests
DIR_REM=/var/lib/rancher/k3s/server/manifests
time k3d cluster create custom-cluster $IMAGE \
    --config /etc/k3d-default-config.yml \
    \
    --k3s-arg='--disable=traefik@all:*' \
    --volume $DIR_LOC/traefik-chart.yaml:$DIR_REM/traefik-chart-custom.yaml \
    \
    --k3s-arg '--disable-network-policy@all:*' \
    --k3s-arg='--flannel-backend=none@all:*' \
    --volume $DIR_LOC/calico-v3.29.1.yaml:$DIR_REM/calico-custom.yaml \
    \
    --k3s-arg='--disable=servicelb@all:*' \
    --volume $DIR_LOC/metallb-native-v0.14.8.yaml:$DIR_REM/metallb-custom.yaml \
    --volume $DIR_LOC/metallb-ip-pool.yaml:$DIR_REM/metallb-ip-pool-custom.yaml \
    \
    $@
EOF
chmod +x $FILE

# Example script:
#   Start cluster without traefik ingress, in order to experiment with upgrading it
#
FILE=/usr/local/bin/kk0
cat <<"EOF" > $FILE
#!/bin/bash -x
[[ -n $K3D_IMAGE ]] && IMAGE="--image ${K3D_IMAGE}" || IMAGE=''
time k3d cluster create $IMAGE \
    --config /etc/k3d-default-config.yml \
    --k3s-arg="--disable=traefik@server:*" \
    $@
EOF
chmod +x $FILE


# Example script:
#   Start cluster with command line parameters and config file
#
FILE=/usr/local/bin/kk1
cat <<"EOF" > $FILE
#!/bin/bash -x
[[ -n $K3D_IMAGE ]] && IMAGE="--image ${K3D_IMAGE}" || IMAGE=''
time k3d cluster create $IMAGE --config /etc/k3d-default-config.yml $@
date
EOF
chmod +x $FILE

# Example script:
#   Start cluster using only command line parameters
#   Note1: Some possible nodefilters: all, serverlb, server:0, server:0;agent:*
#   Note2: Before k3d v5.0.0 the filters used square brackets as delimiters for indexes; These are no longer needed.
#
FILE=/usr/local/bin/kk2
cat <<"EOF" > $FILE
#!/bin/bash -x
echo "NOTE: To configure port forwards use --port XX:YY@loadbalancer"
[[ -n $K3D_IMAGE ]] && IMAGE="--image ${K3D_IMAGE}" || IMAGE=''
time k3d cluster create $IMAGE \
    --wait \
    --servers 1 \
    --agents 2 \
    --api-port $IP:random \
    --network k3d-backend \
    --kubeconfig-update-default \
    --kubeconfig-switch-context \
    --volume "$REGISTRY_DIR/caching-proxy/certs/ca.crt:/etc/ssl/certs/registry-proxy.pem@all" \
    --volume "$TEMP_DIR/rootCA.pem:/etc/ssl/certs/registry-local.pem@all" \
    --env "HTTP_PROXY=http://$PROXY_IP:$PROXY_PORT@all" \
    --env "HTTPS_PROXY=http://$PROXY_IP:$PROXY_PORT@all" \
    --env "NO_PROXY=$NOPROXY_LIST@all" \
    $@
    # --port 80:80@loadbalancer \
    # --port 443:443@loadbalancer \
kubectl get nodes
EOF
chmod +x $FILE

# Example script:
#   Same as previous script, but without proxy
#
FILE=/usr/local/bin/kk3
cat <<"EOF" > $FILE
#!/bin/bash -x
echo "NOTE: To configure port forwards use --port XX:YY@loadbalancer"
[[ -n $K3D_IMAGE ]] && IMAGE="--image ${K3D_IMAGE}" || IMAGE=''
time k3d cluster create $IMAGE\
    --wait \
    --servers 1 \
    --agents 2 \
    --api-port $IP:random \
    --network k3d-backend \
    --kubeconfig-update-default \
    --kubeconfig-switch-context \
    --volume "$REGISTRY_DIR/caching-proxy/certs/ca.crt:/etc/ssl/certs/registry-proxy.pem@all" \
    --volume "$TEMP_DIR/rootCA.pem:/etc/ssl/certs/registry-local.pem@all" \
    $@
EOF
chmod +x $FILE
