Howto get list of chart versions
--------------------------------
In case you would like to update the traefik/ingress-nginx helm chart manifest with the latest version::

    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm repo add traefik https://traefik.github.io/charts
    helm repo update
    helm repo list

    # show most recent versions of all repos
    helm search repo

    # optional: show all versions of all repos
    helm search repo --versions

Misc::

    https://github.com/projectcalico/calico/releases
    wget https://raw.githubusercontent.com/projectcalico/calico/v3.29.1/manifests/calico.yaml
    https://github.com/metallb/metallb/releases
    wget https://raw.githubusercontent.com/metallb/metallb/v0.14.8/config/manifests/metallb-native.yaml
