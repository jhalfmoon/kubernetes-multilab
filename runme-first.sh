#!/bin/bash -eu

HERE=$(dirname $(readlink -f $0))
MODE='g+rwX,o+rX'

echo "=== Now running 'chmod $MODE' on scripts and support folder. This needs to be done at least"
echo "    one time after cloning this repo to prepare the folders to be accessed via NFS."
echo "    If your NFS service is already running, this script may take up to a minute to complete."
chmod $MODE $HERE
chmod -R $MODE $HERE/{scripts,support,examples}
echo "=== Done"
