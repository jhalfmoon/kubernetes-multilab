packer
======

* Add user to HyperV Administrator group.

* Install prerequisites:

  * git - https://git-scm.com/download/win
  * vagrant - https://developer.hashicorp.com/vagrant/install?product_intent=vagrant
  * packer - https://developer.hashicorp.com/packer/install?product_intent=packer
      copy to C:\Users\Beep\AppData\Local\Microsoft\WindowsApps

NOTE:
* Location of packer plugins, in case you need to debug: C:/Users/beep/AppData/Roaming/packer.d/plugins

* Ensure that you have a fixed IP on your local network, as HyperV VMs will be using a virtual switch, bridging your network, and requesting IP addresses on it. Particularly if your NIC is using DCHP with a fixed assigned IP address, that will cause problems, as your VMs will receive the same IP as your host computer is using.

* Maybe obvious, but it was not to me at first:Ubuntu autoinstall is not the same as cloud-init and only share a subset of features.

    * https://canonical-subiquity.readthedocs-hosted.com/en/latest/reference/autoinstall-reference.html
    * https://serverfault.com/questions/1122573/what-is-the-difference-between-cloud-init-and-autoinstall
    * https://canonical-subiquity.readthedocs-hosted.com/en/latest/intro-to-autoinstall.html

* Pre-configure bento switch, as the bento packer config cannot do that for you, as it does not know which interface you wish to bridge. This can be done in two ways. Either open Hyper-V manager and add an external switch, bridging your internet facing interface and sharing it with the host OS.

Or use a powershell script::

  # Find the name of your internet facing NIC
  Get-NetAdapter

  # Create a switch using your internet facing NIC
  New-VMSwitch -Name bento -NetAdapterName 'Wi-Fi'
  Set-VMSwitch -Name foo-switch -AllowManagementOS $true

For more info on using Powershell to manage HyperV:
* https://learn.microsoft.com/en-us/windows-server/virtualization/hyper-v/get-started/create-a-virtual-switch-for-hyper-v-virtual-machines?tabs=powershell
* https://learn.microsoft.com/en-us/powershell/module/hyper-v

You could run into a few issues:
* If you get the error " Error getting host adapter ip address: No ip address.", then your host OS has an incorrectly configured static IP address.
* If your images boot, but do not start cloud-config / provisioning automatically, then you may have the wrong type of switch type configured, or have it bridging an incorrect interface. Ensure that you are bridging the internet facing NIC.

* Open a powershell terminal.

* Clone bento and patch it to make it work with HyperV::

  mkdir repo
  cd repo
  git clone https://github.com/chef/bento.git
  cd bento
  # Checkout a know working commit. The commmit after this one, for example, breaks the HyperV completely
  git checkout 5ff5536c8d80d9a97cf3b8ce62d46c4944cfb2dd

TODO: Patch bento here.
How to make IP address available::

  sudo apt-get install linux-cloud-tools-virtual

Via: https://stackoverflow.com/questions/33027204/how-can-i-get-hyper-v-to-detect-my-ubuntu-vms-ip-address
Via: https://github.com/hashicorp/packer-plugin-hyperv/issues/65

Cloud-init reference:
* https://cloudinit.readthedocs.io/en/latest/reference/examples.html#install-arbitrary-packages

* Patch bento::

  # Checkout the last known-working commit. Bento sometimes breaks in a big way
  git checkout 5ff5536

  # Apply fix specifically for hyperv, because this is broken in all commits until now
  git apply build-vagrant-box-generic.patch
  # Apply fix for hyperv build specifically, which seems to have been broken for a very long time
  git apply build-vagrant-box-hyperv.patch

* Start the provisioning run::

  packer init -upgrade ./packer_templates
  # optionally enable debug logging
  $env:PACKER_LOG=1
  packer build -only='hyperv-iso.vm' -var='cpus=4' -var='memory=4096' -var='headless=false' -var-file='os_pkrvars/ubuntu/ubuntu-22.04-x86_64.pkrvars.hcl' ./packer_templates

* Add the box to vagrant::

  vagrant box remove bento-ubuntu-22.04
  vagrant box add builds\ubuntu-22.04-x86_64.hyperv.box --name bento-ubuntu-22.04

* Bring up the box::

  vagrant up

will ask for
    switch name / number
    smb user / password

FIXTHIS - was working up till this point
break on download of docker: command "download" not found
  as download is a function defined in bashrc, and that file should be mounted as share on /vagrant/scripts/basrc, my guess is that the share does not work

Reference:
* https://learn.microsoft.com/en-us/virtualization/community/team-blog/2017/20170706-vagrant-and-hyper-v-tips-and-tricks
* https://cloudinit.readthedocs.io/en/latest/reference/base_config_reference.html

