This is a bare alpine VM.

It will probably require you to first build a vagrant box. A support script for that is located in the support folder, along with a copy of the alpine-vagrant repo, for just-in-case-backup purposes.

Links:

* https://wiki.alpinelinux.org/wiki/K8s
* https://wiki.alpinelinux.org/wiki/Podman
* https://wejn.org/2023/09/installing-podman-on-alpine-linux

