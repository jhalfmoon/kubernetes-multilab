#!/bin/bash -eu

# Download and install binary
FILE=kind
LINK="https://kind.sigs.k8s.io/dl/${KIND_VERSION}/kind-linux-amd64"
DEST=/usr/local/bin/kind
download $FILE $LINK $DEST
sudo chmod +x $DEST

# Comfigure bash completion
$DEST completion bash | sudo tee /etc/profile.d/kind_completion.sh > /dev/null

# Create default cluster
export -n HTTP_PROXY HTTPS_PROXY http_proxy https_proxy
unset HTTP_PROXY HTTPS_PROXY http_proxy https_proxy

# Generate example config
#   https://kind.sigs.k8s.io/docs/user/configuration
#   https://pkg.go.dev/sigs.k8s.io/kind/pkg/apis/config/v1alpha4
cat <<EOF | sudo tee /etc/kind-multinode-config.yml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
  - role: control-plane
  - role: worker
  - role: worker
networking:
  apiServerAddress: $IP
EOF

# Available images: https://hub.docker.com/r/kindest/node/tags
# Note that --config is used to be able to set a routable apiServerAddress
#
time kind create cluster --name mycluster --config /etc/kind-multinode-config.yml
# --image kindest/node:v1.26.0
# --wait 120s

# Display some inforation
echo "Clusters running:"
kind get clusters
echo
echo "Nodes running:"
kind get nodes -A
echo

# Generate kubeconfig file
kind export kubeconfig --name mycluster

# Replace loopback address with routable address
#sed -i "s/127\.0\.0\.1/$IP/g" $HOME/.kube/config

# alias cluster to 'local'
(which kubectx &> /dev/null) && kubectx local=.

# Copy kubeconfig to shared folder so the cluster can be managed from outside the vm
chmod 600 $HOME/.kube/config
sudo cp $HOME/.kube/config $TEMP_DIR/config

cat <<EOF
Hints:
    kind create cluster --name mycluster1 --wait
    kind create cluster --name mycluster2 --config /etc/kind-multinode-config.yml --wait
    kind delete clusters -A

    kind export kubeconfig
    kind get clusters
    kind get nodes

    kubectx local=.
EOF

