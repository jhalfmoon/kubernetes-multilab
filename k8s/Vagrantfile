# vi: set ft=ruby :

$nodes          = 3
$vm_cpus        = 4
$vm_memory      = 4096
$ip_range       = "10.0.20"
$ip_start       = 20
$vm_name_prefix = "k8s"

# https://stackoverflow.com/questions/40270391/shell-environment-variables-in-vagrant-files-are-only-passed-on-first-up
$set_vars = <<SCRIPT
tee -a "/etc/profile.d/100-vagrant.sh" > "/dev/null" <<"EOF"
source /vagrant/scripts/env
export IMAGE_DIR=${IMAGE_DIR}-${K8S_VERSION}
source /vagrant/scripts/bashrc
export K8S_JOIN_FILE=$TEMP_DIR/k8s-join
EOF
SCRIPT

Vagrant.configure("2") do |config|
  # Bento Ubuntu images are not available pre-built for libvirt. See readme on how to build your own.
  config.vm.box = "bento-ubuntu-22.04"

  config.vm.box_check_update = false
  config.ssh.insert_key = false
  config.vm.provider 'libvirt' do |lv, config|
    lv.cpus = $vm_cpus
    lv.memory = $vm_memory
    lv.graphics_type = "none"
    lv.default_prefix = ''
    # NFS share options use no_root_squash to prevent chown errors when mounted in docker
    #   https://stackoverflow.com/questions/56126490/docker-run-on-nfs-mount-causes-fail-to-copy-and-fail-to-chown
    #   https://www.vagrantup.com/docs/synced-folders/nfs
    config.vm.synced_folder "..", "/vagrant", disabled: false, type: "nfs", nfs_version: 4, nfs_udp: false, linux__nfs_options: [ 'rw','no_root_squash','no_subtree_check','anonuid=1000','anongid=1000' ]
  end

  # inject environment variables into the vm, including Vagrant variables
  config.vm.provision :shell, inline: "echo 'export VM_NAME_PREFIX=" + $vm_name_prefix + "' > /etc/profile.d/100-vagrant.sh"
  #config.vm.provision :shell, inline: "echo 'export IP_ADDR=" + $vm_ip_range + "." + $ip_start +"' >> /etc/profile.d/vagrant-env.sh"
  config.vm.provision :shell, privileged: true, inline: $set_vars, run: "always"
  config.vm.provision :shell, privileged: true, path: "../scripts/flag-clear-all.sh"

  (1..$nodes).each do |i|
    config.vm.define $vm_name_prefix + "-#{i}", autostart: true, primary: true do |node|
      node.vm.hostname = $vm_name_prefix + "-#{i}.mine.local"
      node.vm.network :private_network, ip: $ip_range + ".#{$ip_start+i}", nic_type: "virtio"
      if i == 1
        node.vm.provision :shell, privileged: true,  path: "../scripts/package-restore.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/system-base-install.sh"
        node.vm.provision :shell, privileged: true,  path: "scripts/install-base.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/docker-install.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/package-save.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/flag-set.sh", args: "packages"

        node.vm.provision :shell, privileged: false, path: "../scripts/kubectl-install.sh"
        node.vm.provision :shell, privileged: false, path: "../scripts/kubectx-install.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/helm-install.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/docker-compose-install.sh"
        node.vm.provision :shell, privileged: false, path: "../scripts/ca-cert-install.sh"

        node.vm.provision :shell, privileged: false, path: "../scripts/docker-image-restore.sh"
        node.vm.provision :shell, privileged: false, path: "../scripts/registry-install.sh"
        node.vm.provision :shell, privileged: false, path: "../scripts/proxy-install.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/proxy-activate.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/docker-image-restore.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/ctr-image.sh", args: [ "-r","-n","k8s.io" ]

        node.vm.provision :shell, privileged: false, path: "scripts/install-master.sh"
        node.vm.provision :shell, privileged: false, path: "scripts/wait-calico-ready.sh"

        node.vm.provision :shell, privileged: false, path: "../scripts/docker-image-save.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/ctr-image.sh", args: [ "-s","-n","k8s.io" ]
        node.vm.provision :shell, privileged: true,  path: "../scripts/flag-set.sh", args: "images"
        node.vm.provision :shell, privileged: true,  path: "../scripts/clean.sh"
      else
        node.vm.provision :shell, privileged: true,  path: "../scripts/flag-wait.sh", args: "packages"
        node.vm.provision :shell, privileged: true,  path: "../scripts/package-restore.sh"

        node.vm.provision :shell, privileged: true,  path: "../scripts/system-base-install.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/docker-install.sh"
        node.vm.provision :shell, privileged: false, path: "../scripts/ca-cert-install.sh"
        node.vm.provision :shell, privileged: true,  path: "scripts/install-base.sh"

        node.vm.provision :shell, privileged: true,  path: "../scripts/flag-wait.sh", args: "images"
        node.vm.provision :shell, privileged: true,  path: "../scripts/proxy-activate.sh"
        # Note: Restoring images is not required when using an image proxy cache, as the images will
        #       be cached there. The image restore used here is used as a proof of concept.
        node.vm.provision :shell, privileged: false, path: "../scripts/docker-image-restore.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/ctr-image.sh", args: [ "-r","-n","k8s.io" ]
        node.vm.provision :shell, privileged: true,  path: "scripts/join.sh"
        node.vm.provision :shell, privileged: true,  path: "scripts/wait-node-ready.sh"
        node.vm.provision :shell, privileged: true,  path: "../scripts/clean.sh"
      end
    end
  end
end
