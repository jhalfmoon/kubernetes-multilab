#!/bin/bash -eu

echo "=== Joining k8s cluster"
sudo bash $K8S_JOIN_FILE

# Install kubeconfig, in case we want to do kubectl stuff on the nodes themselves
DIR=$HOME/.kube
[[ ! -d $DIR ]] && mkdir -p $DIR
cp -L $TEMP_DIR/config $HOME/.kube
