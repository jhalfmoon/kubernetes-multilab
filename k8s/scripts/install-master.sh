#!/bin/bash -eu

echo "=== Initializing k8s master node"

sudo kubeadm init --upload-certs --pod-network-cidr 10.10.0.0/16 --kubernetes-version $K8S_VERSION

# Configure kubeconfig for current user
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
(which kubectx &> /dev/null) && kubectx local=.

# Configure kubeconfig for root
[[ -d /root/.kube ]] || sudo mkdir -p /root/.kube
sudo cp -i $HOME/.kube/config /root/.kube/config

# Make config available on kvm host node
cp -f $HOME/.kube/config $TEMP_DIR/config

#=== Helm based tigera installation. Uses the tigera operator to install the CNI.

cat > /tmp/values.yml <<EOF
# For more info, see : kubectl explain installations.spec
# NOTE: The toleration in this manifest was added to fix an issue that arose with Kubernetes 1.24 / Calico 3.24.5
#       That version of Kubernetes changed the naming of control-plane nodes, breaking Calico. The fix is to
#       add an explicit custom controlPlaneToeleration to the installation manifest. It may no longer be necessary
#       for newer versions of Calico. See https://github.com/projectcalico/calico/issues/6087
installation:
  cni:
    type: Calico
  calicoNetwork:
    ipPools:
    - cidr: 10.10.0.0/16
  # Fix for node-role.kubernetes.io/control-plane:NoSchedule that is in k8s 1.24+
  # This will be unnecessary when the following calico fix is in place:
  # https://github.com/frezbo/calico/commit/87b03bc33bc397a2b10f15ee9c1097e15a0a78dc
  controlPlaneTolerations:
    - key: "node-role.kubernetes.io/control-plane"
      effect: NoSchedule
      operator: Exists
EOF

helm repo add projectcalico https://projectcalico.docs.tigera.io/charts
helm install calico projectcalico/tigera-operator --debug --create-namespace --namespace tigera-operator -f /tmp/values.yml --version v$CALICO_VERSION | tee /tmp/calico-helm.log

# For reference:
#
# kubectl manifest-based tigera installation:
# kubectl create -f https://projectcalico.docs.tigera.io/manifests/tigera-operator.yaml
# cd /tmp
# FILE=custom-resources.yaml
# curl https://projectcalico.docs.tigera.io/manifests/$FILE -O
# sed -ie 's@^\(.*cidr:\).*@\1 10.10.0.0/16@g' $FILE
# kubectl apply -f $FILE
#
# Alternative, non-tigera based installation:
# kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
#
# Identical config on alternate location:
# kubectl apply -f https://projectcalico.docs.tigera.io/manifests/calico.yaml

kubeadm token create --print-join-command > $K8S_JOIN_FILE
