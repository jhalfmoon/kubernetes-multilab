#!/bin/bash -eu

while ! (kubectl get pods --all-namespaces | grep -q 'calico-kube.*Running' &> /dev/null) ; do
    echo "=== Waiting for calico-kube to become running..."
    sleep 10
done
while ! (kubectl get pods --all-namespaces | grep -q 'calico-node.*Running' &> /dev/null) ; do
    echo "===Waiting for calico-node to become running..."
    sleep 10
done
