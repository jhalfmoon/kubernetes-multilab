#!/bin/bash -eu

SHORT_VERSION=$(echo $K8S_VERSION | tr -d '.' | rev | cut -c 2- | rev )

if [[ $SHORT_VERSION -le 123 ]] ; then
    while [[ $(docker container ls | grep 'k8s_calico-node\|k8s_kube-proxy' | wc -l) -ne 2 ]] ; do
        echo "=== Waiting for k8s node to become ready..."
        sleep 10
    done
else
    while [[ $(crictl ps | grep 'calico-node\|kube-proxy' | wc -l) -ne 2 ]] ; do
        echo "=== Waiting for k8s node to become ready..."
        sleep 10
    done
fi
