#!/bin/bash -eu

echo "=== Installing k8s base packages $K8S_VERSION"

# Kubectl installation docs: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux
apt-get install -y apt-transport-https ca-certificates curl

SHORT_VERSION=$(echo $K8S_VERSION | cut -d. -f1,2)

# From the docs: The same signing key is used for all repositories so you can disregard the version in the URL
# see: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
curl -fsSL ${K8S_DEB_REPO_KEY} | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo "${K8S_DEB_REPO_CONFIG}" | sudo tee /etc/apt/sources.list.d/kubernetes.list

apt-get update
apt-get install --allow-change-held-packages -y kubelet=${K8S_VERSION}-* kubeadm=${K8S_VERSION}-* kubectl=${K8S_VERSION}-*
apt-mark hold kubelet kubeadm kubectl

# Configure crictl /etc/crictl.yaml
# see https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md
#
ENDPOINT="unix:///run/containerd/containerd.sock"
cat <<EOF > /etc/crictl.yaml
runtime-endpoint: "$ENDPOINT"
image-endpoint: "$ENDPOINT"
timeout: 2
debug: false
pull-image-on-create: true
disable-pull-on-run: true
EOF

# Alternative way of configuring crictl. Downside is it generates errormessages before it is configured.
# Sending the error messages to /dev/null is not a satisfactory solution, as it masks other possible issues.
#
#ENDPOINT=unix:///$(crictl info 2>/dev/null | jq -r .config.containerdEndpoint)
#crictl config \
#    --set debug=false \
#    --set runtime-endpoint=$ENDPOINT \
#    --set image-endpoint=$ENDPOINT \
#    --set timeout=2 \
#    --set pull-image-on-create=true \
#    --set disable-pull-on-run=true
