#!/bin/bash

cd /tmp
NAME=provider-terraform
if [[ ! -d $NAME ]] ; then
    git clone https://github.com/upbound/$NAME
fi
cd $NAME
kubectl apply -f examples/install.yaml

NAME=crossplane-provider-terraform
while ! (kubectl get workspaces.tf.upbound.io -A) ; do
    echo "$(date) Waiting for $NAME to become available."
    sleep 1
done

kubectl apply -f examples/workspace-random-generator.yaml
