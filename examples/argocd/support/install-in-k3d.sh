#!/bin/bash

# NOTE: Run this from within the vm running k3d

# Test script to develop and test an argo multi-cluster setup:
#   - destroy any existing k3d clusters
#   - bring up two k3d clusters
#   - deploy argo self-managed setup on one cluster
#   - add the new clusters to argo, by name
#   - watch the example application be deployed to both clusters

unset http_proxy https_proxy HTTP_PROXY HTTPS_PROXY

# remove any backgrounded port forwards
sudo killall kubectl

# remove existing clusters
time k3d cluster delete --all

# bring up clusters
time kk1 --agents 0 foo2
time kk1 --agents 0 --port 80:80@loadbalancer --port 443:443@loadbalancer foo1

#echo press enter....
#read

# install dependencies
#which xclip &> /dev/null && apt install -y xclip

# Duplicate sealed secrets chart folder because write access is needed
NAME=sealed-secrets
DEST=/tmp/$NAME
sudo rm -rf $DEST || true
cp -r /vagrant/examples/argocd/content/infra/components/$NAME $DEST
cd $DEST
# Install sealed secrets
helm dependency update
#helm upgrade --install --debug $NAME ./ --namespace kube-namespace | tee /tmp/helm-${NAME}.log
helm upgrade --install --debug --create-namespace --namespace $NAME $NAME .| tee /tmp/helm-${NAME}.log
# Remove argo from Helm's management
kubectl -n $NAME delete secrets -l owner=helm,name=$NAME

# Duplicate argocd installation folder because write access is needed
NAME=argocd
DEST=/tmp/$NAME
sudo rm -rf $DEST || true
cp -r /vagrant/examples/$NAME $DEST
cd $DEST
# Install argocd
helm dependency update

# Due to the way Helm handles CRDs, upgrading older versions of argocd causes issues. Use 'helm template' to properly upgrade argocd.
#helm upgrade --debug --set crds.install=true --install --create-namespace --namespace $NAME $NAME .

kubectl create ns $NAME

helm template --set crds.install=true --debug --include-crds --create-namespace --namespace $NAME $NAME . > /tmp/${NAME}.yaml

# First install CRDSs, along with everthing else. Application manifests will fail to
# install due to missing CRDs, which is expected.
cat /tmp/${NAME}.yaml | kubectl apply -n $NAME -f -

# Next, do the same installation again, this time around the Application manifests
# will be able to be applied, due to the CRDs now being available.
cat /tmp/${NAME}.yaml | kubectl apply -n $NAME -f -

# Remove argo from Helm's management
kubectl -n $NAME delete secrets -l owner=helm,name=$NAME
# Wait until secret is available
while ! PASS=$(kubectl -n $NAME get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" 2> /dev/null ) ; do echo Waiting for login secret to become available... ; sleep 4 ; done
# store password in order to be able to dump it to the terminal at the end of this script
PASS=$(echo $PASS | base64 -d)

echo === Wait for port forward to succeed
while ! (curl -s localhost:8800 > /dev/null) ; do
    nohup kubectl -n $NAME port-forward svc/argocd-server 8800:80 2> /dev/null &
    sleep 4
done

# Install argo cmd line application
FILE=argocd-linux-amd64
while ! (wget --no-check-certificate http://localhost:8800/download/$FILE) ; do sleep 4 ; done
mv $FILE /vagrant/temp/argocd
chmod +rx /vagrant/temp/argocd

# Add argocd command line auto completion
echo 'source <(argocd completion bash)' | sudo tee /etc/profile.d/200-argocd-completion.sh > /dev/null

# Add both clusters to argo by name (foo1 is already known as 'in-cluster')
kubectx k3d-foo1
kubens argocd
argocd login --core
argocd cluster add -y --name acc --label env=acc k3d-foo1
argocd cluster add -y --name prod --label env=prod k3d-foo2

# install kubeseal cmd line tool - https://github.com/bitnami-labs/sealed-secrets/releases
VERSION=0.24.5
FILE=kubeseal-$VERSION-linux-amd64.tar.gz
URL=https://github.com/bitnami-labs/sealed-secrets/releases/download/v$VERSION/$FILE
[[ ! -e $DL_DIR/$FILE ]] && wget -O $DL_DIR/$FILE $URL
cd $DL_DIR
tar xf $DL_DIR/$FILE && sudo cp kubeseal /usr/local/bin

# sync argo to inform it that the clusters are now added
#argocd app sync root-infra

# For demonstration purposes:
# Backup sealed-secrets key. By default sealed-secrets controller will generate a new cert
# if no cert is found. This means that any sealed secret you might have in your repositories,
# will not decrypt, because they were crypted with another cert.
# To be able to test sealed secrets with ephemeral test clusters, you need to import a fixed
# cert, that was used to encrpt the secrets in you repositories.
# You need to copy the exported cert to argocd/sealed-secrets/templates/sealed-secret-cert to
# re-use it.
#
# Note that this only returns a vaild value if sealed-secrets is up and running.
#
# kubectl get -o yaml -n sealed-secrets secret -l sealedsecrets.bitnami.com/sealed-secrets-key | grep -v 'uid\|resourceVersion\|creationTimestamp' > /tmp/ss-key-backup.yaml

echo "=============================================================="
echo
echo "Argocd deployment completed."
echo
echo "* Sealed secrets tip: Pass the namespace to kubeseal like so:"
echo "      kubeseal --controller-namespace sealed-secrets"
echo
echo "* To access argocd ui from your laptop:"
echo "      kubectl -n argocd port-forward svc/argocd-server 8800:80"
echo "  And browse to http://localhost:8800"
echo
echo "* argocd login:"
echo "      user:     admin"
echo "      password: $PASS"
echo
echo "=============================================================="
date

