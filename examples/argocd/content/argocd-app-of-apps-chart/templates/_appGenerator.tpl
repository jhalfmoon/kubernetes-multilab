{{- /* =====================================================*/}}
{{- define "appOfApps.mergeAndGenerateApps" }}
{{- /* =====================================================*/}}
{{- /*
In:
    * .Values.ConfigDirs ; List of diriectories to traverse containing values.
Does:
    * See the comments for the includeded templates for details.
Out:
    * ArgoCD application manifests for the values defined in the passed variable
*/}}
{{ include "appOfApps.mergeFolders" . }}
{{ include "appOfApps.generateApps" . }}
{{- end }}

{{- /* =====================================================*/}}
{{- define "appOfApps.mergeFolders" }}
{{- /* =====================================================*/}}
{{- /*
In:
    * .Values.ConfigDirs ; List of diriectories to traverse. One directory will suffice in most cases.
Does:
    * Traverse the list of directories
    * Reads all *.yaml files in each directory
    * Merges the values of all found yaml files into $mergedConfig
    * Inject $mergedConfig into root context as .Values.mergeConfig
Out:
    * .Values.mergedConfig contains the generated output
*/}}
{{- $mergedConfig := (dict) }}
{{- range $configDir := .Values.configDirs }}
  {{- with $ }}
  {{- range $path, $_ := .Files.Glob (print $configDir "/" "**.yaml") }}
    {{- with $ }}
    {{- $mergedConfig = mergeOverwrite (dict) $mergedConfig (.Files.Get $path | fromYaml) }}
    {{- end }}
  {{- end }}
  {{- end }}
{{- end }}
{{- /* Inject the generated variable into .Variables of the caller  */}}
{{- /* https://helm.sh/docs/chart_template_guide/function_list/#set */}}
{{- $_ := set $.Values "mergedConfig" $mergedConfig }}
{{- end }}

{{- /* =====================================================*/}}
{{- define "appOfApps.generateApps" }}
{{- /* =====================================================*/}}
{{- /*
In:
    * Context must be set to $.Values.mergedConfig, which contains appGroup and default values. See example value file for structure of values.
Does:
    * For each defined appGroup
        - merge the appGroup.defaults with default.appGroupDefaults
        - merge the appGroup.apps with default.apps
        - generate an application manifest for each app, based on app settings merged with default settings
Out:
    * ArgoCD application manifests for the values defined in the passed variable
*/}}
{{- /* Loop over each appGroup */}}
{{- range $groupName,$groupVal := .Values.mergedConfig.appGroups }}
#=== AppGroup: {{ $groupName }} ======================

{{- /* Merge the optional default apps into each appGroup */}}
{{- $apps := mergeOverwrite (deepCopy (default (dict) (($.Values.mergedConfig).default).apps)) (deepCopy (default (dict) ($groupVal).apps)) }}

{{- /* Loop over each app in the appGroup */}}
{{- range $appName,$appVal := $apps }}

{{- /* An app must have at least one parameter (path,repoURL etc) excplictely defined ie. not from defaults */}}
{{- /* This is to prevent accidental empty apps with only "enabled" set and nothing more */}}
{{- if not (or $appVal.source $groupVal.groupDefaults) }}
{{- fail (print "ERROR: At least one path and/or repoUrl must be defined for directory: " $.Values.configDirs ", group: " $groupName ", app: " $appName) }}
{{- end }}

{{- /* Merge default values with custom values - https://olof.tech/helm-values-inheritance-one-liner/ */}}
{{- $appConfig := mergeOverwrite (deepCopy (default (dict) (($.Values.mergedConfig).default).groupDefaults)) (deepCopy (default (dict) ($groupVal).groupDefaults)) $appVal}}

{{- if ($appConfig.enabled | required "ERROR: app.enabled not defined") }}

{{- /* Generate a default name to use to use later, when no specific name is given */}}
{{- $generatedName := (print $appName ) }}
{{- if eq $appConfig.groupNamePosition "prefix" }}
  {{- $generatedName = (print $groupName "-" $appName ) }}
{{- else if (eq $appConfig.groupNamePosition "suffix") }}
  {{- $generatedName = (print $appName "-" $groupName ) }}
{{- end }}

{{- /* Infra applications generally do not need to have a group-identifier appended */}}
{{- /* to the namspace as they tend to be unique for each cluster. */}}
{{- $generatedNamespace := $generatedName }}
{{- if ($appConfig).noGroupInNamespace }}
  {{- $generatedNamespace = $appName }}
{{- end }}

{{- /* Append basepath if it is defined. $path defaults to '.' */}}
{{- $path := default "." $appConfig.source.path }}
{{- if $appConfig.source.basePath }}
  {{- $path = (print $appConfig.source.basePath "/" $path) }}
{{- end }}

{{- $argoNamespace := ($appConfig.argoNamespace | default "argocd") }}

#=== App: {{ $generatedName }} ======================
{{- /* Uncomment one or more of this section help debug this code
#-----------------------
{{ toYaml $appConfig }}
#-----------------------
{{ toYaml $apps }}
#-----------------------
{{ toYaml $groupVal.apps }}
#-----------------------
{{ toYaml $.default.apps }}
#-----------------------
*/}}
# Reference links:
#   https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup
#   https://github.com/argoproj/argo-cd/blob/master/docs/user-guide/application_sources.md
#   https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/application.yaml
#   https://github.com/argoproj/argo-cd/blob/master/docs/user-guide/helm.md
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: {{ $appConfig.name | default $generatedName }}
  namespace: {{ $argoNamespace }}
  labels:
    argocdRootGroup: {{ $groupName }}
  finalizers:
    - resources-finalizer.argocd.argoproj.io
spec:
  project: {{ $appConfig.project | default $generatedName }}
  revisionHistoryLimit: 3
  source:
    repoURL: {{ $appConfig.source.repoURL | required "source.repoURL not defined" }}
    path: {{ $path }}
    targetRevision: {{ $appConfig.source.targetRevision | default "HEAD" }}
    {{- if (hasKey $appConfig.source "chart") }}
    chart: {{ $appConfig.source.chart }}
    {{- end }}
    {{- if (or $appConfig.source.helm (eq $appConfig.source.type "helm")) }}
    helm:
      {{- /* NOTE: By default this template generates the releasename using namespace and group name. */}}
      {{- /*       This will cause issues with applications that are pre-installed, as a requirement  */}}
      {{- /*       for argocd, as these are installed without the group-name in the release name. To  */}}
      {{- /*       prevent this issue, noGroupInNamespace must be set to true for those applications. */}}
      releaseName: {{ ((($appConfig).destination).namespace | default $generatedNamespace)}}
      {{- if $appConfig.source.helm }}
      {{- toYaml $appConfig.source.helm | nindent 6 }}
      {{- end }}
    {{- end }}
  destination:
    name: {{ ternary "in-cluster" ($appConfig.destination).name ($appConfig.isRootApp | default false) | required "destination.name not defined" }}
    namespace: {{ ternary $argoNamespace ((($appConfig).destination).namespace | default $generatedNamespace) ($appConfig.isRootApp | default false ) }}
  {{- if hasKey $appConfig "syncPolicy"}}
  syncPolicy:
    automated:
      {{- if hasKey $appConfig.syncPolicy "prune" }}
      prune: {{ $appConfig.syncPolicy.prune }}
      {{- end }}
      {{- if hasKey $appConfig.syncPolicy "selfHeal" }}
      selfHeal: {{ $appConfig.syncPolicy.selfHeal }}
      {{- end }}
    {{- if hasKey $appConfig.syncPolicy "createNamespace" }}
    syncOptions:
      - CreateNamespace={{ $appConfig.syncPolicy.createNamespace }}
    {{- end }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}
