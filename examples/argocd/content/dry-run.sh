#!/bin/bash

# Test script to generate do a dry-run-generate of the manifests that argocd
# would generate. Use it to check if the output matches what is expected / desired.

HERE="$(dirname $(readlink -f $0))"

dry_run() {
    if false; then
        # This does not produce clean yaml output, but it is human readable and useful for debugging
        echo "@@@ $@  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        helm template --debug --dry-run $@ . 2>&1
    else
        # Alternative: No stderr and no other text, just clean yaml output:
        helm template --debug --dry-run $@ . 2> /dev/null
    fi
    echo
}

# Call this function to continuously update the output on screen.
# This function monitors the output that it generates and updates the
# screen only when a change is detected, to prevent screen-flicker.
watch_it ()
{
    OLDSUM=''
    while true; do
        OUTPUT="$(helm template --debug --dry-run $@ . 2>&1)"
        NEWSUM=$(echo "$OUTPUT" | md5sum)
        if [[ $OLDSUM != $NEWSUM ]]; then
            OLDSUM=$NEWSUM
            clear
            date
            echo "$OUTPUT"
        fi
        sleep 1
    done
}

{
    # Check for tarballs in charts directories, as having a tarball along with
    # folders in charts directories can create some unexpected helm behaviour.
    TARBALLS=$(find -type f | grep -i tgz)
    if [[ -n $TARBALLS ]] ; then
        echo "WARNING! Tarballs found - Delete these to prevent any issus while developping these charts:"
        echo "$TARBALLS"
        sleep 5
    fi

    if false ; then
        cd $HERE/infra
        watch_it --set configDirs={root/infra}
    else
        cd $HERE/root
        dry_run --set configDirs={infra}
        dry_run --set configDirs={apps}
        cd $HERE/infra
        dry_run --set configDirs={clusters/prod}
        dry_run --set configDirs={clusters/acc}
        cd $HERE/apps
        dry_run --set configDirs={groups/team1}
    fi
}

