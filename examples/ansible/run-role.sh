#!/bin/bash

if [[ $# -lt 2 ]] ; then
    echo "This script requires at least 2 parameters:"
    echo "  - A host or group name"
    echo "  - A role name"
    exit 1
fi

HOST=$1
ROLE=$2
shift 2

ansible-playbook --limit $HOST playbooks/role.yml -e "playbook_role=$ROLE" $@

