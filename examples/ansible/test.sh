#!/bin/bash

SIMPLE=1

sudo sed -ie 's/nl\.//g' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive
sudo -E apt-get update
sudo -E apt install -y python3-venv

BASE=/tmp/test

rm -rf $BASE &> /dev/null
rm -rf $HOME/.cache/pip/wheels &> /dev/null

[[ -d $BASE ]] || mkdir -p $BASE
cd $BASE
if [[ ! -e $BASE/bin/activate ]] ; then
    python3 -m venv $BASE
fi
source $BASE/bin/activate

pip install --upgrade pip

#sudo -E apt install -y build-essential python3-dev
#pip install --upgrade pip wheel setuptools
