Ansible in a box
################

Initially based on https://github.com/willhallonline/docker-ansible

Introduction
============

What this repo can do:

* Run ansible CLI tooling in a container as if installed locally.
* Includes support script for building the image and running ansible tooling from within the container.
* The version of ansible and ansible-core can be matched exactly to requirements, using the build script. The default is to use the versions included in the latest rhel8 update.
* The build script ensures custom / corporate CA certificates are made available in the image.
* $(pwd) is mounted to /ansible in the container, ensuring access to the ansible code on the host.
* $SSH_SOCK is mounted into the container, ensuring access to ssh keys on the host system.

The wrapper script that starts the container has a few noteworthy features built in:

* Aliases are created for all ansible CLI tools, such as ansible-playbook, ansible-vault, ansible-config etc.
* A check is done to verify that the commands are called from an ansible repo by checking fo the presence of ansible.cfg.
* The vault primary password is cached across calls using the environment variable ANSIBLE_VAULT_PASSWORD. The ansible wrapper script will check if the variable is set and if not set, a request to enter the password will appear. The wrapper also ensures that any 'prompt' setting in vault_identity_list, that might be in ansible.cfg, are overriden using the variable ANSIBLE_VAULT_IDENTITY_LIST, so that ansible will never prompt for a password, but instead use the supplied ANSIBLE_VAULT_PASSWORD.

Usage
=====
Most steps are only required once or can be automated by letting .bashrc do the work.

* Run build.sh to build the container image.
* Source sourcme.sh. Currently this assumes that bash is used. Placing this action in .bashrc is recommended.

At this point calling ansible will run the application in the container. Further suggested actions are:

* Use `keychain <https://www.funtoo.org/Funtoo:Keychain>`_ or ssh-add to load ssh keys to be used by ansible. Placing this step in .bashrc is recommended.
* Optional: Have the VPN connected to the company network.
* Clone an ansible repo and/or change dir to the root folder containing ansible code.
* Optional - Initialize primary/main vault password using ansible-vault. If migrating from a stepstone, one could also just (s)copy the following files from the stepstone into the ansible repo:
    * ansible-main-vault-password
    * inventory/group_vars/all/ansible-vault

Done. Ansible cli tooling can now be used as if installed locally.

The following environment variables can be set:

* ANSIBLE_REMOTE_USER - Defaults to $(whoami)
* ANSIBLE_NOCOLOR - Set to 'true' to force monochrome text output from ansible
* GITLAB_PAT - If set, it will make git use the username 'git' with password $GITLAB_PAT for authorization. This includes ansible-galaxy calls that access git repos.

If a file called '.shared-vars' is present in the root of the ansible directory, its contents will be used to share environment variables, that are listed in it, with the ansible container. An example of this file is located in the root of this repo.

Building the image
==================

The supplied build script is specific for Linux. The script has pre-configured groups of ansible/ansible-core/ansible-lint that are known to work together.

To build the image:

* Ensure docker is `installed <https://docs.docker.com/engine/install>`_ and that the active user is member of the group 'docker'. Remember to run 'newgrp docker' or reboot after installation. Note that podman would preferably be used for building the image, but due to the lack of an actively maintained and current podman package on Ubuntu, docker is used.
* Optionally edit build.sh and/or Dockerfile to configure the desired versions components such as alpine, ansible and pip modules. The versions of ansible, ansible-core and alpine will be reflected in the image tag.
* Run build.sh.

Optional parameters::

-v                 Select a given set of ansible/ansible-core/ansible-lint versions to be built. Check the build script for more info. Currently available options: rhel8, custom, 213, 214, 215, 216.
-r <registry url>  Configure a registry that the image will be pushed to.
-c                 Flush the build cache before building.

Notes
=====

* The Dockerfiles use cache mounts for apk, pip and cargo package caching, to limit network bandwidth and build time. The dockerfile comments contain details on this.
* The following list explains the purpose of the docker runtime parameters that are used to run the container::

    --network host
        Allow docker to access 127.0.0.53 on the host for name resolution.

    --env ANSIBLE_VAULT_PASSWORD=$ANSIBLE_VAULT_PASSWORD
        Pass the environment variable containing the vault primary password

    --user $(id -u):$(id -g)
        Match the user id in the container to that of the host.
    --mount type=bind,src=/etc/*,dst=/etc/*,ro
        Match user and group names in the container to the host.

    --mount type=tmpfs,dst=$HOME
        Provide a homedir for the active user.
    --mount type=bind,src=$HIST,dst=$HOME/.bash_history
        Provide a persistent .bash_history

    --mount type=bind,src=$(pwd),dst=/ansible
        Mount the current directory into the container
    --mount type=bind,src=$SSH_AUTH_SOCK,dst=ssh-agent
        Mount the socket of the running ssh-agent into the container.

    --env SSH_AUTH_SOCK=/ssh-agent
        Tell ssh where the ssh-agent socket is located.
    --env ANSIBLE_REMOTE_USER=$(whoami)
        Configure ansible to use the current user for all activity.

* A single stage build results in a smaller image for this particular build. A multistage build would use a COPY statement to transfer the files from the build stage to the final stage, which causes a persistent layer that does not shrink if files are removed from it.

Example commands::

    ansible -m ping localhost
    ansible -m setup localhost
    ansible-playbook --limit somehost01,anotherhost01 playbooks/all.yml --diff --check
    ansible-vault encrypt_string "$(cat ~/priv)" | xclip

Links
=====

* https://docs.ansible.com/ansible/latest/reference_appendices/config.html
* https://docs.ansible.com/ansible/latest/reference_appendices/general_precedence.html

On ansible vault:

* https://docs.ansible.com/ansible/latest/vault_guide/index.html
* https://docs.ansible.com/ansible/latest/reference_appendices/config.html#envvar-ANSIBLE_ASK_VAULT_PASS
* https://developers.redhat.com/blog/2020/01/30/vault-ids-in-red-hat-ansible-and-red-hat-ansible-tower
* https://github.com/ansible/ansible/issues/45214

