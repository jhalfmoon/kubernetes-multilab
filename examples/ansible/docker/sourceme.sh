# Source this file. For example from your .bashrc file.
#
# It defines wrapper functions to run ansible CLI tooling in a container as if installed locally.

# Create aliases for the following commands
ANSIBLE_COMMANDS="
    ansible
    ansible-playbook
    ansible-config
    ansible-console
    ansible-galaxy
    ansible-lint
    ansible-pull
    ansible-vault
    ansible-community
    ansible-connection
    ansible-doc
    ansible-inventory
    ansible-test
"

ansible_in_a_box() {
    # default image name
    local IMAGE=ansible
    # location of persistent bash_history
    local HIST=$HOME/.bash_history_containers
    # optional extra parameters for docker
    local EXTRA_PARAMS=''
    # If this file exists, it will be used to pass the variables listed in it to the docker container.
    # Only the variable names will be passed to the commandline. Assigned values and comments will be ignored.
    local VAR_FILE=.shared-vars

    # Exit if ansible.cfg is not found in the current directory
    if [[ -e ansible.cfg ]] ; then
        ANSIBLE_REPO_DIR=$(pwd)
    else
        echo "ERROR: ansible.cfg not found in current directory. Exiting." >&2
        return 1
    fi

    # Prepare the docker command line parameters for passing variable listed in $VAR_FILE
    if [[ -e $VAR_FILE ]] ; then
        echo "INFO: The variables listed in .shared-vars will be shared with the container." >&2
        while read VAR ; do
            # Note: Bash variable indirection (!) is used here
            # https://stackoverflow.com/questions/8515411/what-is-indirect-expansion-what-does-var-mean
            EXTRA_PARAMS+=" --env $VAR=${!VAR}"
        done < <(cat $VAR_FILE | grep -v '^\s*#\|^\s*$' | cut -d= -f1)
    fi

    # Create the persistent storage for .bash_history in the container
    [[ ! -e $HIST ]] && touch $HIST

    # If ANSIBLE_VER is set, then use that tag with the image name. Setting it to 'rhel8' for example will use alpine:rhel8 as image.
    # By default no tag will be used, which will make the tag 'latest' to be used.
    [[ -n $ANSIBLE_VER ]] && IMAGE=ansible:${ANSIBLE_VER}

    # Exit if the requested image is not available
    if [ -z "$(docker images -q $IMAGE 2> /dev/null)" ]; then
        echo ERROR: Image $IMAGE as set by \$ANSIBLE_VER does not exist. >&2
        return 1
    fi

    # If vault_identity_list is defined in ansible.cfg and 'prompt' is used in it, then do the following:
    # * Override the config in ansible.cfg using the environment variable ANSIBLE_VAULT_IDENTITY_LIST. In it, replace
    #   the prompt parameter with a reference to a script that echos $VAULT_PRIMARY_PASSWORD. This is done so that
    #   a password can be passed to the container and ansible will then use it.
    # * Query for the vault primary password and store it in an environment variable for later use.
    ANSIBLE_VAULT_IDENTITY_LIST=$(cat ansible.cfg | grep vault_identity_list | tr -d ' ' | cut -d= -f2 | sed 's/prompt/\/echo-primary-password.sh/g')
    if [[ -n "$ANSIBLE_VAULT_IDENTITY_LIST" ]] ; then
        EXTRA_PARAMS+=" --env ANSIBLE_VAULT_IDENTITY_LIST=$ANSIBLE_VAULT_IDENTITY_LIST"
        # Check for availability of vault password
        if [[ -z "$VAULT_PRIMARY_PASSWORD" ]] ; then
            # First, use escape codes to set the window title of the terminal, for use by a password manager
            echo -ne '\e]0;Enter vault passphrase\a'
            # Request the password
            echo "VAULT_PRIMARY_PASSWORD not set, please enter it now:" >&2
            read -s VAULT_PRIMARY_PASSWORD
        else
            echo "VAULT_PRIMARY_PASSWORD is set and will be used." >&2
        fi
        # Next, use escape codes to set the window title of the terminal again, for use by a password manager
        echo -ne '\e]0;Ansible run in progress\a'
    fi

    # If not explicit username has been set, then assume that it should be the name of the user calling this function
    [[ -z "$ANSIBLE_REMOTE_USER" ]] && ANSIBLE_REMOTE_USER=$(whoami)

    # https://stackoverflow.com/questions/911168/how-can-i-detect-if-my-shell-script-is-running-through-a-pipe
    # Only add the --interactive parameter when this function is called interactively.
    tty -s && EXTRA_PARAMS+=' --interactive'

    docker run \
    ${EXTRA_PARAMS[@]} \
    --rm \
    --tty \
    --network host \
    --user $(id -u):$(id -g) \
    --env VAULT_PRIMARY_PASSWORD=$VAULT_PRIMARY_PASSWORD \
    --env ANSIBLE_BECOME_PASSWORD=$ANSIBLE_BECOME_PASSWORD \
    --env ANSIBLE_REMOTE_USER=$ANSIBLE_REMOTE_USER \
    --env ANSIBLE_NOCOLOR=$ANSIBLE_NOCOLOR \
    --env GITLAB_PAT=$GITLAB_PAT \
    --env SSH_AUTH_SOCK=/ssh-agent \
    --mount type=bind,src=$SSH_AUTH_SOCK,dst=/ssh-agent \
    --mount type=tmpfs,dst=$HOME \
    --mount type=bind,src=$HIST,dst=$HOME/.bash_history \
    --mount type=bind,src=$HOME/.ssh/known_hosts,dst=$HOME/.ssh/known_hosts \
    --mount type=bind,src=$ANSIBLE_REPO_DIR,dst=/ansible \
    --mount type=bind,src=/etc/passwd,dst=/etc/passwd,ro \
    --mount type=bind,src=/etc/group,dst=/etc/group,ro \
    --mount type=bind,src=/etc/shadow,dst=/etc/shadow,ro \
    --mount type=bind,src=/tmp,dst=/tmp \
    $IMAGE \
    "$@"
}

# Make this function available for use in scripts
export -f ansible_in_a_box

# Create a wrapper function for each ansible command in the given list
for NAME in $ANSIBLE_COMMANDS ; do
    eval "$NAME() {
        # Quoting the paramaters is needed to re-pass them to the function as a whole.
        # Escaping the quotes and @ is needed to prevent them from being interpreted during definition of the function.
        ansible_in_a_box $NAME \"\$@\"
    }"
    export -f $NAME
done

# This alias is can be useful for debugging
alias abash="ansible_in_a_box bash"

