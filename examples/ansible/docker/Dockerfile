# An ansible-in-a-box build file.
# Based on https://github.com/willhallonline/docker-ansible

# Note that for this particuar build file,  a single stage build yields a small image than a
# multi-stage build, due to the fact that a COPY command creates a layer that does not reduce
# in size when you delete the data that is copied within that layer.

# Pass sane default for this argument; if you don't then docker build will throw a warning.
ARG ALPINE_VER=3.20
FROM alpine:${ALPINE_VER}

ARG ANSIBLE_VER
ARG ANSIBLE_CORE_VER
ARG ANSIBLE_LINT_VER
ARG BUILD_DATE
ARG BUILD_REF_NAME
ARG COMMIT_DATE
ARG COMMIT_HASH

# NOTE:
# * Label Schema Convention (org.label-schema.*) is deprecated in favour of OCI image spec
#   Reference:  https://github.com/opencontainers/image-spec/blob/main/annotations.md
#               https://specs.opencontainers.org/image-spec/annotations/
#
LABEL org.opencontainers.image.description="ansible, ansible-core and ansible lint CLI tooling"
LABEL org.opencontainers.image.created=${BUILD_DATE}
LABEL org.opencontainers.image.revision=${COMMIT_HASH}
LABEL org.opencontainers.image.ref-name=${BUILD_REF_NAME}
#LABEL org.opencontainers.image.version='v5.2.42'
#LABEL org.opencontainers.image.authors='J. Doe <j.doe@example.com>, Q. Public <qpub@company.example.com>'
#LABEL org.opencontainers.image.url=https://gitlab.com/foo/bar.git
#LABEL org.opencontainers.image.documentation=https://some.site/docs
#LABEL org.opencontainers.image.licenses='GPL-3.0-only / Apache-2.0'

COPY requirements.txt /requirements.txt

# NOTE:
# * Cache mounts are used to minimize bandwidth usage and potentially increase build speed. As a consequence, 'apk --no-cache' and
#   'pip --no-cache-dir' are not used and there is no 'rm -rf $SOME_CACHE'.
#    reference: https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/reference.md#run---mounttypecache
# * When installing wheel packages, the pip --no-index flag will prevent PIP from accessing PyPI.
# * apk --cache-max-age 120 is used to refresh the cache at most every 120 minutes.
# * To clear the cache mounts, run 'docker builder prune -f' .
#
RUN --mount=type=cache,mode=0755,target=/var/cache/apk \
    --mount=type=cache,mode=0755,target=/root/.cache/pip \
    --mount=type=cache,mode=0755,target=/root/cargo \
    apk add --cache-max-age 120 \
        python3\
        py3-pip \
        openssl \
        openssh-client \
        ca-certificates \
        bash \
        vim \
        git \
        curl \
        bind-tools && \
    apk add --cache-max-age 120 --virtual build-deps \
        build-base \
        python3-dev \
        openldap-dev \
        libffi-dev && \
    rm -rf /usr/lib/python3.*/EXTERNALLY-MANAGED && \
    pip3 wheel --wheel-dir /wheels -r /requirements.txt \
        wheel \
        ansible==${ANSIBLE_VER} \
        ansible-core==${ANSIBLE_CORE_VER} \
        ansible-lint==${ANSIBLE_LINT_VER} && \
    pip3 install --no-index /wheels/* && \
    rm -rf /wheels && \
    apk del build-deps

# NOTE:
# * The safe.directory statement is used to fix the issue with git files not being owned by the user in the container.
#   Reference: https://git-scm.com/docs/git-config#Documentation/git-config.txt-safedirectory
# * The helper script checks for presence of the $GITLAB_PAT variable and if it exists, uses it to authenticate using user git and the password $GITLAB_PAT
# * The cache statement is used for caching credentials used by ansible-galaxy when accessing git repositories, so galaxy-updates for example only ask for credentials once
# * The git settings are all set system-wide, so will reside in /etc/gitconfig
RUN mkdir /ansible && \
    mkdir -p /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts && \
    git config --system safe.directory '*' && \
    git config --system credential.helper 'cache --timeout=3600' && \
    git config --system --add credential.helper '!f() { if [[ -n "$GITLAB_PAT" ]] ; then echo username=git; echo password=$GITLAB_PAT ; fi ; }; f'

# This file will / can be used to pass a vault password via an environment variable.
# Reference: https://docs.ansible.com/ansible/latest/vault_guide/vault_using_encrypted_content.html#setting-a-default-password-source
COPY echo-primary-password.sh /

COPY certificates/* /usr/local/share/ca-certificates/

RUN update-ca-certificates

WORKDIR /ansible

CMD [ "bash" ]
