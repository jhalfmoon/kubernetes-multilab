#!/bin/bash -eu
#
# This script does a few sanity checks, sets a few important variables such as application
# versions and labels, and then triggers the docker build process.
#
# * Check out the getopts part below for possible input parameters.
# * If $REGISTRY is set , an image will created that can be pushed to that registry.
#   This script does not trigger the actual push.
# * $GROUP_VER determines which collection of ansible components get used.
# * $ALPINE_VER determines which alpine image version is used as a base image.
# * By default three tags are created:
#     * $BASE_NAME only, with no version tag ie. latest
#     * $BASE_NAME with tag containing the group name, for example 'custom' or '216'
#     * $BASE_NAME containing registry URL and version numbers of main components, ready to be pushed

fail() {
    echo $1
    exit 1
}

# https://hub.docker.com/_/alpine/tags
ALPINE_VER=3.20.3
IMAGE_BASE_NAME=ansible
# Default group to build
GROUP_VER=newest
# Set this to 1 to clear the cache layers created with --mount=type=cache
CLEAR_BUILD_CACHE=0
# If not pushing to a registry then leave this empty. Example setting: my.registry.address:port.
REGISTRY=""

HERE=$(dirname $(readlink -f $0))
cd $HERE

while getopts "cr:v:" opt; do
  case $opt in
    c)  CLEAR_BUILD_CACHE=1
        ;;
    r)  REGISTRY=$OPTARG
        ;;
    v)  GROUP_VER=$OPTARG
        ;;
    *)  fail "Invalid option: -$OPTARG"
        ;;
  esac
done

# Check connectivity with registry
if [[ -n $REGISTRY ]] ; then
    if ! (which curl &>/dev/null) ; then
        echo "WARNING: curl is not installed. Cannot validate connectivity with registry $REGISTRY."
        echo "Press enter to continue."
        read
    elif ! (curl http://$REGISTRY &>/dev/null) ; then
        echo "WARNING: Unable to contact registry $REGISTRY - Press enter to continue"
        read
    fi
    # add trailing slash to registry string
    REGISTRY="${REGISTRY}/"
fi

# NOTE: This part groups versions together that are known to work together (213...216) and/ or
#       that match a setup that you require.

case $GROUP_VER in
    213)
        ANSIBLE_VER=6.7.0
        ANSIBLE_CORE_VER=2.13.13
        ANSIBLE_LINT_VER=5.4.0
        ;;
    214)
        ANSIBLE_VER=7.7.0
        ANSIBLE_CORE_VER=2.14.14
        ANSIBLE_LINT_VER=6.20.3
        ;;
    215)
        ANSIBLE_VER=8.7.0
        ANSIBLE_CORE_VER=2.15.9
        ANSIBLE_LINT_VER=6.20.3
        ;;
    216)
        ANSIBLE_VER=9.2.0
        ANSIBLE_CORE_VER=2.16.4
        ANSIBLE_LINT_VER=6.22.2
        ;;
    custom)
        ANSIBLE_VER=9.2.0
        ANSIBLE_CORE_VER=2.16.3
        ANSIBLE_LINT_VER=6.22.2
        ;;
    newest)
        # https://pypi.org/project/ansible/#history
        # https://pypi.org/project/ansible-core/#history
        # https://pypi.org/project/ansible-lint/#history
        ANSIBLE_VER=10.4.0
        ANSIBLE_CORE_VER=2.17.4
        ANSIBLE_LINT_VER=24.9.2
        ;;
    *)
        echo "Unknown group version: $GROUP_VER"
        exit 1
        ;;
esac

VERSIONS=${ANSIBLE_CORE_VER}-${ANSIBLE_VER}-${ALPINE_VER}
VERSIONS_AND_NAMES="ansiblecore-${ANSIBLE_CORE_VER}-ansible-${ANSIBLE_VER}-alpine-${ALPINE_VER}"

# The following variables will be used to set image metadata.
#
# RFC3339 time format - https://www.ietf.org/rfc/rfc3339.txt
BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
COMMIT_DATE=$(git show -s --format=%ci)
COMMIT_HASH=$(git rev-parse HEAD)

[[ $CLEAR_BUILD_CACHE -eq 1 ]] && docker builder prune -f
time docker build \
    --progress=plain \
    --build-arg ALPINE_VER=${ALPINE_VER} \
    --build-arg ANSIBLE_VER=${ANSIBLE_VER} \
    --build-arg ANSIBLE_CORE_VER=${ANSIBLE_CORE_VER} \
    --build-arg ANSIBLE_LINT_VER=${ANSIBLE_LINT_VER} \
    --build-arg BUILD_REF_NAME="${VERSIONS_AND_NAMES}" \
    --build-arg BUILD_DATE=${BUILD_DATE} \
    --build-arg COMMIT_DATE="${COMMIT_DATE}" \
    --build-arg COMMIT_HASH=${COMMIT_HASH} \
    -t ${REGISTRY}${IMAGE_BASE_NAME}:${VERSIONS} \
    -t ${IMAGE_BASE_NAME}:${GROUP_VER} \
    -t ${IMAGE_BASE_NAME} \
    .

