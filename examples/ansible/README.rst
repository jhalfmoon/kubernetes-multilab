Ansible test and development setup.

Examples
========
::

    ansible -m setup h1 | sed '1 s/^.*$/{/' | jq '.ansible_facts | keys'
    ansible -m ping h1
    ansible -m debug -a var=hostvars[h1] all
    ansible-playbook playbooks/demo1.yml

    VAULT_PRIMARY_PASSWORD=123
    alias ae="ansible-vault encrypt_string"
    alias av="ansible-vault view --vault-id primary@/echo-primary-password.sh --vault-id secondary@vault-secondary-password"

    # The following depends on the following variable being set:
    #   ANSIBLE_VAULT_IDENTITY_LIST=primary@/echo-primary-password.sh,secondary@vault-secondary-password

    unset VAULT_PRIMARY_PASSWORD
    ae supersecret  --encrypt-vault-id primary   --output vault-secondary-password
    ae supersecret2 --encrypt-vault-id secondary --output some-secret
    av vault-secondary-password
    av some-secret


Galaxy
======
::

    ansible-galaxy role search awx
    ansible-galaxy role info robertdebock.awx
    ansible-galaxy role install robertdebock.awx

https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#setting-where-to-install-roles

