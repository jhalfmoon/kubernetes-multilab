#!/bin/bash
#
# Source me.
# Simple ansible setup for testing.

BASE=/tmp/ansible
KEY=vagrant-insecure

# Set this to '1' to work around broken ubuntu nl.* mirrors
USE_FIX=0

# This function allows to only run apt-get if it has been longer thatn $2 minutes ago
is_modified_within_minutes() {
  test -f "$1" && test -z "$(find "$1" -mmin +$2)"
}

if [[ $USE_FIX -eq 1 ]] ; then
    # fix for broken .nl mirrors
    sudo sed -ie 's/nl\.//g' /etc/apt/sources.list
    sudo apt-get update
else
    is_modified_within_minutes /var/cache/apt/pkgcache.bin 180 || sudo apt-get update
fi

# sudo apt update
sudo apt install -y python3-venv keychain

if [[ ! -e $BASE/bin/activate ]] ; then
    echo Creating Ansible venv
    python3 -m venv $BASE
fi
source $BASE/bin/activate
pip install --upgrade pip
pip install ansible

# If the vagrant insecure key is available then install it
if [[ -e /vagrant/support/$KEY ]] ; then
    if [[ ! -e ~/$KEY ]] ; then
        cp /vagrant/support/$KEY ~
        chmod 600 ~/$KEY
    fi
    keychain ~/$KEY
    source ~/.keychain/*-sh
    echo
    echo === Keys currently in ssh-agent
    ssh-add -L
    echo
fi

DIR=/vagrant/examples/ansible
[[ -d $DIR ]] && cd $DIR

