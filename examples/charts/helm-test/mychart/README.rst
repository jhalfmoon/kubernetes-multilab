Radom notes::

    # Inject a variable into root context to be able to do tpl calls.
    # Rationale: tpl requires .Template.BaesPathh to run, so by injecting a variable into root,
    # which has .Template, tpl calls can de done with error.
    {{- $_ := set $.Values "appConfig" $appConfig }}
    {{ tpl $.Values.appConfig.destination.namespace $ }}

    # Alternative method to do tpl calls on variables.
    # Create a dict and copy .Templates from root in the dicht
    {{- with (dict "Values" $appConfig "Template" $.Template) }}
    {{ tpl .Values.destination.namespace2 . }}
    {{ end }}

    # This should also work, but it doens't. It's here to remind me.
    {{ tpl .Values.destination.namespace2 (dict "Values" $appConfig "Template" $.Template) }}

