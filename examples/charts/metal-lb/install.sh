#!/bin/bash

# https://doc.traefik.io/traefik/getting-started/install-traefik/#use-the-helm-chart
# https://github.com/traefik/traefik
# https://github.com/traefik/traefik-helm-chart
# https://github.com/metallb/metallb/releases

HERE=$(dirname $(readlink -f $0))

CHART_VERSION=0.13.11
CHART_NAME=metallb
REPO_NAME=metallb
REPO_URL=https://metallb.github.io/metallb
NAMESPACE=metal-lb
RELEASE_NAME=$NAMESPACE

helm repo add $REPO_NAME $REPO_URL
helm repo update

# Note: The helm release name is used for the ingressclass name of the ingress
helm upgrade --install --debug $RELEASE_NAME $REPO_NAME/$CHART_NAME --create-namespace --namespace $NAMESPACE --version $CHART_VERSION -f $HERE/custom-values.yaml | tee /tmp/helm-${NAMESPACE}.log

#kubectl apply -f traefik/netpol-all-to-ingress.yaml
#kubectl apply -f traefik/ingress-dashboard.yaml

# test dashboard
# k port-forward traefik-667b854789-8vp5n 8080:9000
# curl http://localhost:8080/dashboard/

