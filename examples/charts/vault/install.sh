#!/bin/bash

# https://github.com/hashicorp/vault/releases
# https://www.vaultproject.io/docs/platform/k8s/helm

HERE=$(dirname $(readlink -f $0))

# argo-cd appversion in chart is 1.10.3
CHART_VERSION=0.20.1
REPO_NAME=hashicorp
CHART_NAME=vault
REPO_URL=https://helm.releases.hashicorp.com
NAMESPACE=vault
RELEASE_NAME=$NAMESPACE

helm repo add $REPO_NAME $REPO_URL
helm repo update
#helm dependency update argo-cd
helm upgrade --install --debug $RELEASE_NAME $REPO_NAME/$CHART_NAME --create-namespace --namespace $NAMESPACE --version $CHART_VERSION -f $HERE/custom-values.yaml | tee /tmp/helm-${NAMESPACE}.log

#kubectl apply -f $NAMESPACE/ingress-http.yaml
#kubectl apply -f $NAMESPACE/ingress-grpc.yaml

