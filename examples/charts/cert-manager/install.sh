#!/bin/bash

# https://github.com/cert-manager/cert-manager/releases
# https://cert-manager.io/docs/

HERE=$(dirname $(readlink -f $0))

CHART_VERSION=v1.13.1
CHART_NAME=cert-manager
REPO_NAME=jetstack
REPO_URL=https://charts.jetstack.io
NAMESPACE=cert-manager
RELEASE_NAME=$NAMESPACE
CMDLINE_VALUES='--set installCRDs=true'

helm repo add $REPO_NAME $REPO_URL
helm repo update

#kubectl apply -f cert-manager/corporate-secret.yaml

# Note: The helm release name is used for the ingressclass name of the ingress

#helm upgrade --install $RELEASE_NAME $REPO_NAME/$CHART_NAME --create-namespace --namespace $NAMESPACE --version $CHART_VERSION $CMDLINE_VALUES

helm upgrade --install --debug $RELEASE_NAME $REPO_NAME/$CHART_NAME --create-namespace --namespace $NAMESPACE --version $CHART_VERSION -f $HERE/custom-values.yaml | tee /tmp/helm-${NAMESPACE}.log

#kubectl apply -f cluster-issuer.yaml

