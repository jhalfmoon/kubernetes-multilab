#!/bin/bash

# backup a running etcd node and restore to a new directory
# only step needed is to stop api-server, restart etcd with new path, and restart api-server

BACKUP_FILE=/var/lib/etcd/mybackup.db
RESTORE_DIR=/var/lib/etcd/etcd-restore

LIST=$(kubectl describe pod etcd-kube1 -n kube-system | grep -- '--' | grep 'server\|ca\|adver' | grep -v peer | sed 's/.*--//g' | sed 's/\(.*\)/_\1/g')
URL=$(echo "$LIST" | grep _adv | cut -d= -f2)
CA=$(echo "$LIST" | grep _trusted | cut -d= -f2)
CERT=$(echo "$LIST" | grep _cert| cut -d= -f2)
KEY=$(echo "$LIST" | grep _key | cut -d= -f2)

kubectl exec etcd-kube1 -n kube-system -- sh -c "ETCDCTL_API=3 etcdctl --endpoints=$URL --cacert=$CA --cert=$CERT --key=$KEY snapshot save $BACKUP_FILE"

kubectl exec etcd-kube1 -n kube-system -- sh -c "ETCDCTL_API=3 etcdctl snapshot restore --data-dir $RESTORE_DIR $BACKUP_FILE"

# this pod wil be gone after the restore
kubectl run foofoo --image nginx

ssh k1 -- "sudo mv $BACKUP_FILE /var/lib/"
ssh k1 -- "sudo mv $RESTORE_DIR /var/lib/"

