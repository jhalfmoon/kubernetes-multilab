#!/bin/bash -eux

# Script to (re)create a kubernetes cluster and (re)install a helm chart
# Example 1; install old version of traeifk and then upgrade to new version
#   ./test-chart-upgrade -a traefik -o
#   ./test-chart-upgrade -a traefik -n -k

UMBRELLA=0
KEEP=0
K3D_IMAGE=+v1.24

APP=''
CHART_VERSION=''
VALUES=''
K3S_ARGS=''
HELM_SET=''

while getopts "a:ondukv:V:" options; do
    case $options in
        a)  APP="$OPTARG"           # (a)pplication
            ;;
        o)  CHART_VERSION="OLD"     # installl (o)ld version
            ;;
        n)  CHART_VERSION="NEW"     # install (n)ew version
            ;;
        d)  UMBRELLA=0              # use chart (d)irectly from helm repo to install traefik
            ;;
        u)  UMBRELLA=1              # use (u)mbrella chart to install traefik
            ;;
        k)  KEEP=1                  # (k)eep the current cluster ie. do not delete and recreate the cluster
            ;;
        V)  VALUES="-f $OPTARG"     # (V)alue file
            ;;
        v)  CHART_VERSION="$OPTARG" # custom (v)ersion
            ;;
    esac
done
shift $((OPTIND-1))

case $APP in
    prometheus)
            UMBRELLA_DIR=/work/dummy
            REPO_URL=https://prometheus-community.github.io/helm-charts
            REPO_NAME=prom
            CHART_NAME=prometheus
            NAMESPACE=$CHART_NAME
            OLD_VERSION=18.0.0
            NEW_VERSION=20.2.0
            ;;
    sealed-secrets)
            UMBRELLA_DIR=/work/sealed-secrets
            REPO_URL=https://bitnami-labs.github.io/sealed-secrets
            REPO_NAME=bitname
            CHART_NAME=sealed-secrets
            NAMESPACE=$CHART_NAME
            OLD_VERSION=2.3.0
            NEW_VERSION=2.8.1
            ;;
    cert-manager)
            # https://cert-manager.io/docs/installation/upgrading/
            # https://cert-manager.io/docs/tutorials/backup/
            #   backup
            #       kubectl get --all-namespaces -oyaml issuer,clusterissuer,cert > backup.yaml
            #   restore
            #       kubectl apply -f <(awk '!/^ *(resourceVersion|uid): [^ ]+$/' backup.yaml)
            UMBRELLA_DIR=/work/cert-manager
            REPO_URL=https://charts.jetstack.io
            REPO_NAME=jetstack
            CHART_NAME=cert-manager
            NAMESPACE=$CHART_NAME
            OLD_VERSION=v1.8.2
            NEW_VERSION=v1.11.0
            HELM_SET='--set installCRDs=true'
            ;;
    traefik)
            UMBRELLA_DIR=/work/traefik
            VALUES='-f environment/staging.yaml'
            REPO_URL=https://traefik.github.io/charts
            REPO_NAME=traefik
            CHART_NAME=traefik
            NAMESPACE=$CHART_NAME
            OLD_VERSION=v10.24.0
            NEW_VERSION=v22.0.0
            K3S_ARGS='--k3s-arg=\"--disable=traefik@server:*\"'
            ;;
    *)      echo "ERROR: No app defined. Use -a \$NAME to define it."
            exit 1
            ;;
esac

if [[ -z $CHART_VERSION ]] ; then
    echo "ERROR: Requires -o(ld)  | -n(ew) | -v(ersion) <version>"
    exit 1
elif [[ $CHART_VERSION == 'NEW' ]] ; then
        CHART_VERSION=$NEW_VERSION
elif [[ $CHART_VERSION == 'OLD' ]] ; then
        CHART_VERSION=$OLD_VERSION
fi

MANIFEST_FILE=/tmp/$CHART_NAME-$CHART_VERSION.yaml

# Optional: Wipe and reprovision cluster
if [[ $KEEP -eq 0 ]] ; then
    k3d cluster delete --all
    k3d cluster create test1 --image $K3D_IMAGE --config /etc/k3d-default-config.yml $K3S_ARGS
fi

if [[ $UMBRELLA -eq 0 ]]; then
    # Install directly from helm chart
    #
    helm repo add $REPO_NAME $REPO_URL
    helm repo update
    #helm install $HELM_SET $VALUES --version $CHART_VERSION --create-namespace --namespace $NAMESPACE $CHART_NAME $REPO_NAME/$CHART_NAME
    rm -rf /tmp/${CHART_NAME}-${CHART_VERSION} || true
    helm pull --untar --untardir /tmp/${CHART_NAME}-${CHART_VERSION} --version $CHART_VERSION $REPO_NAME/$CHART_NAME
    helm template --debug --include-crds $HELM_SET --version $CHART_VERSION --create-namespace --namespace $NAMESPACE $CHART_NAME $REPO_NAME/$CHART_NAME > $MANIFEST_FILE
    kubectl create ns $NAMESPACE || true
    # NOTE: Deleting jobs is done for the cert-manager install but it probably does
    #       not negatively impact other installations.
    kubectl delete -n $NAMESPACE jobs --all
    #
    #cat $MANIFEST_FILE | kubectl apply --server-side=true -f -
    cat $MANIFEST_FILE | kubectl apply -f -
else
    # Install from a local umbrella chart
    #
    if [[ -z "$UMBRELLA_DIR" ]] ; then
        echo "ERROR: No UMBRELLA_DIR defined."
        exit 1
    fi
    TEMP_DIR=/tmp/$CHART_NAME
    rm -rf $TEMP_DIR
    cp -r $UMBRELLA_DIR $TEMP_DIR/
    cd $TEMP_DIR
    sed -i "s/ version:.*/ version: $CHART_VERSION/g" Chart.yaml
    rm -f Chart.lock
    helm dependency build
    # helm install --debug --create-namespace --namespace $NAMESPACE $CHART_NAME .
    helm template --debug --include-crds $VALUES --create-namespace --namespace $NAMESPACE $CHART_NAME . > $MANIFEST_FILE
    kubectl create ns $NAMESPACE || true
    cat $MANIFEST_FILE | kubectl apply -f -
fi

echo MANIFEST_FILE : $MANIFEST_FILE
