#!/bin/bash

# This is sets up a caching, multi-registry proxy, which uses traefik.
# Via https://github.com/rancher/k3d/issues/238 :
#   https://github.com/frimik/local-docker-registry-proxy
# Might be useful for tweaking the config:
#   https://docs.docker.com/compose/compose-file/compose-file-v3/

echo "=== Installing docker registry caching proxy (Type: Multi registry via traefik)"

PROXY_NAME=local-docker-registry-proxy
PROXY_LINK=https://github.com/frimik/$PROXY_NAME.git

# Download proxy setup
cd $TEMP_DIR
if [[ ! -d $PROXY_NAME ]] ; then
    git clone $PROXY_LINK
fi
cp -r $PROXY_NAME $HOME

# create proxy certs
cd $HOME/$PROXY_NAME/services/traefik/certs
mkcert "*.local"

cd $HOME/$PROXY_NAME
# patch the compose file to point storage volumes to persistent storage so that we can retain the cache between reboots
sed -i 's@\(- "\)\(registry.*\)\(:.*\)@\1/vagrant/temp/registry/\2\3@g' docker-compose-local-registry.yml docker-compose.yml

# create storage folder and chow 777 to prevent silly docker-compose errors on mount
[[ -d $REGISTRY_DIR ]] || mkdir -p $REGISTRY_DIR
chmod -R 777 $REGISTRY_DIR

# start the proxies
docker-compose -f docker-compose.yml -f docker-compose-local-registry.yml up -d

cat <<"EOF" >> /etc/profile.d/999_k3d.sh
k3() { k3d cluster create foo\
    --servers 1 \
    --agents 2 \
    --wait \
    --k3s-server-arg='--no-deploy=traefik' \
    --volume "/root/local-docker-registry-proxy/registries.yaml:/etc/rancher/k3s/registries.yaml" \
    --volume "/root/.local/share/mkcert/rootCA.pem:/etc/ssl/certs/Registry_Root_CA.pem" \
    --network k3d-backend ; }
EOF

