#!/bin/bash

GITHUB_TOKEN=FOO_BAR
kubectl create namespace actions-runner-system
kubectl create secret generic controller-manager -n actions-runner-system --from-literal=github_token=${GITHUB_TOKEN}
kubectl apply -f https://github.com/actions-runner-controller/actions-runner-controller/releases/download/v0.20.4/actions-runner-controller.yaml

