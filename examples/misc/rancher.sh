#!/bin/bash

# https://rancher.com/docs/rancher/v2.5/en/installation/install-rancher-on-k8s/
# https://rancher.com/docs/rancher/v2.5/en/installation/install-rancher-on-k8s/chart-options/#http-proxy
# https://github.com/rancher/rancher/releases

echo "=== Installing rancher"

HERE=$(dirname $(readlink -f $0))

# restore any images that might have been stored previously
#$HERE/docker-image-restore.sh $IMAGE_DIR/rancher

#kubectl get pods --namespace cert-manager

echo "--- Installing rancher (latest)"
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm repo update
kubectl create namespace cattle-system
helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=rancher.k3d.localhost
time kubectl -n cattle-system rollout status deploy/rancher
kubectl -n cattle-system get deploy rancher

# Some notes on setting the proxy:
#  --set proxy="http://$PROXY_IP:$PROXY_PORT/" \
#  --set noproxy="echo $(echo ${NOPROXY_LIST} | sed 's/,/\\,/g')" \
#
# This bit can be deleted if the '--set proxy' bit works correctly
#  --set "extraEnv[0].name=HTTP_PROXY"
#  --set "extraEnv[0].value=http://$PROXY_IP:$PROXY_PORT"
#  --set "extraEnv[1].name=HTTPS_PROXY"
#  --set "extraEnv[1].value=http://$PROXY_IP:$PROXY_PORT"
#  --set "extraEnv[1].name=NOPROXY"
#  --set "extraEnv[1].value=$NOPROXY_LIST"

# save the images for re-use during a future provisioning run
#$HERE/docker-image-save.sh $IMAGE_DIR/rancher rancher

