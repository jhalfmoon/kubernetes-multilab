apiVersion: v1
kind: Pod
metadata:
  name: network-multitool
  labels:
    app: network-multitool
spec:
  containers:
  - image: praqma/network-multitool:extra
    name: tool
  restartPolicy: Always
