
Basic usage::

    ./start
    ./patch
    ./stop

You can fool around by doing the following steps one at a time, and never more than one change in total, and then doing a delete/apply/edit cycle

* remove the apply-storageclass from the above commands, so no class gets made, and also comment out the storaegClass lines in the pv.yaml and pvc.yaml, so they do not reference it.
* comment out the allowVolumeExpansion line in sc.yaml, to se if resizing is still possible
* comment out the storageClass line from the pv
* comment out the storageClass line from the pvc

