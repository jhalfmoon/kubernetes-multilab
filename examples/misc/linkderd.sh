#!/bin/bash -e

echo "=== Installing linkerd"

# Linkderd installation info:
# https://linkerd.io/2.14/getting-started/
# https://linkerd.io/2.14/reference/cli/viz/

# NOTE: Do not cache requests for cr.l5d.io in the docker proxy. Doing that will brek the linkerd installer.
#       The reason for this is that cr.l5d.io redirects to cloudfare, but docker does not handle the redirect
#       correctly when going through the proxy. I suspect it has something to do with how the proxy returns the headers.
#       You only need to cache for the domain of production.cloudflare.docker.com.

BASENAME=linkerd
INSTALLER=linkerd2-installer.sh
# NOTE: $INSTALLROOT is used by the linkerd installer
export INSTALLROOT=/usr/local

# Restore previously stored script and binary, if available
if [[ -L $TEMP_DIR/$BASENAME ]] && [[ -e $TEMP_DIR/$INSTALLER ]] ; then
    sudo cp -P $TEMP_DIR/{$INSTALLER,$BASENAME} $INSTALLROOT/bin
else
    sudo curl --proxy '' --retry 3 --retry-delay 5 -sSL run.linkerd.io/install -o $INSTALLROOT/bin/$INSTALLER
fi

sudo -E bash $INSTALLROOT/bin/$INSTALLER

# Store installation script and binary
FULLNAME=$(basename $(readlink -f $INSTALLROOT/bin/$BASENAME))
sudo cp $INSTALLROOT/bin/{$FULLNAME,$INSTALLER} $TEMP_DIR/
[[ -e  $TEMP_DIR/$BASENAME ]] && sudo rm $TEMP_DIR/$BASENAME
sudo ln -fs $FULLNAME $TEMP_DIR/$BASENAME

linkerd check --pre
linkerd install | kubectl apply -f -
linkerd check

# install the on-cluster metrics stack
linkerd viz install | kubectl apply -f -
linkerd viz check
linkerd viz dashboard --show url

