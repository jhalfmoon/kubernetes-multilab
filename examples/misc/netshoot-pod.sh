apiVersion: v1
kind: Pod
metadata:
  name: netshoot
  labels:
    app: netshoot
spec:
  containers:
  - image: nicolaka/netshoot
    name: tool
    tty: true
    stdin: true
    stdinOnce: true
