HERE=$(dirname $(readlink -f $0))
source $HERE/cert-functions.sh

DESTDIR=/tmp/certs
[[ -d $DESTDIR ]] || mkdir -p $DESTDIR
cd $DESTDIR

gen_ca

FILE=grafana
HOST=test.$FILE.local
req_and_sign $FILE $HOST

FILE=prometheus
HOST=test.$FILE.local
req_and_sign $FILE $HOST

FILE=alertmanager
HOST=test.$FILE.local
req_and_sign $FILE $HOST

FILE=node_exporter
HOST=test.$FILE.local
req_and_sign $FILE $HOST

echo
echo "Certificates:"
echo
ls -alh
