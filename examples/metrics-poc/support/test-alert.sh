#!/bin/bash -eux
#
# Send a test alert to alertmanager. Use this to test alert routing.

curl -vL 'http://localhost:9093/api/v2/alerts' \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-d \
'[{
    "labels":
    {
        "alertname": "Test Alert",
        "namespace": "example_namespace",
        "status": "Open",
        "source": "Shell script",
        "severity": "Critical",
        "cluster": "Example cluster"
    },
    "annotations":
    {
        "description": "This is a mock alert for testing",
        "generatorURL": "https://test-endpoint.local",
        "categories": "Best-Practices, Configuration"
    }
}]'
