#!/bin/bash -eu

# The config files contain settings for type server, client of both (=multi).
# The multi config can be used for applications where you want to do server
# and client authentication using a single certificate

# Example usage:
#   HERE=$(dirname $(readlink -f $0))
#   source $HERE/cert-functions.sh
#   gen_ca
#   req_and_sign one one.host.local IP:1.1.1.1
#   self_sign    two two.host.local IP:2.2.2.2

CONFIGFILE=$HERE/multi.cnf
DAYS=1000

check_exists() {
    local FILE=$1
    if [[ -e $FILE ]] ; then
        echo "WARNING: File $FILE exists. Skipping it..."
        return 1
    else
        echo "Generating $FILE"
    fi
}

gen_key() {
    local FILE=$1
    if (check_exists $FILE) ; then
        openssl genpkey   \
            -algorithm EC \
            -pkeyopt ec_paramgen_curve:P-256 \
            -out $FILE
            # -algorithm RSA \
            # -pkeyopt rsa_keygen_bits:4096 \
            # -aes-128-cbc
    fi
}

gen_ca() {
    self_sign ca "self-signed.root.ca.timestamp.$(date +%s)"
}

self_sign() {
    local FILE=$1
    local HOST=$2
    local EXTRASAN=$3
    local SAN="subjectAltName=DNS:$HOST"
    [[ -n $EXTRASAN ]] && SAN=${SAN},${EXTRASAN}
    gen_key $FILE-key.pem
    if (check_exists $FILE-crt.pem) ; then
        openssl req \
            -x509   \
            -sha256 \
            -noenc  \
            -days $DAYS        \
            -subj "/CN=$HOST"  \
            -addext "$SAN"     \
            -key $FILE-key.pem \
            -out $FILE-crt.pem
     fi
}

req() {
    local FILE=$1
    local HOST=$2
    local EXTRASAN=$3
    local SAN="subjectAltName=DNS:$HOST"
    [[ -n $EXTRASAN ]] && SAN=${SAN},${EXTRASAN}
    gen_key $FILE-key.pem
    if (check_exists $FILE-csr.pem) ; then
        openssl req \
            -new \
            -sha256 \
            -noenc  \
            -subj "/CN=$HOST"  \
            -addext "$SAN"     \
            -key $FILE-key.pem \
            -out $FILE-csr.pem
    fi
}

sign() {
    local FILE=$1
    local HOST=$2
    if (check_exists $FILE-crt.pem) ; then
        openssl x509 \
            -req     \
            -copy_extensions copyall \
            -days $DAYS          \
            -extfile $CONFIGFILE \
            -CA ca-crt.pem       \
            -CAkey ca-key.pem    \
            -in $FILE-csr.pem    \
            -out $FILE-crt.pem
    fi
}

req_and_sign() {
    req $@
    sign $@
}

