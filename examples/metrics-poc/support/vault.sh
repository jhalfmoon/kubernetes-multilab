#!/bin/bash
#
# Example script to generate corporate certificates.

vault_cert() {
    FULL="${NAME}.${SUFFIX}"
    SUBJ="/C=US/ST=State/L=City/street=Oneway 66/postalCode=5555 ZZ/O=Corp Name/CN=$FULL"

    openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:P-256 -out $NAME-key.pem
    openssl req -new -sha256 -noenc -subj "$SUBJ" -addext "subjectAltName=DNS:$ALT_NAME" -out $NAME-csr.pem
    echo ===
    openssl req -text -noout -in $NAME-csr.pem
    echo ===
    vault write -format=json foo-pki/ica2-t1/sign/foo-domain csr=@$NAME-csr.pem ttl=8760h | tee $NAME-vault.json

    cat $NAME-vault.json | jq -r .data.ca_chain[]  | tee $NAME-ca-crt.pem
    echo ===
    cat $NAME-vault.json | jq -r .data.certificate | tee $NAME-crt.pem
}

export VAULT_ADDR=https://some-vault-node.local:8200
export VAULT_SKIP_VERIFY=true
vault login -method=oidc

SUFFIX=local
ALT_NAME=some.server.local

NAME=test.grafana
vault_cert
NAME=test.prometheus
vault_cert
NAME=test.alertmanager
vault_cert
