#!/bin/bash
#
# A support script to configure a node_exporter service on a VM.

BIN=/usr/local/bin/node_exporter
if [[ ! -e $BIN ]] ; then
    echo "ERROR: Please install $BIN first."
    exit 1
fi

SVC_USER=node_exporter
SVC_GROUP=node_exporter
sudo -E groupadd -f $SVC_USER
sudo -E useradd -g $SVC_GROUP --no-create-home --shell /bin/false $SVC_USER

CONFIG_DIR=/usr/local/etc/node_exporter
CONFIG_FILE=$CONFIG_DIR/web-config.yml
sudo -E mkdir -p $CONFIG_DIR
cat <<EOF | sudo -E tee $CONFIG_FILE
# https://github.com/prometheus/node_exporter
# https://github.com/prometheus/exporter-toolkit/blob/master/docs/web-configuration.md

tls_server_config:
  # NOTE: The hostname / SAN in the certificate is irrelevant if the prometheus scrape job is configured with insecure_skip_verify=true.
  # TLS will then only be for purposes of encryption. That means you could use any certificate you want to do TLS.
  # A self-signed cert would work just fine. For this POC one of the existing certs is used instead.
  cert_file: '$CONFIG_DIR/certs/prometheus-crt.pem'
  key_file: '$CONFIG_DIR/certs/prometheus-key.pem'
  # This bit enables client side authentication. As a result, the scraper (prometheus, alloy etc.) will have to present a valid certificate
  # to node_exporter and if configured, must also contain one of the given SANs. Remove this block to disable client authentication.
  client_ca_file: '$CONFIG_DIR/certs/ca-crt.pem'
  client_auth_type: 'RequireAndVerifyClientCert'
  client_allowed_sans:
    - 'test.prometheus.company.com'

http_server_config:
  # Enable HTTP/2 support. Note that HTTP/2 is only supported with TLS.
  http2: true
EOF
sudo -E chown -R $SVC_USER:$SVC_GROUP $CONFIG_DIR

cat <<EOF | sudo -E tee /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=$SVC_USER
Group=$SVC_GROUP
Type=simple
Restart=on-failure
ExecStart=$BIN \
  --web.config.file=$CONFIG_FILE
# --log.level=debug \
#  --web.disable-exporter-metrics \
#  --collector.disable-defaults   \
#  --collector.os                 \
#  --collector.cpu                \
#  --collector.meminfo
# --collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter
sleep 1
sudo systemctl status node_exporter

# To check the logs of node_exporter:
# journalctl -fu node_exporter

