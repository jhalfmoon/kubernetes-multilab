Metrics POC
###########
NOTE: This document is a moving target. Its goal is to document the progress of this POC. It might not be complete or accurante at times. Keep that in mind.

This repo is for a POC of a simple on-prem metrics setup using Prometheus and Grafana, running in a docker compose setup.

Goals
=====
For the POC and initial implementation, it is important to keep the complexity of the project to a bare minimum, that still meets all requirements.

Done:

* Prometheus and Grafana deployed to a single node, in containers, using docker-compose. This makes for easy deployment and independance of most OS components.

* Only node_exporter metrics are exported. That component will run as a non-containerized service, installed on each client.

* Authentication will be one with local accounts. There will be two types: Admin and reader. Each team will receive two accounts, one for each purpose.

* TLS encryption using the corporate CA for the UI of Prometheus, Grafana and Alertmanager.

* Only a single dashboard will be available by default: https://grafana.com/grafana/dashboards/1860-node-exporter-full/

* node_exporter TLS encryption. Note: self signed certificates will be just fine, as TLS is only used for encryption.

* Extra: Alertmanager configured and succesfully tested to send emails.

* Extra: Client side authentication; node_manager checks for a valid certificate offered by prometheus and also checks for a valid SAN. This effectively acts as a firewall to ensure that only the prometheus server can access node_exporter instances. This can easily be disabled, adjusting the node_exporter configs to not do client side authentication.
* LDAP integration.

Getting started
===============
This is not a comprehensive guide, but more an assistance::

    sudo -i
    BASE=/metrics
    mkdir $BASE
    cp -r /vagrant/examples/metrics-in-docker-compose $BASE/app
    cd /$BASE/app/support
    ./gen-certs.sh
    cd ..
    cp /tmp/certs/* config/certs
    chmod -R +r config/certs
    chown -R vagrant:vagrant $BASE
    exit
    cd $BASE/app
    ./up.sh

Misc
====
* On Ubuntu you might encounter DNS issues. Try the following fix: Add DNSStubListener=no to /etc/systemd/resolved.conf and restart the service::

    vim /etc/systemd/resolved.conf
    systemctl daemon-reload
    systemctl restart systemd-resolved.service

* All certificates are generated using "extendedKeyUsage = serverAuth, clientAuth". This is actually only needed when you wish to do both server side an d client side authentication with a single certificate. For prometheus, this is done in this POC: The https frontend (server side auth) uses the same certificate as is used to connect to the node_exporters (client side auth).

* To see the data and metadata of a certificate file::

    openssl x509 -text -in $CERT

* Testing client side auth::

    CERTS=config/certs
    echo | openssl s_client -CAfile $CERTS/ca-crt.pem -cert $CERTS/prometheus-crt.pem -key $CERTS/prometheus-key.pem -connect node_exporter.local:9100

    # Using curl - not useful for debugging, but doing actual http requests... yes
    CERTS=config/certs
    curl --noproxy '*' -kv --cacert $CERTS/ca-crt.pem --cert $CERTS/prometheus-crt.pem --key $CERTS/prometheus-key.pem https://localhost:9100

* To check what certificate(-chain) is being used on connection, you can use the following::

    echo | openssl s_client -showcerts -connect $HOST:443 -servername $VHOST 2>/dev/null
    # Or for the certificate data only:
    echo | openssl s_client -showcerts -connect $HOST:443 -servername $VHOST 2>/dev/null | sed -n '/BEGIN.*-/,/END.*-/p'

* If you restart nftables, for example after you've modified its config, you will have to restart docker too, in order for it to re-initialize the docker firewall rules.

* The file .env is used by 'docker compose' to source the variables from, that are used in the docker-compose file.

* The .env file contains UID and GID that are used to run the containers. For the POC, the IDs of the user 'metrics' are used. That user has /dev/null configured as shell and is the owner of /data/metrics with rwX access.

* The paths defined in .env must exist for the containers to be able to start.

* The folder 'targets' is bind mounted into the prometheus container and checked every 30 seconds by prometheus. Any updates to that file will be picked up automatically.

* The values of 'container_name' in the docker-compose file, can be used by the containers to access each other. For example, the source http://prometheus:9090 is a valid URL to access prometheus.

* For monitoring webhook alerts that are configured in alertmanager, you could use netcat to listen on the node and port you have configured alertmanager with::

  ssh some.webhooktarget.local
  nc -kl 0 5001

