#!/bin/bash -eu
#
# This is a support script to assist in starting the docker setup in this repo.

ENV=.env

init_dir() {
    DIR=$1
    if [[ ! -d $DIR ]] ; then
        sudo mkdir -p $DIR
        sudo chmod ugo+rwX $DIR
        sudo chown -R $SVC_USER:docker $DIR
    fi
}

if [[ ! -e $ENV ]] ; then
    echo "ERROR: The env file $ENV was not found."
    exit 1
fi
source $ENV

if [[ ! -d $BASE_DIR ]] ; then
    echo "ERROR: The directory $BASE_DIR does not exist. Please ensure that it exists and mounted on storage with enough capacity to host prometheus TSDB."
    exit 1
fi

FILE=config/grafana/ldap-password
if [[ ! -e $FILE ]] ; then
    echo "ERROR: The files $FILE does not exist. Please ensure that it exists and contains the password for the account defined in grafana/ldap.toml"
    exit 1
fi

# This bit is probably only needed when using caddy as reverse proxy.
# Fix for caddy log entry "failed to sufficiently increase receive buffer size (was: 208 kiB, wanted: 7168 kiB, got: 416 kiB"
# https://github.com/quic-go/quic-go/wiki/UDP-Buffer-Sizes
FILE=/etc/sysctl.d/udp-buffer-increase.conf
if [[ ! -e $FILE ]] ; then
    sudo tee $FILE <<EOF
sysctl -w net.core.rmem_max=7500000
sysctl -w net.core.wmem_max=7500000
EOF
    sudo sysctl -p
fi

#sudo docker network create -d bridge monitoring

# create group if it does not already exist
grep -q $SVC_USER /etc/group &>/dev/null || sudo groupadd $SVC_USER
# create user if it does not already exist
grep -q $SVC_GROUP /etc/passwd &>/dev/null || sudo useradd $SVC_USER -g $SVC_GROUP -s /dev/null
# add current user to the group, so to ensure access to $PROMETHEUS_DIR and $GRAFANA_DIR
(groups | grep -q $SVC_GROUP) || sudo usermod -a -G $SVC_GROUP $SVC_USER

init_dir $PROMETHEUS_DIR
init_dir $GRAFANA_DIR
init_dir $ALERTMANAGER_DIR
init_dir $CADDY_DIR

docker compose up -d

